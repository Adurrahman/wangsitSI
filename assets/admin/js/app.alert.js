const activate = {
  title: "Verifikasi",
  text: "Apakah kamu yakin data user sudah benar",
  icon: "warning",
  buttons: {
    cancel: true,
    confirm: {
      text: "Ya, benar",
      value: "activation"
    }
  }
}

const banUser = {
  title: "Ban User",
  text: "Apakah kamu yakin User ini bermasalah",
  icon: "warning",
  dangerMode: true,
  buttons: {
    cancel: true,
    confirm: {
      text: "Ya, benar",
      value: true
    }
  }
}

const restoreUser = {
  title: "Ban User",
  text: "Apakah kamu yakin User ini sudah di rehabilitasi",
  icon: "warning",
  dangerMode: true,
  buttons: {
    cancel: true,
    confirm: {
      text: "Ya, benar",
      value: true
    }
  }
}

const disableAcademyCategory = {
  title: "Hapus Kategori",
  text: "Apakah kamu yakin ingin menghapus kategori ini ?",
  icon: "warning",
  dangerMode: true,
  buttons: {
    cancel: true,
    confirm: {
      text: "Ya, benar",
      value: true
    }
  }
}

const disableAcademyCourse = {
  title: "Hapus Materi",
  text: "Apakah kamu yakin ingin menghapus materi ini ?",
  icon: "warning",
  dangerMode: true,
  buttons: {
    cancel: true,
    confirm: {
      text: "Ya, benar",
      value: true
    }
  }
}

const example = {
  title: "Are you sure?",
  text: "Once deleted, you will not be able to recover this imaginary file!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
}

function callAlert(state, action){
  let actionObj;
  switch(state){
    case 'activate':
      actionObj = activate;
      break;
    case 'banUser':
      actionObj = banUser;
      break;
    case 'restore':
      actionObj = restoreUser;
    case 'disableAcademyCategory':
      actionObj = disableAcademyCategory;
      break;
    case 'disableAcademyCourse':
      actionObj = disableAcademyCourse;
      break;
  }

  swal(actionObj)
  .then((isConfirmed) => {
    if(isConfirmed)
      action();
  });
}


const activationSuccess = ()=>{
  swal("Selesai! User telah aktif ", {
    icon: "success",
  })
  .then(()=> location.reload());
}

const banUserSuccess = ()=>{
  swal("Selesai! User telah Dinonaktifkan ", {
    icon: "success",
  })
  .then(()=> location.reload());
}

const restoreUserSuccess = ()=>{
  swal("Selesai! User telah Diaktifkan Kembali ", {
    icon: "success",
  })
  .then(()=> location.reload());
}

const disableCategorySuccess = ()=>{
  swal("Selesai! Kategori telah Dihapus ", {
    icon: "success",
  })
  .then(()=> location.reload());
}

const disableCourseSuccess = ()=>{
  swal("Selesai! Materi telah Dihapus ", {
    icon: "success",
  })
  .then(()=> location.reload());
}

const eventManagerAuthorizationSuccess = (id)=>{
  swal("Selesai! Selamat menjalankan program kerja", {
    icon: "success",
  })
  .then(()=> location.replace("/admin/eventDetail/" + id));
}
