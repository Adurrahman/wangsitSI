$(document).ready( function () {
  const dropify = $('.dropify').dropify({
    messages: {
        'default': 'Drag and drop a file Event Poster',
        'replace': 'Drag and drop or click to replace',
        'remove':  'Remove',
        'error':   'Ooops, something wrong happended.'
    }
  });

  $('#members, #academy').DataTable();
  $('select').selectpicker();
  $('.eventDetail').hover(function(){
    const card = $(this).children()[0];
    $(card).toggleClass("shadow");
  })
  
  $('.eventDetail').click(function(){
    const card    = $(this).children()[0]; 
    let id_event  = $(card).data('id');
  
    checkEventAuthorization(id_event);
  });

  $('.collapse-item').collapse();

  $('.datepicker').datepicker({
    format: "yyyy-mm-dd",
    todayBtn: "linked",
    autoclose: true
  });

  $('.form-choice-trigger').click(function(){
    const target    = $(this).data('target');
    const wrapper   = $(this).data('wrapper');

    $(target).toggleClass('border border-success bg-success');
    $(target + "> button").toggleClass('text-white font-weight-bold choiced-from');
    $(wrapper).removeClass('show')
    $(this).toggleClass('btn-outline-success')
    $(this).hasClass('btn-outline-success') ? $(this).html('Pilih Form') : $(this).html('Hapus Form'); 
  })

  $('.dropify').change(function(){
    const id_event  = window.location.pathname.split('/')[3]; 
    const url       = `${window.location.origin}/Admin/Event/changeImagePoster/${id_event}`;
    const form      = $('form#imagePoster')[0];

  
    $.ajax({
      type:     "POST",
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      timeout: 60000,
      url:      url,
      data:     new FormData(form),
  
      success: function(result){
        const {status, img_path} = JSON.parse(result)
        
        if(status){
          $('input[name="event_poster"]').attr('data-default-file', window.location.origin + img_path )
          swal("Upload Berhasil! Poster Event telah diperbarui ", { icon: "success" })
        } else {
          swal("Error", "Maaf Kamu tidak memiliki hak dalam event ini!", "error");
        }
        
      },
      error: function(error){
        swal("Error", error.statusText, "error");
      }
    })
  })

  dropify.on('dropify.afterClear', function(){
    const id_event  = window.location.pathname.split('/')[3]; 
    const url       = `${window.location.origin}/Admin/Event/removeEventPoster/${id_event}`;

      $.ajax({
        type: "POST",
        url: url,
        success: function(result){
          const {status} = JSON.parse(result)
          if(status){
            swal("Operasi Berhasil! Poster Event telah dihapus ", { icon: "success" })
            .then(() => location.reload())
          } else {
            swal("Error", "Maaf Kamu tidak memiliki hak dalam event ini!", "error");
          }
        },
        error: function(error){
          swal("Error", error.statusText, "error");
        }
      })
  });

});

function activateAccount(id){
  const url = window.location.origin + "/admin/activate";
  let id_user = id;
  let activate = ()=> updateData(url, id_user, activationSuccess);
  callAlert('activate' ,activate);
}

function bannedAccount(id){
  const url = window.location.origin + "/admin/banUser";
  let id_user = id;
  let activate = ()=> updateData(url, id_user, banUserSuccess);
  callAlert('banUser', activate);
}

function restoreAccount(id){
  const url = window.location.origin + "/admin/restoreUser";
  let id_user = id;
  let activate = ()=> updateData(url, id_user, restoreUserSuccess);
  callAlert('restore', activate);
}


function disableCategory(id){
  const url         = window.location.origin + "/Admin/Academy/disableCategory";
  let id_category   = id;
  let disable      = ()=> updateData(url, id_category, disableCategorySuccess);
  callAlert('disableAcademyCategory', disable);
}

function disableCourse(id){
  const url         = window.location.origin + "/Admin/Academy/disableCourse";
  let id_course     = id;
  let disable       = ()=> updateData(url, id_course, disableCourseSuccess);
  callAlert('disableAcademyCourse', disable);
}

function updateCategory(el){
  const url         = window.location.origin + "/Admin/Academy/updateCategory";
  let categoryName  = $(el).data("name");
  let idCategory    = $(el).data("id");

  $('button#action').html("Update");
  $('#id_category').val(idCategory);
  $('#name').val(categoryName);
  $('#academyForm').attr("action", url);
  $('#academyCategory').modal('show');
}

function updateCourse(el){
  const url         = window.location.origin + "/Admin/Academy/updateCourse";
  let idCourse      = $(el).data("id");
  let courseName    = $(el).data("name");
  let courseAlias   = $(el).data("alias");
  let courseLink    = $(el).data("link");
  let courseCategory= $(el).data("category");

  $('#id_course').val(idCourse);
  $('#name').val(courseName);
  $('#alias').val(courseAlias);
  $('#link').val(courseLink);
  $('.bootstrap-select button[title="Pilih Kategori"] .filter-option').text(courseName);
  $('select[name=category]').val(courseCategory);
  $('#academyForm').attr("action", url);
  $('#academyCourse').modal("show");
}

function checkEventAuthorization(id_event){
  const url = window.location.origin + "/Admin/Event/CheckAuthorization";

  $.ajax({
    type:     "POST",
    url:      url,
    data:     {id_event: id_event},

    success: function(result){
      const {status} = JSON.parse(result)
      
      if(status){
        eventManagerAuthorizationSuccess(id_event);
      } else {
        swal("Error", "Maaf Kamu tidak memiliki hak dalam event ini!", "error");
      }
      
    },
    error: function(error){
      swal("Error", error.statusText, "error");
    }
  })
}

function updateData(url, id_data, success){
  $.ajax({
    type:     "POST",
    url:      url,
    data:     {id: id_data},
    success: function (status) {
      status = JSON.parse(status);

      if(status)
        success();
      else
        swal("Error", "Maaf terjadi kesalahan", "error");
    },
    error: function (error) {
      swal("Error", error.statusText, "error");
    }
  })
}