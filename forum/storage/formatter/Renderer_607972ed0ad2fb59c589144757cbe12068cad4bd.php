<?php

/**
* @package   s9e\TextFormatter
* @copyright Copyright (c) 2010-2016 The s9e Authors
* @license   http://www.opensource.org/licenses/mit-license.php The MIT License
*/
class Renderer_607972ed0ad2fb59c589144757cbe12068cad4bd extends \s9e\TextFormatter\Renderer
{
	protected $params=array('DISCUSSION_URL'=>'','PROFILE_URL'=>'');
	protected static $tagBranches=array('EMAIL'=>0,'EMOJI'=>1,'ESC'=>2,'POSTMENTION'=>3,'URL'=>4,'USERMENTION'=>5,'br'=>6,'e'=>7,'i'=>7,'s'=>7,'html:a'=>8,'html:b'=>9,'html:br'=>10,'html:em'=>11,'html:h1'=>12,'html:h2'=>13,'html:h3'=>14,'html:i'=>15,'html:li'=>16,'html:ol'=>17,'html:p'=>18,'p'=>18,'html:strong'=>19,'html:u'=>20,'html:ul'=>21);
	public function __sleep()
	{
		$props = get_object_vars($this);
		unset($props['out'], $props['proc'], $props['source']);
		return array_keys($props);
	}
	public function renderRichText($xml)
	{
		if (!isset($this->quickRenderingTest) || !preg_match($this->quickRenderingTest, $xml))
			try
			{
				return $this->renderQuick($xml);
			}
			catch (\Exception $e)
			{
			}
		$dom = $this->loadXML($xml);
		$this->out = '';
		$this->at($dom->documentElement);
		return $this->out;
	}
	protected function at(\DOMNode $root)
	{
		if ($root->nodeType === 3)
			$this->out .= htmlspecialchars($root->textContent,0);
		else
			foreach ($root->childNodes as $node)
				if (!isset(self::$tagBranches[$node->nodeName]))
					$this->at($node);
				else
				{
					$tb = self::$tagBranches[$node->nodeName];
					if($tb<11)if($tb<6)if($tb<3)if($tb===0){$this->out.='<a href="mailto:'.htmlspecialchars($node->getAttribute('email'),2).'">';$this->at($node);$this->out.='</a>';}elseif($tb===1)$this->out.='<img alt="'.htmlspecialchars($node->textContent,2).'" class="emoji" draggable="false" src="//cdn.jsdelivr.net/emojione/assets/png/'.htmlspecialchars($node->getAttribute('seq'),2).'.png">';else$this->at($node);elseif($tb===3)$this->out.='<a href="'.htmlspecialchars($this->params['DISCUSSION_URL'].$node->getAttribute('discussionid'),2).'/'.htmlspecialchars($node->getAttribute('number'),2).'" class="PostMention" data-id="'.htmlspecialchars($node->getAttribute('id'),2).'">'.htmlspecialchars($node->getAttribute('username'),0).'</a>';elseif($tb===4){$this->out.='<a href="'.htmlspecialchars($node->getAttribute('url'),2).'" target="_blank" rel="nofollow noreferrer">';$this->at($node);$this->out.='</a>';}else$this->out.='<a href="'.htmlspecialchars($this->params['PROFILE_URL'].$node->getAttribute('username'),2).'" class="UserMention">@'.htmlspecialchars($node->getAttribute('username'),0).'</a>';elseif($tb<9){if($tb===6)$this->out.='<br>';elseif($tb===7);else{$this->out.='<a';if($node->hasAttribute('href'))$this->out.=' href="'.htmlspecialchars($node->getAttribute('href'),2).'"';if($node->hasAttribute('title'))$this->out.=' title="'.htmlspecialchars($node->getAttribute('title'),2).'"';$this->out.='>';$this->at($node);$this->out.='</a>';}}elseif($tb===9){$this->out.='<b>';$this->at($node);$this->out.='</b>';}else$this->out.='<br>';elseif($tb<17)if($tb<14)if($tb===11){$this->out.='<em>';$this->at($node);$this->out.='</em>';}elseif($tb===12){$this->out.='<h1>';$this->at($node);$this->out.='</h1>';}else{$this->out.='<h2>';$this->at($node);$this->out.='</h2>';}elseif($tb===14){$this->out.='<h3>';$this->at($node);$this->out.='</h3>';}elseif($tb===15){$this->out.='<i>';$this->at($node);$this->out.='</i>';}else{$this->out.='<li>';$this->at($node);$this->out.='</li>';}elseif($tb<20)if($tb===17){$this->out.='<ol>';$this->at($node);$this->out.='</ol>';}elseif($tb===18){$this->out.='<p>';$this->at($node);$this->out.='</p>';}else{$this->out.='<strong>';$this->at($node);$this->out.='</strong>';}elseif($tb===20){$this->out.='<u>';$this->at($node);$this->out.='</u>';}else{$this->out.='<ul>';$this->at($node);$this->out.='</ul>';}
				}
	}
	private static $static=array('/EMAIL'=>'</a>','/ESC'=>'','/URL'=>'</a>','/html:a'=>'</a>','/html:b'=>'</b>','/html:em'=>'</em>','/html:h1'=>'</h1>','/html:h2'=>'</h2>','/html:h3'=>'</h3>','/html:i'=>'</i>','/html:li'=>'</li>','/html:ol'=>'</ol>','/html:p'=>'</p>','/html:strong'=>'</strong>','/html:u'=>'</u>','/html:ul'=>'</ul>','ESC'=>'','html:b'=>'<b>','html:br'=>'<br>','html:em'=>'<em>','html:h1'=>'<h1>','html:h2'=>'<h2>','html:h3'=>'<h3>','html:i'=>'<i>','html:li'=>'<li>','html:ol'=>'<ol>','html:p'=>'<p>','html:strong'=>'<strong>','html:u'=>'<u>','html:ul'=>'<ul>');
	private static $dynamic=array('EMAIL'=>array('(^[^ ]+(?> (?!email=)[^=]+="[^"]*")*(?> email="([^"]*)")?.*)s','<a href="mailto:$1">'),'URL'=>array('(^[^ ]+(?> (?!url=)[^=]+="[^"]*")*(?> url="([^"]*)")?.*)s','<a href="$1" target="_blank" rel="nofollow noreferrer">'),'html:a'=>array('(^[^ ]+(?> (?!(?>href|title)=)[^=]+="[^"]*")*( href="[^"]*")?(?> (?!title=)[^=]+="[^"]*")*( title="[^"]*")?.*)s','<a$1$2>'));
	private static $attributes;
	private static $quickBranches=array('EMOJI'=>0,'POSTMENTION'=>1,'USERMENTION'=>2);

	protected function renderQuick($xml)
	{
		$xml = $this->decodeSMP($xml);
		self::$attributes = array();
		$html = preg_replace_callback(
			'(<(?:(?!/)((?>EMOJI|html:br|(?>POST|USER)MENTION))(?: [^>]*)?>.*?</\\1|(/?(?!br/|p>)[^ />]+)[^>]*?(/)?)>)s',
			array($this, 'quick'),
			preg_replace(
				'(<[eis]>[^<]*</[eis]>)',
				'',
				substr($xml, 1 + strpos($xml, '>'), -4)
			)
		);

		return str_replace('<br/>', '<br>', $html);
	}

	protected function quick($m)
	{
		if (isset($m[2]))
		{
			$id = $m[2];

			if (isset($m[3]))
			{
				unset($m[3]);

				$m[0] = substr($m[0], 0, -2) . '>';
				$html = $this->quick($m);

				$m[0] = '</' . $id . '>';
				$m[2] = '/' . $id;
				$html .= $this->quick($m);

				return $html;
			}
		}
		else
		{
			$id = $m[1];

			$lpos = 1 + strpos($m[0], '>');
			$rpos = strrpos($m[0], '<');
			$textContent = substr($m[0], $lpos, $rpos - $lpos);

			if (strpos($textContent, '<') !== false)
				throw new \RuntimeException;

			$textContent = htmlspecialchars_decode($textContent);
		}

		if (isset(self::$static[$id]))
			return self::$static[$id];

		if (isset(self::$dynamic[$id]))
		{
			list($match, $replace) = self::$dynamic[$id];
			return preg_replace($match, $replace, $m[0], 1);
		}

		if (!isset(self::$quickBranches[$id]))
		{
			if ($id[0] === '!' || $id[0] === '?')
				throw new \RuntimeException;
			return '';
		}

		$attributes = array();
		if (strpos($m[0], '="') !== false)
		{
			preg_match_all('(([^ =]++)="([^"]*))S', substr($m[0], 0, strpos($m[0], '>')), $matches);
			foreach ($matches[1] as $i => $attrName)
				$attributes[$attrName] = $matches[2][$i];
		}

		$qb = self::$quickBranches[$id];
		if($qb===0){$attributes+=array('seq'=>null);$html='<img alt="'.htmlspecialchars($textContent,2).'" class="emoji" draggable="false" src="//cdn.jsdelivr.net/emojione/assets/png/'.$attributes['seq'].'.png">';}elseif($qb===1){$attributes+=array('discussionid'=>null,'number'=>null,'id'=>null,'username'=>null);$html='<a href="'.htmlspecialchars($this->params['DISCUSSION_URL'].htmlspecialchars_decode($attributes['discussionid']),2).'/'.$attributes['number'].'" class="PostMention" data-id="'.$attributes['id'].'">'.str_replace('&quot;','"',$attributes['username']).'</a>';}else{$attributes+=array('username'=>null);$html='<a href="'.htmlspecialchars($this->params['PROFILE_URL'].htmlspecialchars_decode($attributes['username']),2).'" class="UserMention">@'.str_replace('&quot;','"',$attributes['username']).'</a>';}

		return $html;
	}
}