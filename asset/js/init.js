$(document).ready(function(){
  $(".sidenav").sidenav();
  $(".parallax").parallax();
  $(".dropdown-trigger").dropdown({coverTrigger : false, constrainWidth : false});
  $(".tabs").tabs();
  $(".modal").modal();
  $("select").formSelect();
  $("input#input_text").characterCounter();

  if($('#kbmsiProduct').html() != null)
    $('#kbmsiProduct').owlCarousel({
      loop: true,
      margin: 10,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 3,
          nav: false
        },
        1000: {
          items: 3,
          nav: false,
          loop: false,
          margin: 10
        }
      }
    });

  if($("#summernote").html() != null){
    $('#summernote').summernote('code', '');
    
    $("#summernote").summernote({
      placeholder: '',
      tabsize: 5,
      height: 200
    });
  }

});			