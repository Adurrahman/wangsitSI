
  $(document).ready(function () {
    $('#search').keyup(function (e) { 
      var searchValue = this.value.toString().toLowerCase();
      generateObj(searchValue);				
    });

    $('.cat-trigger').click( function (el) {				
      var catItem = el.currentTarget.textContent.toString().toLowerCase().trim();
      $('.cat-trigger').removeClass("active");
      $(this).addClass("active");
      
      generateObj(catItem);
    });

    /* 
      this function for generate object that contain academy card 
      and it will get the data atribut for compare the data
    */
    const generateObj = (elValue) => {
      Array.from($('.academy-card')).forEach(childEl => {
        var dataCategory    = childEl.getAttribute('data-category').toString().toLowerCase();
        var dataCourseName  = childEl.getAttribute('data-courseName').toString().toLowerCase();
        
        childEl.style.opacity = 0;
        
        if ( dataCategory.indexOf(elValue) > -1 ||  dataCourseName.indexOf(elValue) > -1|| elValue == "Semua"){					
          childEl.style.opacity = 1;							
          setTimeout(() => { childEl.style.display = "block"; }, 500);         
        } else {
          setTimeout(() => { childEl.style.display = "none"; }, 500);
        }
      });
    }
  });