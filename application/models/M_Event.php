<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Event extends CI_Model {
 
  protected $user           = "mhs_kbmsi";
  protected $event          = "event";
  protected $history        = "event_history";
  protected $registration   = "event_registration";
  protected $event_img      = "event_img";
  protected $role           = "event_role";
  protected $division       = "division";
  protected $div_member     = "division_member";

  public function getEvents(){
      
    $date = date('y-m-d');
    
    if(isset($_SESSION['admin'])){
      $this->db->select('*');
      $this->db->from("$this->user user");
      $this->db->join("$this->event event", "user.ID = event.responsible_person") ;
      return $this->db->get()->result();
      
    }
    
    $this->db->where('quota is NOT NULL');
    $this->db->where('open_registration is NOT NULL');
    $this->db->where('open_registration <=', "$date");
    $this->db->where('close_registration is NOT NULL');
    $this->db->where("close_registration >=", "$date");
    return $this->db->get($this->event)->result();
  }

  public function getEventById($id){
    $this->db->where("id_event", $id);
    return $this->db->get($this->event)->row();
  }

  public function regist($data){
    $data['registration_date'] = date(DATE_ATOM, time());
    return $this->db->insert($this->registration, $data);
  }
  
  public function checkRegisterStatus($id_event){
    $this->db->where('id_event', $id_event);
    $this->db->where('id_user', $_SESSION['userData']['ID']);
    return $this->db->get($this->registration)->num_rows() != 0;
  }

  public function isFinishedEvent($id_event) {
    $date = date('y-m-d');

    $this->db->where("id_event", $id_event);
    $this->db->where("close_registration <=", "$date");
    return $this->db->get($this->event)->num_rows() == 1; 
  }

  public function getParticipants($id_event){
    $this->db->select("user.NAMA, DISTINCT user.NIM, user.HP, user.id_line");
    $this->db->from("mhs_kbmsi user");
    $this->db->join("event_registration event", "user.ID = event.id_user");
    return $this->db->get()->result();
  }

  public function getQuota($id_event){
    $this->db->select("COUNT(id_user) as jumlah");
    $this->db->from($this->registration);
    $this->db->where("id_user !=", 0);
    $this->db->where("id_event", $id_event);
    return $this->db->get()->row();
  }

  public function addEvent($data){
    return $this->db->insert($this->event, $data);
  }

  public function updateEvent($data){
    $this->db->where('id_event', $data['id_event']);
    $this->db->update($this->event, $data);
  }

  public function checkEventManager($data){
    $this->db->where('responsible_person', $_SESSION['admin']->id_user);
    $this->db->where('id_event', $data);
    $result =  $this->db->get($this->event)->result();

    return !empty($result);
  }

  public function updateImgPath($data){
    $this->db->where("id_event", $data['id_event']);
    return $this->db->update($this->event, $data);
  }

  public function getEventParticipants($id_event){
    $this->db->select('user.ID as id_user, user.NAMA as name, user.EMAIL as email, user.NIM as nim, user.HP as phone, user.id_line');
    $this->db->from("$this->user user");
    $this->db->join("$this->registration event", "user.ID = event.id_user");
    $this->db->where('id_event', $id_event);
    return $this->db->get()->result();
  }

  public function getPastEvents() {
    $date = date('y-m-d');

    $this->db->where("close_registration <=", "$date");
    return $this->db->get($this->event)->result();
  }

  public function getDepartmentById($id){
    return $this->db->get_where('division', ['id' => $id])->row_array();
  }

  public function getDepartmentByName($name){
    return $this->db->get_where('division', ['singkatan' => $name])->row_array();
  }

  public function getAllDepartment(){
    return $this->db->get('division')->result_array();
  }

  public function getAllEvents(){
    $date = date('y-m-d');
    $this->db->where('open_registration <=', "$date");
    $this->db->order_by('id_event', 'DESC');
    return $this->db->get($this->event)->result();
  }

  public function getEventByDepartment($id_dept) {
    return $this->db->get_where($this->event, ['department' => $id_dept])->result();
  }
}