<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_EventStarship extends CI_Model {

public function insertForm($data){
     $this->db->insert('pendaftaran_starship', $data); 
}

public function cekDaftar($nim){
   $result = array();
    $this->db->where('nim', $nim);
    $query = $this->db->get('pendaftaran_starship'); 
    $result = $query->result();
    if (count($result)>0)
    {
      return 1;
    }
    else {
      return 0;
    }
}
// public function cekStatus($nim){
//   $this->db->select('diterima');
//   $this->db->from('pendaftaran_starship');
//   $this->db->where('nim', $nim);
//   $query = $this->db->get();
//   $result = $query->result();
//   if ($result == 1) {
//     return 1;
//   } else {
//     return 0;
//   }
// }
public function getAllPendaftar(){
  $this->db->select('*');
	 $this->db->from('pendaftaran_starship');
	 $query = $this->db->get();
	 $result = $query->result();
	 return $result;
}

public function Pendaftar($nim){
  $this->db->select('nama, diterima');
	 $this->db->from('pendaftaran_starship');
   $this->db->where('nim', $nim);
	 $query = $this->db->get();
	 // $result = $query->result();
	 return $query;
}

function ubahStatus($id, $key)
 {
	 $this->db->set('diterima', $key);
	 $this->db->where('id_event', $id);
	 $this->db->update('pendaftaran_starship');
 }

}
