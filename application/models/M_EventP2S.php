<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_EventP2S extends CI_Model
{

  public function cekDaftar($nim)
  {
    $result = array();
    $this->db->where('nim', $nim);
    $query = $this->db->get('pendaftaran_skrim');
    $result = $query->result();
    if (count($result) > 0) {
      return 1;
    } else {
      return 0;
    }
  }

  public function insertForm($data)
  {
    $this->db->insert('pendaftaran_skrim', $data);
  }

  public function getAllPendaftar()
  {
    $this->db->select('*');
    $this->db->from('pendaftaran_skrim');
    $query = $this->db->get();
    $result = $query->result();
    return $result;
  }
}
