<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Login extends CI_Model {

	public function isRegistered($uid){
		$this->db->where("UID_PROVIDER", $uid);
		return $this->db->get("mhs_kbmsi")->result() != NULL;
	}

	public function getUserData($uid){
		$query = $this->db->query("SELECT ID, NIM, NAMA, USERNAME, GAMBAR, STATUS FROM mhs_kbmsi WHERE UID_PROVIDER = '$uid' OR NIM = '$uid' OR USERNAME = '$uid'");
		return $query->row_array();
	}

	public function getUsernameAndPassword($NIM){
		$this->db->where('NIM', $NIM);
		$this->db->or_where('USERNAME', $NIM);
		return $this->db->get('mhs_kbmsi')->row();
	}
	
	public function EditAdmin($id){
			$query = $this->db->query("SELECT * FROM mhs_kbmsi WHERE ID='$id'");
			return $query->row();
	}

	public function checkUsernameAvailability($formData){
		return !$this->db->insert('mhs_kbmsi', $formData) && $this->db->error()['code']==1062;
	}

	public function insertData($formData){
		$this->db->insert("mhs_kbmsi", $formData);
	}
	
	public function getRoleData($username){
		$this->db->select('user.ID as id_user, user.USERNAME as username, user.NAMA as fullname, user.NIM as nim, user.GAMBAR as profile_image, division.name as division, role.name as role');
		$this->db->from('mhs_kbmsi user');
		$this->db->join('division_member div_member', 'user.ID = div_member.id_user');
		$this->db->join('division', 'div_member.id_division = division.id_division');
		$this->db->join('division_role role', 'div_member.id_role = role.id_role');
		$this->db->where('USERNAME', $username);
		$this->db->or_where('NIM', $username);
		
		return $this->db->get()->row();
	}

}