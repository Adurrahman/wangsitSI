<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Division extends CI_Model {
  protected $division = 'division'; 
  protected $member   = 'division_member';
  protected $role     = 'division_role';
  protected $user     = 'mhs_kbmsi';

 public function getDivisions(){
   return $this->db->get($this->division)->result();
 }

 public function getRoles(){
   return $this->db->get($this->role)->result();
 }

 public function getIdByName($name){
   $this->db->where("name", $name);
   return  $this->db->get($this->division)->result()[0]->id_division;
 }

 public function getCandidates(){
   $range = getGenerationRange();
   $this->db->select("user.NAMA as fullname, user.NIM as nim, user.ID as id_user");
   $this->db->where("(user.nim LIKE '$range->min%') OR (user.nim LIKE '$range->mid%') OR (user.nim LIKE '$range->max%') ");
   return $this->db->get($this->user . ' user')->result();
 }

 public function getDivisionMembers($division){
  $params = $division == "NON-DEPARTEMEN" || isSuperAdmin() ? '%' : $division;

  $this->db->select('user.ID as id_user, user.NAMA as fullname, user.NIM as nim, div.name as division, role.name as role');
  $this->db->from("$this->member member");
  $this->db->join("$this->user user", 'member.id_user = user.ID');
  $this->db->join("$this->role role", 'member.id_role = role.id_role');
  $this->db->join("$this->division div", "member.id_division = div.id_division");
  $this->db->where("div.name LIKE '$params'");
  return $this->db->get()->result();
 }

 public function addMember($data){
   return $this->db->insert($this->member, $data);
 }

}