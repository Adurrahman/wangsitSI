<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_EventPsdm extends CI_Model
{

  public function insertForm($data)
  {
    $this->db->insert('oprec_wow', $data);
  }

  public function cekDaftar($nim)
  {
    $result = array();
    $this->db->where('nim', $nim);
    $query = $this->db->get('oprec_wow');
    $result = $query->result();
    if (count($result) > 0) {
      return 1;
    } else {
      return 0;
    }
  }

  public function cekDiterima($nim)
  {
    $this->db->select('diterima');
    $this->db->where('nim', $nim);
    $query = $this->db->get('oprec_wow')->row();
    return $query;
  }

  public function getAllPendaftar()
  {
    $this->db->select('*');
    $this->db->from('oprec_wow');
    $query = $this->db->get();
    $result = $query->result();
    return $result;
  }

  public function Pendaftar($nim)
  {
    $this->db->select('nama, diterima');
    $this->db->from('oprec_wow');
    $this->db->where('nim', $nim);
    $query = $this->db->get();
    // $result = $query->result();
    return $query;
  }

  function ubahStatus($id, $key)
  {
    $this->db->set('diterima', $key);
    $this->db->where('idPeserta', $id);
    $this->db->update('oprec_wow');
  }
}
