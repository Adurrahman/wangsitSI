<?php

/**
* 
*/
class m_user extends CI_Model{

	protected $user = 'mhs_kbmsi';
	
	function tampil_data(){
		return $this->db->get('seller');
	}
	
	function input_data($data,$table){
		$this->db->insert($table,$data);
	}

	public function all(){
		$hasil = $this->db->get('seller');
		if ($hasil->num_rows() > 0) {
			return $hasil->result();
		} else {
			return array();
		}
	}

	function verifyAccount($email,$password,$table){
		$this->db->where('email', $email);
		$this->db->where('password', $password);
		$query = $this->db->get($table);
		return $query;
	}

	public function find($id){
		$hasil = $this->db->where('id',$id)->limit(1)->get('seller');
		if ($hasil->num_rows()>0) {
			return $hasil->row();
		} else {
			return array();
		}
	}

	public function update($id, $data_products){
		$this->db->where('id',$id)->update('seller', $data_products);
	}

	public function deleteuser($id){
		$this->db->where('id',$id)->delete('seller');
		// $this->db->where('Nd',$punyaSiapa)->delete('produk');
		// $query = $this->db->query("DELETE * FROM produk WHERE punyaSiapa=''");
		// $this->db->where('email',$email)
	}

	public function create($data_seller){
		$this->db->insert('seller',$data_seller);
	}

	public function delete($id){
		$this->db->where('id',$id)->delete('seller');
		$this->db->where('seller_id',$id)->delete('product');
	}

	public function getUserData($id){
		$this->db->where('ID', $id);
		return $this->db->get('mhs_kbmsi')->row();
	}

	public function updateData($id_user, $id_line, $NAMA, $NIM, $HP){
		$this->db->where('ID', $id_user);
		$this->db->update('mhs_kbmsi', [
							"id_line" => $id_line,
							"NAMA" => $NAMA,
							"NIM" => $NIM,
							"HP" => $HP	
						]);
	}

	public function getUsersData(){
		$this->db->where('STATUS', 1);
		$this->db->order_by("NAMA", "ASC");
		return $this->db->get($this->user)->result();
	}

	public function getPendingUser(){
		$this->db->where('STATUS', 0);
		$this->db->order_by("NAMA", "ASC");
		return $this->db->get($this->user)->result();
	}

	public function getBannedUser(){
		$this->db->where('STATUS', -1);
		$this->db->order_by("NAMA", "ASC");
		return $this->db->get($this->user)->result();
	}

	public function activateUser($id_user){
		$action = ['STATUS' => 1];
		
		$this->db->where('ID', $id_user);
		return $this->db->update($this->user, $action);
	}

	public function bannedUser($id_user){
		$action = ['STATUS' => -1];
		
		$this->db->where('ID', $id_user);
		return $this->db->update($this->user, $action);
	}
	
	public function restoreUser($id_user){
		$action = ['STATUS' => 1];
		
		$this->db->where('ID', $id_user);
		return $this->db->update($this->user, $action);
	}

	public function editUser($data){
		
		
		$this->db->where('ID', $data['id_user']);
		return $this->db->update($this->user, $action);
	}

}