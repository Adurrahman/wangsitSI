<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_EventOprec extends CI_Model {

public function insertForm($data){
     $this->db->insert('oprec_kbmsi', $data); 
}

public function cekDaftar($nim){
   $result = array();
    $this->db->where('nim', $nim);
    $query = $this->db->get('oprec_kbmsi'); 
    $result = $query->result();
    if (count($result)>0)
    {
      return 1;
    }
    else {
      return 0;
    }
}
// public function cekStatus($nim){
//   $this->db->select('status');
//   $this->db->from('oprec_kbmsi');
//   $this->db->where('nim', $nim);
//   $query = $this->db->get();
//   $result = $query->result();
//   if ($result == 1) {
//     return 1;
//   } else {
//     return 0;
//   }
// }
public function getAllPendaftar(){
  $this->db->select('*');
	 $this->db->from('oprec_kbmsi');
	 $query = $this->db->get();
	 $result = $query->result();
	 return $result;
}

public function Pendaftar($nim){
  $this->db->select('nama, status');
	 $this->db->from('oprec_kbmsi');
   $this->db->where('nim', $nim);
	 $query = $this->db->get();
	 // $result = $query->result();
	 return $query;
}

function ubahStatus($id, $key)
 {
	 $this->db->set('status', $key);
	 $this->db->where('id_regist', $id);
	 $this->db->update('oprec_kbmsi');
 }

}
