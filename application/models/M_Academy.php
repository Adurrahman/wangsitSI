<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_academy extends CI_Model {

  protected $academy  = "academy";
  protected $category = "academy-category";

  public function getLessons(){
    $this->db->select("academy.id, academy.course as name, academy.abbreviation as alias, category.category, academy.link, academy.visited");
    $this->db->from($this->academy);
    $this->db->join("$this->category category", "academy.category = category.id");
    $this->db->order_by("academy.course", "ASC");

    return $this->db->get()->result();
  }

  public function getCategories(){
    $this->db->where("status", "1");
    $this->db->order_by("category", "ASC");
    
    return $this->db->get($this->category)->result();
  }

  public function getVisitValue($idCourse){
    $result = $this->db->query("SELECT visited FROM academy WHERE id = $idCourse");
    return $result->row()->visited;
  }

  public function updateVisitValue($idCourse){
    $oldVal = $this->getVisitValue($idCourse);
    $result = $this->db->query("UPDATE academy SET visited = ($oldVal + 1) WHERE id = $idCourse");
  }

  public function getLink($id){
    $result = $this->db->query("SELECT link FROM academy WHERE id = $id");
    return $result->row()->link;
  }

  public function insertNewCourse($data){
    return $this->db->insert($this->academy , $data);
  }

  public function insertNewCategory($data){
    return $this->db->insert($this->category, $data);
  }

  public function updateCategory($data){
    $this->db->where("id", $data['id']);
    return $this->db->update($this->category, $data);
  }

  public function updateCourse($data){
    $this->db->where("id", $data['id']);
    return $this->db->update($this->academy, $data);
  }
  
  public function disableCategory($data){
    $this->db->where("id", $data['id']);
    return $this->db->update($this->category, $data);
  }
  
  public function disableCourse($data){
    $this->db->where("id", $data['id']);
    return $this->db->update($this->academy, $data);
  }
}