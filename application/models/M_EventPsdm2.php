<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_EventPsdm2 extends CI_Model
{

  public function insertForm($data)
  {
    $this->db->insert('oprec_mrfest', $data);
  }

  public function cekDaftar($nim)
  {
    $result = array();
    $this->db->where('nim', $nim);
    $query = $this->db->get('oprec_mrfest');
    $result = $query->result();
    if (count($result) > 0) {
      return 1;
    } else {
      return 0;
    }
  }

  public function cekDiterima($nim)
  {
    $this->db->select('diterima');
    $this->db->where('nim', $nim);
    $query = $this->db->get('oprec_mrfest')->row();
    return $query;
  }

  public function getAllPendaftar()
  {
    $this->db->select('*');
    $this->db->from('oprec_mrfest');
    $query = $this->db->get();
    $result = $query->result();
    return $result;
  }

  public function Pendaftar($nim)
  {
    $this->db->select('nama, diterima');
    $this->db->from('oprec_mrfest');
    $this->db->where('nim', $nim);
    $query = $this->db->get();
    // $result = $query->result();
    return $query;
  }

  function ubahStatus($id, $key)
  {
    $this->db->set('diterima', $key);
    $this->db->where('idPeserta', $id);
    $this->db->update('oprec_mrfest');
  }
}
