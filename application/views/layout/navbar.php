<!-- Sidenav
check if users are logged in or not -->
<?php if (isset($_SESSION['userData'])) { ?>

	<ul id="slide-out" class="sidenav">
		<li>
			<div class="user-view">
				<div class="background">
					<img src="<?= base_url("asset/img/bg-bawah.jpg") ?>">
				</div>
				<a href="">
					<img class="circle" src="<?= $_SESSION['userData']['GAMBAR'] ?>">
				</a>
				<span class="white-text name">
					<b> <?= $_SESSION['userData']['USERNAME'] ?> </b>
				</span>
				<span class="white-text email"> <?= $_SESSION['userData']['NAMA'] ?> </span>
			</div>
		</li>
		<li>
			<a href="<?= base_url('events') ?>" class="wangsit-text">
				<i class="material-icons">event_note</i><b> Event</b>
				<span class="new-feature"> new </span>
			</a>
		</li>
		<li>
			<a href="<?= base_url('academy') ?>" class="wangsit-text">
				<i class="material-icons">school</i><b>Academy</b>
			</a>
		</li>
		<!-- <li>
			<a href=" <?= base_url('article') ?>" class="wangsit-text">
				<i class="material-icons">book</i><b>Article</b>
			</a>
		</li>
		<li>
			<a href=" <?= base_url('Article/My/Post') ?>" class="wangsit-text">
				<i class="material-icons">edit</i><b>Write Your Article</b>
			</a>
		</li> -->
		<li class="divider" tabindex="-1"></li>
		<li>
			<a href=" <?= base_url('/dashboard') ?>" class="wangsit-text">
				<i class="material-icons">computer</i>Dashboard
			</a>
		</li>
		<li>
			<a href="<?= base_url("/" . $_SESSION["userData"]["USERNAME"]) ?>" class="wangsit-text">
				<i class="material-icons">person</i>Profile
			</a>
		</li>
		<li class="divider"></li>
		<li>
			<a href=" <?= base_url('/logout') ?>" class="wangsit-text">
				<i class="material-icons">exit_to_app</i>Log Out
			</a>
		</li>
	</ul>
<?php } else { ?>
	<ul id="slide-out" class="sidenav">
		<li>
			<a href="<?= base_url("academy") ?>" class="wangsit-text">
				<i class="material-icons">school</i><b>Academy</b>
			</a>
		</li>
		<!-- <li>
			<a href="<?= base_url("article") ?>" class="wangsit-text">
				<i class="material-icons">book</i><b>Article</b>
			</a>
		</li> -->
		<li class="divider" tabindex="-1"></li>
		<li>
			<a class="btn btn-small" href="<?= base_url("/login") ?>">Login</a>
		</li>
	</ul>
<?php } ?>



<div class="navbar-fixed">
	<nav class="z-depth-1 white nempel">
		<div class="nav-wrapper white">
			<div class="container">
				<a href="#" data-target="slide-out" class="sidenav-trigger hide-on-large-only">
					<i class="material-icons icon-wangsit">menu</i>
					<span class="new-feature-dot"> </span>

				</a>
				<a href="<?= base_url() ?>" class="brand-logo fontt">
					<img class="center-align" src="<?= base_url('asset/img/text-logo.png') ?>" style="width: 140px; margin-top: 7px;" alt="">
				</a>

				<ul class="right hide-on-med-and-down" style="margin-right: -100px;">
					<li>
						<a href="<?= base_url('events') ?>" class="new-features">
							<b><i class="material-icons left">event_note</i> Event </b>
							<span class="new-feature"> new </span>
						</a>
					</li>
					<li>
						<a href="<?= base_url('academy') ?>">
							<b><i class="material-icons left">school</i> Academy</b>
						</a>
					</li>
					<!-- <li>
						<a href="<?= base_url('article') ?>">
							<b><i class="material-icons left">book</i>Article</b>
						</a>
					</li>
					<li>
						<a href="#" class="dropdown-trigger" data-target="notification">
							<b> <i class="material-icons"> notifications</i> </b>
						</a>
					</li> -->

					<?php if (isset($_SESSION['userData'])) { ?>
						<li>
							<a class='dropdown-trigger' href='#!' data-target='profile-dropdown'>
								<img class='circle' src="<?= $_SESSION['userData']['GAMBAR'] ?>" style='width: 40px; vertical-align:middle; margin-right: 10px; margin-top: -5px' alt=''>
								<b> <?= ucfirst($_SESSION['userData']['USERNAME']) ?> </b>
								<i class='material-icons right' style='margin-left: 10px;'>arrow_drop_down</i>
							</a>
						</li>;

					<?php } else { ?>
						<li><a class='btn btn-small' href="<?= base_url("/login") ?>">Login</a></li>";
					<?php	}	?>

				</ul>
				<div id="nav-mobile" class="right hide-on-med-and-down" style="margin-right: 30px"> </div>
			</div>
		</div>
	</nav>
</div>

<ul id="profile-dropdown" class="dropdown-content">
	<li>
		<a href="<?= base_url($_SESSION["userData"]["USERNAME"]) ?>" class="wangsit-text">Profil</a>
	</li>
	<li class="divider" tabindex="-1"></li>
	<li>
		<a href="<?= base_url('/profile/Me') ?>" class="wangsit-text">Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url("/logout") ?>" class="wangsit-text">
			<i class="material-icons">exit_to_app</i>Log Out
		</a>
	</li>
</ul>

<ul id="notification" class="dropdown-content" style="max-height: 70vh; overflow-Y: scroll">

	<?php
	if (isset($notifikasi)) {
		foreach ($notifikasi as $dataNotif) {

			echo "
			<li>
				<a href='#'>
					<div class='notification-card row'>
						<div class='col xl2 s2'>
							<img class='circle' src=" . $dataNotif['GAMBAR'] . " alt='foto kbmsi'>
						</div>
						<div class='col xl10 s10'>
							<p class='user'> $dataNotif[nama] </p>
							<p class='desc'> Tentang " . substr($dataNotif['JUDUL'], 0, 50) . " ... </p>
							<p class='time'> " . substr($dataNotif['DATE_COMMENT'], 0, 10) . " </p>
						</div>
					</div>
				</a>
			</li>
		";
		}
	}
	?>
</ul>