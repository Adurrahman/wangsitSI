<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="icon" type="image/x-icon" href="<?php echo base_url();?>favicon.png"/>

<title> <?= $title ?> | Wangsit KBMSI </title>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127828297-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-127828297-1');
</script>

<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
  var OneSignal = window.OneSignal || [];
  OneSignal.push(function() {
    OneSignal.init({
      appId: "03a9f854-753a-47c3-9313-ca53779a11ce",
    });
  });
</script>