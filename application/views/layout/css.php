<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,800i,700">
<link rel="stylesheet" href="<?= base_url();?>asset/css/materialize.css"  type="text/css">
<link rel="stylesheet" href="<?= base_url();?>asset/css/font-awesome.min.css" r type="text/css">
<link rel="stylesheet" href="<?= base_url();?>asset/css/styles.css?v=2"  type="text/css">

<?php if (@$library == "owlCarousel") { ?>
  <link rel="stylesheet" href="<?= base_url("asset/css/owl.theme.default.css");?>">
  <link rel="stylesheet" href="<?= base_url("asset/css/owl.carousel.min.css");?>">
<?php } ?>

<?php if(@$library == "summerNote") { ?>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css" rel="stylesheet">
<?php } ?>
