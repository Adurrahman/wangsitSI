<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="<?= base_url("asset/js/materialize.js")?>"></script>

<?php if ($library == "owlCarousel") { ?>
  <script src="<?= base_url();?>asset/js/owl.carousel.min.js"></script>
<?php } ?>

<?php if($library == "summerNote") { ?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script>
<?php } ?>

<script src="<?= base_url("asset/js/init.js")?>"></script>

