<div class="row white-text center-align footer">
  <div class="col s12" style="color:#55254e">
    Made with <b>&#x2764; </b> by <a class="text-lighten-3" href="http://kbmsi.filkom.ub.ac.id/kelembagaan/emsi/p2s" target="_blank" style="color:#55254e">P2S EMSI 2021</a>
  </div>
</div>