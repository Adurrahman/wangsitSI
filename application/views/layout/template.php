<!DOCTYPE html>
<html lang="en">
<head>
  <?= @$_head ?>
  <?= @$_css ?>
</head>
<body>
  <?= @$_navbar ?>
  
  <?= @$_body ?>

  <!--<?= @$_footer ?>-->
  <?= @$_js ?>
</body>
</html>