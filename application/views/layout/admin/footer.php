<footer>
  <div class="mdc-layout-grid">
    <div class="mdc-layout-grid__inner">
      <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6-desktop">
        <span class="tx-14" style="color:#55254e"> Made with <b>&#x2764;	</b> By <a href="javascript:void(0)" style="color:#55254e"> P2S EMSI 2021 </a> </span>
      </div>
    </div>
  </div>
</footer>