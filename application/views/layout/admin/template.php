<!DOCTYPE html>
<html lang="en">
<head>
  <?= @$_head ?>
</head>
<body>
  <?= @$_sidebar ?>
  
  <div class="main-wrapper mdc-drawer-app-content">
    <?= @$_navbar ?>
    <div class="page-wrapper mdc-toolbar-fixed-adjust">  
      <?= @$_body ?>
    </div>
    <?= @$_footer ?>
  </div>
  
  <?= @$_js ?>
</body>
</html>