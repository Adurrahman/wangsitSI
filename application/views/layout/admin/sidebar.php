<aside class="mdc-drawer mdc-drawer--dismissible mdc-drawer--open">
  <div class="mdc-drawer__header">
    <a href="<?= base_url('admin/dashboard') ?>" class="brand-logo">
      <img src="<?= base_url('asset/img/text-logo-white.png') ?>" class="img-fluid" alt="logo">
    </a>
  </div>
  <div class="mdc-drawer__content">
    <div class="mdc-list-group">
      <nav class="mdc-list mdc-drawer-menu">
        <div class="mdc-list-item mdc-drawer-item">
          <a class="mdc-drawer-link" href="<?= base_url('/admin/dashboard') ?>">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">home</i>
            Dashboard
          </a>
        </div>

        <div class="mdc-list-item mdc-drawer-item <?= isResearchDepartment() ? '' : 'd-none' ?>">
          <a class="mdc-expansion-panel-link" href="#" data-toggle="expansionPanel" data-target="wangsitAcademy">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">book</i>
            Wangsit Academy
            <i class="mdc-drawer-arrow material-icons">chevron_right</i>
          </a>
          <div class="mdc-expansion-panel" id="wangsitAcademy">
            <nav class="mdc-list mdc-drawer-submenu">
              <div class="mdc-list-item mdc-drawer-item">
                <a class="mdc-drawer-link" href="<?= base_url('admin/wangsitAcademy') ?>">
                  Daftar Materi
                </a>
              </div>
              <div class="mdc-list-item mdc-drawer-item">
                <a class="mdc-drawer-link" href="<?= base_url('/admin/academyCategory') ?>">
                  Kategori Materi
                </a>
              </div>
            </nav>
          </div>
        </div>

        <div class="mdc-list-item mdc-drawer-item">
          <a class="mdc-drawer-link" href="<?= base_url('/admin/events') ?>">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">event</i>
            Program Kerja
          </a>
        </div>

        <div class="mdc-list-item mdc-drawer-item <?= isAuthorized() ? '' : 'd-none' ?>">
          <a class="mdc-drawer-link" href="<?= base_url('/admin/divisions') ?>">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true"> group_work </i>
            Komisi / Department
          </a>
        </div>

        <div class="mdc-list-item mdc-drawer-item <?= isAuthorized() ? '' : 'd-none' ?>">
          <a class="mdc-drawer-link" href="<?= base_url('/admin/members') ?>">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true"> group </i>
            Daftar Anggota
          </a>
        </div>

        <div class="mdc-list-item mdc-drawer-item <?= isResearchDepartment() || isAuthorized() ? '' : 'd-none' ?>">
          <a class="mdc-expansion-panel-link" href="#" data-toggle="expansionPanel" data-target="ui-sub-menu">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">account_circle</i>
            KBMSI
            <i class="mdc-drawer-arrow material-icons">chevron_right</i>
          </a>
          <div class="mdc-expansion-panel" id="ui-sub-menu">
            <nav class="mdc-list mdc-drawer-submenu">
              <div class="mdc-list-item mdc-drawer-item">
                <a class="mdc-drawer-link" href="<?= base_url('admin/users') ?>">
                  Data KBMSI
                </a>
              </div>
              <div class="mdc-list-item mdc-drawer-item <?= isResearchDepartment() ? '' : 'd-none' ?>">
                <a class="mdc-drawer-link" href="<?= base_url('/admin/activation') ?>">
                  Aktivasi
                </a>
              </div>
              <div class="mdc-list-item mdc-drawer-item <?= isResearchDepartment() ? '' : 'd-none' ?>">
                <a class="mdc-drawer-link" href="<?= base_url('admin/editUser') ?>">
                  Edit Data
                </a>
              </div>
              <div class="mdc-list-item mdc-drawer-item">
                <a class="mdc-drawer-link" href="<?= base_url('admin/bannedUsers') ?>">
                  KBMSI Nakal
                </a>
              </div>
            </nav>
          </div>
        </div>

        <div class="mdc-list-item mdc-drawer-item">
          <a class="mdc-drawer-link" href="#" target="_blank">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">description</i>
            Documentation
          </a>
        </div>
      </nav>
    </div>
  </div>
</aside>