<script src="<?= base_url('/assets/admin/vendors/js/vendor.bundle.base.js')?>"></script>
<script src="<?= base_url('/assets/admin/vendors/chartjs/Chart.min.js')?>"></script>
<script src="<?= base_url('/assets/admin/vendors/jvectormap/jquery-jvectormap.min.js')?>"></script>
<script src="<?= base_url('/assets/admin/vendors/jvectormap/jquery-jvectormap-world-mill-en.js')?>"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?= base_url('assets/admin/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script src="<?= base_url('/assets/admin/js/material.js')?>"></script>
<script src="<?= base_url('/assets/admin/js/misc.js')?>"></script>
<script src="<?= base_url('/assets/admin/js/app.alert.js')?>"></script>
<script src="<?= base_url('/assets/admin/js/dashboard.js')?>"></script>
<script src="<?= base_url('/assets/admin/js/app.admin.js')?>"></script>