<!DOCTYPE html>
<html lang="en">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
	<title>WANGSIT AUTH</title>
	<?php $this->load->view('layout/css'); ?>
	<link href="asset/css/materialize.min.css" rel="stylesheet" type="text/css">
</head>

<body>

	<div class="parallax-w">
		<div class="container">
			<div class="section">
				<div class="row">
					<div class="col s12 center-align" style="margin-bottom: 20px;">
						<h4 class="fontt" style="vertical-align: middle"><b><img class="center-align" src="asset/img/text-logo.png" style="width: 200px; vertical-align: middle" alt=""></b></h4>
					</div>
					<?php if (isset($errorMessage)) { ?>
						<div class="col s12" style="margin-top: 25px;">
							<div class="chip red white-text" style="width: 100%; border-radius: 5px;">
								<?= $errorMessage ?>
								<i class="close material-icons">close</i>
							</div>
						</div>
					<?php } ?>
					<div class="col m6 hide-on-small-and-down">
						<img class="center-block" src="asset/img/ilustrasi.png" style="width: 100%;" alt="">
						<h5 class="fontt"><b>Wangsit Auth</b></h5>
						<p class="">Adalah sebuah sistem autentikasi mahasiswa Sistem Informasi Universitas Brawijaya, yang dapat digunakan oleh beberapa aplikasi-aplikasi lain yang ada di KBMSI</p>
					</div>
					<div class="col m1"></div>
					<div class="col s12 m5 white-text">
						<div class="card z-depth-3">
							<div class="card-content" style="height: 60px; background-color: #54264E;">
								<span class="card-title fontt center-align white-text" style="margin-top: -10px;"><b>Login</b></span>
							</div>
							<div class="card-content wow-text" style="padding-left: 40px; padding-right: 40px; padding-bottom: 10px;">
								<form action="" method="post">
									<div class="row">
										<div class="input-field col s12">
											<input name="NIM" type="text" class="validate" placeholder="">
											<label for="icon_prefix" style="background-color: transparent; color:#54264E !important">NIM atau Username</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12" style="margin-top: 0px;">
											<input name="PASSWD" type="password" class="validate" placeholder="">
											<label for="icon_prefix" style="background-color: transparent; color:#54264E !important">Password</label>
										</div>
										<div class="col s12 left-align" style="margin-bottom: 0px;">
											<input type="checkbox" id="test5" />
											<label style="font-size: 12px;" for="test5">Remember Me</label>
										</div>
									</div>
									<div class="row">
										<button type="submit" name="btn_login" class="col s12 btn btn-wangsit btn-large waves-effect wangsit-color" style="background-color: #54264E;">Login</button>
									</div>
								</form>
								<div class="row" style="margin-bottom: 10px;">
									<div class="col s5 center-align">
										<div style="border-top: 2px rgba(131,131,131,0.64) solid"></div>
									</div>
									<center>
										<div class="col s2" style="padding: 0; margin-top: -13px">
											<p class="center-align fontt">Atau</p>
										</div>
									</center>
									<div class="col s5 center-align">
										<div style="border-top: 2px rgba(131,131,131,0.64) solid"></div>
									</div>

								</div>
								<div class="row">
									<a href="<?php echo $loginURL; ?>"><button class="col s12 btn btn-large waves-effect red">daftar / Masuk dengan Google</button></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row white-text center-align grey-text">
					<div class="col s12" style="color:#55254e;">
						Made with <b>&#x2764; </b> by <a class="brown-text text-lighten-3" href="http://kbmsi.filkom.ub.ac.id/kelembagaan/emsi/p2s/" style="color:#55254e">P2S EMSI 2021</a>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!--  Scripts-->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="asset/js/materialize.js"></script>
	<script src="asset/js/init.js"></script>

</html>