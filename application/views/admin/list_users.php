<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
    <div class="mdc-layout-grid">
      <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
          <div class="mdc-card">
            <div class="container">
              <div class="table-responsive">
                <table id="members" class="table table-striped text-left">
                  <thead>
                    <tr>
                      <th> No </th>
                      <th> Nama Lengkap</th>
                      <th> NIM </th>
                      <th> Angkatan </th>
                      <th> Email </th>
                      <th> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($users as $index => $user) : ?>
                    <tr>
                      <td> <?= $index + 1 ?> </td>
                      <td> <?= $user->NAMA ?> </td>
                      <td> <?= $user->NIM ?> </td>
                      <td> <?= getGeneration($user->NIM) ?> </td>
                      <td> <?= $user->EMAIL ?> </td>
                      <td> 
                        <?php if($action == 'detail') : ?>
                          <div class="d-inline">
                            <button onClick="bannedAccount(<?= $user->ID ?>)" class="mdc-button mdc-button--outlined outlined-button--secondary mdc-ripple-upgraded <?= isResearchDepartment() ? '' : 'd-none' ?> "> Banned </button> 
                            <a href="javscript:void(0)" class="text-decoration-none"> <button class="mdc-button mdc-button--raised filled-button--info mdc-ripple-upgraded"> Detail </button> </a>
                          </div>
                          <?php elseif($action == 'activate') : ?>
                            <button onClick="activateAccount(<?= $user->ID ?>)" class="mdc-button mdc-button--raised filled-button--info mdc-ripple-upgraded"> Activate </button> 
                          <?php elseif($action == 'banned') : ?>
                            <button onClick="restoreAccount(<?= $user->ID ?>)" class="mdc-button mdc-button--raised filled-button--info mdc-ripple-upgraded <?= isResearchDepartment() ? '' : 'd-none' ?>"> Restore </button> 
                          <?php elseif($action == 'edit') : ?>
                            <a href="javscript:void(0)" class="text-decoration-none"> <button class="mdc-button mdc-button--raised filled-button--info mdc-ripple-upgraded"> Edit Data </button> </a>
                          <?php endif ?>
                      </td>
                    </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>

<!-- Modal -->
<div class="modal fade" id="addNewMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Anggota</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('admin/addMember') ?>" method="post">
        <div class="modal-body">
          <div class="container">
            <label for=""> Pilih Anggota </label>
            <select name="id_user" class="selectpicker form-control" data-live-search="true" title="Pilih Calon Anggota">
              <?php foreach($candidates as $candidate) : ?>
                <option value="<?= $candidate->id_user ?>"> <?= $candidate->fullname ?> _ <?= $candidate->nim ?>  </option>
              <?php endforeach ?>
            </select>

            <?php if(strtolower($this->session->userdata('admin')->division) == 'non-department' || $this->session->userdata('admin')->id_user == 14) : ?>
              <label for="" class="mt-4"> Pilih Department/Komisi </label>
              <select name="id_division" class="selectpicker form-control" data-live-search="true" title="Pilih Department/komisi">
                <?php foreach($divisions as $division) : ?>
                  <option value="<?= $division->id_division ?>"> <?= $division->name ?> </option>
                <?php endforeach ?>
              </select>

              <label for="" class="mt-4"> Pilih Posisi </label>
              <select name="id_role" class="selectpicker form-control" data-live-search="true" title="Pilih Posisi">
              <?php foreach($roles as $role) : ?>
                <option value="<?= $role->id_role ?>"> <?= $role->name ?> </option>
              <?php endforeach ?>
              </select>
            <?php endif ?>  

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="mdc-button text-button--dark mdc-ripple-upgraded" data-dismiss="modal"> Batal </button>
          <button type="submit" class="btn btn-success"> Tambah </button>
        </div>
      </form>  
    </div>
  </div>
</div>