<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
    <div class="mdc-layout-grid">
      <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
          <div class="mdc-card p-0">
            <div class="d-inline p-3">
              <h6 class="d-inline card-title"> Daftar Anggota </h6>
              <button data-toggle="modal" data-target="#addNewMember" class="mdc-button mdc-button--outlined mdc-ripple-upgraded float-right modal-trigger <?= isAuthorized() ? '' : 'd-none' ?> ">
                <i class="material-icons mdc-button__icon">add</i>
                Tambah Anggota
              </button>
            </div>
            <div class="container pb-4">
              <div class="table-responsive">
                <table id="members" class="table table-striped text-left">
                  <thead>
                    <tr>
                      <th> No </th>
                      <th> Nama Lengkap</th>
                      <th> NIM </th>
                      <th> Angkatan </th>
                      <th> Department/Komisi </th>
                      <th> status </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($members as $index => $member) : ?>
                    <tr>
                      <td> <?= $index + 1 ?> </td>
                      <td> <?= $member->fullname ?> </td>
                      <td> <?= $member->nim ?> </td>
                      <td> <?= getGeneration($member->nim) ?> </td>
                      <td> <?= $member->division ?> </td>
                      <td> <?= $member->role ?> </td>
                    </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>

<!-- Modal -->
<div class="modal fade" id="addNewMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Anggota</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('admin/addMember') ?>" method="post">
        <div class="modal-body">
          <div class="container">
            <label for=""> Pilih Anggota </label>
            <select name="id_user" class="selectpicker form-control" data-live-search="true" title="Pilih Calon Anggota">
              <?php foreach($candidates as $candidate) : ?>
                <option value="<?= $candidate->id_user ?>"> <?= $candidate->fullname ?> _ <?= $candidate->nim ?>  </option>
              <?php endforeach ?>
            </select>

            <?php if(strtolower($this->session->userdata('admin')->division) == 'non-department' || $this->session->userdata('admin')->id_user == 14) : ?>
              <label for="" class="mt-4"> Pilih Department/Komisi </label>
              <select name="id_division" class="selectpicker form-control" data-live-search="true" title="Pilih Department/komisi">
                <?php foreach($divisions as $division) : ?>
                  <option value="<?= $division->id_division ?>"> <?= $division->name ?> </option>
                <?php endforeach ?>
              </select>

              <label for="" class="mt-4"> Pilih Posisi </label>
              <select name="id_role" class="selectpicker form-control" data-live-search="true" title="Pilih Posisi">
              <?php foreach($roles as $role) : ?>
                <option value="<?= $role->id_role ?>"> <?= $role->name ?> </option>
              <?php endforeach ?>
              </select>
            <?php endif ?>  

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="mdc-button text-button--dark mdc-ripple-upgraded" data-dismiss="modal"> Batal </button>
          <button type="submit" class="btn btn-success"> Tambah </button>
        </div>
      </form>  
    </div>
  </div>
</div>