<div class="body-wrapper">
  <div class="main-wrapper">
    <div class="page-wrapper full-page-wrapper d-flex align-items-center justify-content-center">
      <main class="auth-page">
        <div class="mdc-layout-grid">
          <div class="mdc-layout-grid__inner">
            <div class="stretch-card mdc-layout-grid__cell--span-4-desktop mdc-layout-grid__cell--span-1-tablet"></div>
            <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-4-desktop mdc-layout-grid__cell--span-6-tablet">
              <div class="mdc-card">
                <img src="<?= base_url('asset/img/ilustrasi.png') ?>" width="100%" alt="logo-wangsit">
                
                <?php if(isset($_SESSION['message_content'])) : ?>
                  <div class="alert alert-danger" role="alert">
                    <?= $_SESSION['message_content'] ?>
                    <?php unset($_SESSION['message_content']) ?>
                  </div>
                <?php endif ?>

                <form method="POST" action="<?= base_url('admin-login') ?>">
                  <div class="mdc-layout-grid">
                    <div class="mdc-layout-grid__inner">
                      <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
                        <div class="mdc-text-field w-100">
                          <input class="mdc-text-field__input" id="text-field-hero-input" name="username">
                          <div class="mdc-line-ripple"></div>
                          <label for="text-field-hero-input" class="mdc-floating-label">Username</label>
                        </div>
                      </div>
                      <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
                        <div class="mdc-text-field w-100">
                          <input class="mdc-text-field__input" type="password" name="password" id="text-field-hero-input">
                          <div class="mdc-line-ripple"></div>
                          <label for="text-field-hero-input" class="mdc-floating-label">Password</label>
                        </div>
                      </div>

                      <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12 mt-4">
                        <button type="submit" name="submit" class="mdc-button mdc-button--raised w-100 btn-wangsit-color"> Login </button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="stretch-card mdc-layout-grid__cell--span-4-desktop mdc-layout-grid__cell--span-1-tablet"></div>
          </div>
        </div>
      </main>
    </div>
  </div>
</div>