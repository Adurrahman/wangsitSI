<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
    <div class="mdc-layout-grid">
      <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
          <div class="mdc-card p-0">
            <div class="d-inline p-3">
              <h6 class="d-inline card-title"> Daftar Kategori Academy </h6>
              <button data-toggle="modal" data-target="#academyCategory" class="mdc-button mdc-button--outlined mdc-ripple-upgraded float-right modal-trigger <?= isResearchDepartment() ? '' : 'd-none' ?> ">
                <i class="material-icons mdc-button__icon">add</i>
                Tambah Kategori
              </button>
            </div>
            <div class="container pb-4">
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="academy" class="table table-striped text-left">
                      <thead>
                        <tr>
                          <th> No </th>
                          <th> Nama Category</th>
                          <th> Aksi </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($categories as $index => $category) : ?>
                        <tr>
                          <td> <?= $index + 1 ?> </td>
                          <td> <?= $category->category ?> </td>
                          <td> 
                            <button onclick="disableCategory(<?= $category->id ?>)" class="mdc-button mdc-button--outlined outlined-button--secondary mdc-ripple-upgraded"> Delete </button>
                            <button onclick="updateCategory(this)" data-id="<?= $category->id ?>" data-name="<?= $category->category ?>" class="mdc-button mdc-button--raised filled-button--info mdc-ripple-upgraded"> Update </button>
                          </td>
                        </tr>
                        <?php endforeach ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>

<!-- Modal -->
<div class="modal fade" id="academyCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Anggota</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="academyForm" action="<?= base_url('Admin/Academy/addCategory') ?>" method="post">
        <div class="modal-body">
          <div class="container">
            <input type="text" hidden id="id_category" name="id" >
            <div class="form-group">
              <label for="name">Nama Kategori</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="E.g Pemrograman Dasar">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="mdc-button text-button--dark mdc-ripple-upgraded" data-dismiss="modal"> Batal </button>
          <button id="action" type="submit" class="btn btn-success"> Tambah </button>
        </div>
      </form>  
    </div>
  </div>
</div>