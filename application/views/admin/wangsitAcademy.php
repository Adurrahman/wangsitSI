<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
    <div class="mdc-layout-grid">
      <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
          <div class="mdc-card p-0">
            <div class="d-inline p-3">
              <h6 class="d-inline card-title"> Daftar Materi Academy </h6>
              <button data-toggle="modal" data-target="#academyCourse" class="mdc-button mdc-button--outlined mdc-ripple-upgraded float-right modal-trigger <?= isResearchDepartment() ? '' : 'd-none' ?> ">
                <i class="material-icons mdc-button__icon">add</i>
                Tambah Modul
              </button>
            </div>
            <div class="container pb-4">
              <div class="table-responsive">
                <table id="academy" class="table table-striped text-left">
                  <thead>
                    <tr>
                      <th> No </th>
                      <th> Nama Materi</th>
                      <th> Alias </th>
                      <th> Kategori </th>
                      <th> Jumlah Akses </th>
                      <th>  </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($lessons as $index => $lesson) : ?>
                    <tr>
                      <td> <?= $index + 1 ?> </td>
                      <td> <?= $lesson->name ?> </td>
                      <td> <?= $lesson->alias ?> </td>
                      <td> <?= $lesson->category ?> </td>
                      <td> <?= $lesson->visited ?> </td>
                      <td> 
                        <button onclick="disableCourse(<?= $lesson->id ?>)" class="mdc-button mdc-button--outlined outlined-button--secondary mdc-ripple-upgraded"> Delete </button>
                        <button onclick="updateCourse(this)" data-id="<?= $lesson->id ?>" data-name="<?= $lesson->name ?>" data-alias="<?= $lesson->alias ?>" data-category="<?= $lesson->category ?>" data-link="<?= $lesson->link ?>" class="mdc-button mdc-button--raised filled-button--info mdc-ripple-upgraded"> Update </button>
                      </td>
                    </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>

<!-- Modal -->
<div class="modal fade" id="academyCourse" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Anggota</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="academyForm" action="<?= base_url('admin/academy/addAcademy') ?>" method="post">
        <input type="text" hidden name="id" id="id_course">
        <div class="modal-body">
          <div class="container">
            <div class="form-group">
              <label for="name">Nama Materi</label>
              <input type="text" class="form-control" id="name"  name="name" placeholder="E.g Pemrograman Dasar">
            </div>
            <div class="form-group">
              <label for="alias">Nama Alias</label>
              <input type="text" class="form-control" id="alias" name="alias" placeholder="E.g Pemdas">
            </div>
            <div class="form-group">
              <label for="category">Kategori</label>
              <select name="category" class="selectpicker form-control" data-live-search="true" title="Pilih Kategori">
                <?php foreach($categories as $category) : ?>
                  <option value="<?= $category->id ?>"> <?= $category->category ?> </option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="form-group">
              <label for="link"> Link Drive </label>
              <input type="text" class="form-control" id="link" name="link" placeholder="E.g https://drive.google.com/link">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="mdc-button text-button--dark mdc-ripple-upgraded" data-dismiss="modal"> Batal </button>
          <button type="submit" class="btn btn-success"> Tambah </button>
        </div>
      </form>  
    </div>
  </div>
</div>