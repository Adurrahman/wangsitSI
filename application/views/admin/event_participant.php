<!DOCTYPE html>
<html>
<head>
	<title>admin</title>
	<?php $this->load->view('layout/Styling'); ?>
	<link href="<?= base_url('asset/plugin/data-tables/css/jquery.dataTables.min.css')?>" type="text/css" rel="stylesheet" media="screen,projection"/> 
</head>
<body>
<!-- Dropdown Test -->
  <ul id="dropdown2" class="dropdown-content">
    <li><a href="<?= base_url("/logout")?>" class="wangsit-text"><i class="material-icons">exit_to_app</i>Log Out</a></li>
  </ul>
<!-- Dropdown Test -->

<div class="navbar-fixed">
	<nav class="z-depth-1 white">
	  <div class="nav-wrapper white">
	   <div class="container">

		<a href="#" class="brand-logo fontt"><img class="center-align" src="asset/img/logoo.png" style="width: 45px; " alt=""></a>
		<ul class="right hide-on-med-and-down" style="margin-right: -100px;">

			<li><a class="dropdown-trigger" href="#!" data-target="dropdown2"><b>admin</b><i class="material-icons right" style="margin-left: 10px;">arrow_drop_down</i></a></li>
		</ul>
		<div id="nav-mobile" class="right hide-on-med-and-down" style="margin-right: 30px">
		</div>
		</div>
	  </div>
	</nav>
</div>


	<div class="container">
		<div class="row">
			<div class="col s12">
				<div style="margin-top: 50px">
				<table id="data-table-simple" class="striped">
				  <thead>
					<tr>
					  <th>NO</th>
					  <th>NIM</th>
					  <th>NAMA</th>
					  <th>No HP</th>
					  <th>id_line</th>
					</tr>
				  </thead>
				  <tbody>
					<?php $i=1; foreach($participants as $participant) { ?>
						<tr>
							<td><?= $i; ?></td>
							<td><?= $participant->NIM ?></td>
							<td><?= $participant->NAMA ?></td>
							<td><?= $participant->HP ?></td>
							<td><?= $participant->id_line ?></td>                       	
						</tr>
					<?php $i++; } ?>
				  </tbody> 
				</table>
				</div>
			</div>
		</div>
	</div>
	<!--  Scripts-->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="<?php echo base_url();?>asset/js/materialize.js"></script>
	<script src="<?php echo base_url();?>asset/plugin/data-tables/js/jquery.dataTables.min.js"></script>
  	<script src="<?php echo base_url();?>asset/plugin/data-tables/js/data-tables.js"></script>
	<script src="<?php echo base_url();?>asset/js/init.js"></script>

	<script>
		$('.status').hover(function(){
			if($(this).html() == 'done'){
				$(this).html('highlight_off');
				$(this).parent().attr('style','background-color : #F44336 !important');
			} else {
				$(this).html('done');
				$(this).parent().attr('style','background-color : #4CAF50 !important');
			}
		})
	</script>
							
</body>
</html>