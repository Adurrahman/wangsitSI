<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
    <div class="mdc-layout-grid">
      <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
          <div class="mdc-card p-0">
            <div class="d-inline p-3">
              <h4 class="d-inline card-title"> Informasi umum <?= $event->name ?> </h4>
              <button data-toggle="modal" data-target="#editEvent" class="mdc-button mdc-button--outlined mdc-ripple-upgraded float-right modal-trigger  ">
                <i class="material-icons mdc-button__icon"> edit </i>
                Edit Detail
              </button>
            </div>
            <div class="container pb-3">
              <div class="row">
                <div class="col-lg-3 col-md-3 col-12">
                  <form id="imagePoster">
                    <input type="file" class="dropify" name="event_poster" data-height="150" data-default-file="<?= base_url( isset($event->img_path) ? $event->img_path : 'asset/img/event/default.png') ?>" />
                  </form>
                </div>
                <div class="col-lg-9 col-md-9 col-12">
                  <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                      <div class="mb-4">
                        <h5> Nama Kegiatan</h5>
                        <p> <?= $event->name ?> </p>
                      </div>

                      <div class="mb-4">
                        <h5> Deskripsi kegiatan</h5>
                        <p> <?= $event->description ?? '-' ?> </p>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                      <div class="mb-4">
                        <h5> Nama Penanggung Jawab </h5>
                        <p> <?= $_SESSION['admin']->fullname ?> </p>
                      </div>

                      <div class="mb-4">
                        <h5> Jumlah Quota Peserta </h5>
                        <p> <?= $event->quota ?? '-' ?> </p>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>

<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
    <div class="mdc-layout-grid">
      <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
          <div class="mdc-card p-0">
            <div class="d-inline p-3">
              <h4 class="d-inline card-title"> Pelaksanaan Kegiatan <?= $event->name ?> </h4>
              <button data-toggle="modal" data-target="#editEventTime" class="mdc-button mdc-button--outlined mdc-ripple-upgraded float-right modal-trigger  ">
                <i class="material-icons mdc-button__icon"> edit </i>
                Edit Detail
              </button>
            </div>
            <div class="container pb-3">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                  <div class="mb-4">
                    <h5> Awal Pelaksanaan Kegiatan </h5>
                    <p> <?= $event->start_date ?? '-' ?> </p>
                  </div>

                  <div class="mb-4">
                    <h5> Akhir Pelaksanaan Kegiatan </h5>
                    <p> <?= $event->end_date ?? '-' ?> </p>
                  </div>
                </div>
                
                <div class="col-lg-6 col-md-6 col-12">
                  <div class="mb-4">
                    <h5> Tanggal Pendaftaran </h5>
                    <p> <?= $event->open_registration ?? '-' ?> </p>
                  </div>

                  <div class="mb-4">
                    <h5> Tanggal Pendaftaran </h5>
                    <p> <?= $event->close_registration ?? '-' ?> </p>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>


<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
    <div class="mdc-layout-grid">
      <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
          <div class="mdc-card p-0">
            <div class="d-inline p-3">
              <h6 class="d-inline card-title"> Data yang ingin diambil </h6>
              <a href="<?= base_url("admin/forms/" . $event->id_event) ?>">
                <button class="mdc-button mdc-button--outlined mdc-ripple-upgraded float-right modal-trigger">
                  <i class="material-icons mdc-button__icon"> edit </i>
                  Edit form
                </button>
              </a>
            </div>
            <div class="container pb-4">
              <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                  <div id="accordion">
                    <div class="card collapse-item">
                      <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                          <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Collapsible Group Item #1
                          </button>
                        </h5>
                      </div>
      
                      <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>


<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
    <div class="mdc-layout-grid">
      <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
          <div class="mdc-card p-0">
            <div class="d-inline p-3">
              <h6 class="d-inline card-title"> Peserta / Participant </h6>
              <button class="mdc-button mdc-button--outlined mdc-ripple-upgraded float-right modal-trigger">
                <i class="material-icons mdc-button__icon"> import_export </i>
                Export Data
              </button>
            </div>
            <div class="container pb-4">
              <div class="table-responsive">
                <table id="members" class="table table-striped text-left">
                  <thead>
                    <tr>
                      <th> No </th>
                      <th> Nama Lengkap</th>
                      <th> NIM </th>
                      <th> Angkatan </th>
                      <th> Email </th>
                      <th> No.Telp </th>
                      <th> id line </th>
                      <th> action </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($participants as $index => $participant) : ?>
                    <tr>
                      <td> <?= $index + 1 ?> </td>
                      <td> <?= $participant->name ?> </td>
                      <td> <?= $participant->nim ?> </td>
                      <td> <?= getGeneration($participant->nim) ?> </td>
                      <td> <?= $participant->email ?> </td>
                      <td> <?= $participant->phone ?> </td>
                      <td> <?= $participant->id_line ?> </td>
                      <td> <button class="btn btn-outline-primary btn-sm"> Detail </button> </td>
                    </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>

<!-- Modal -->
<div class="modal fade" id="editEvent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle"> Ubah Informasi Kegiatan </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('admin/updateEvent') ?>" method="post">
        <div class="modal-body">
          <div class="container">

            <input type="hidden" name="id_event" value="<?= $event->id_event ?>">

            <div class="form-group">
              <label for="name"> Nama Kegiatan </label>
              <input type="text" name="name" id="name" value="<?= $event->name ?>" disabled class="form-control" placeholder="E.g Event legendaris">
            </div>

            <div class="form-group">
              <label for="name"> Deskripsi Kegiatan </label>
              <textarea name="description" id="desc" class="form-control" rows="5" placeholder="Kegiatan ini adalah kegiatan yang ..."><?= $event->description ?? null ?></textarea>
            </div>


            <div class="form-group">
              <label for="quota"> Jumlah Quota Kegiatan </label>
              <input type="text" name="quota" id="quota" value="<?= $event->quota ?>" class="form-control" placeholder="E.g 230">
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="mdc-button text-button--dark mdc-ripple-upgraded" data-dismiss="modal"> Batal </button>
          <button type="submit" class="btn btn-success"> Simpan </button>
        </div>
      </form>  
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editEventTime" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle"> Ubah Informasi Kegiatan </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('admin/updateEvent') ?>" method="post">
        <div class="modal-body">
          <div class="container">
            <input type="hidden" name="id_event" value="<?= $event->id_event ?>">

            <div class="form-group">
              <label for="close_registration"> Awal kegiatan </label>
              <input type="text" name="start_date" id="start_date" class="form-control datepicker" autocomplete="off" placeholder="YYYY-MM-DD" value="<?= $event->start_date ?>">
            </div>

            <div class="form-group">
              <label for="close_registration"> Akhir kegiatan </label>
              <input type="text" name="end_date" id="end_date" class="form-control datepicker" autocomplete="off" placeholder="YYYY-MM-DD" value="<?= $event->end_date ?>">
            </div>

            <div class="form-group">
              <label for="open_registration"> Pendaftaran Kegiatan </label>
              <input type="text" name="open_registration" id="open_registration" class="form-control datepicker" autocomplete="off" placeholder="YYYY-MM-DD" value="<?= $event->open_registration ?>">
            </div>

            <div class="form-group">
              <label for="close_registration"> Penutupan Pendaftaran </label>
              <input type="text" name="close_registration" id="close_registration" class="form-control datepicker" autocomplete="off" placeholder="YYYY-MM-DD" value="<?= $event->close_registration ?>">
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="mdc-button text-button--dark mdc-ripple-upgraded" data-dismiss="modal"> Batal </button>
          <button type="submit" class="btn btn-success"> Simpan </button>
        </div>
      </form>  
    </div>
  </div>
</div>

