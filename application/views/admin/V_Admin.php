<!DOCTYPE html>
<html>
<head>
	<title>admin</title>
	<?php $this->load->view('layout/Styling'); ?>
	<link href="<?= base_url('asset/plugin/data-tables/css/jquery.dataTables.min.css')?>" type="text/css" rel="stylesheet" media="screen,projection"/> 
</head>
<body>
<!-- Dropdown Test -->
  <ul id="dropdown2" class="dropdown-content">
    <li><a href="<?= base_url("/logout")?>" class="wangsit-text"><i class="material-icons">exit_to_app</i>Log Out</a></li>
  </ul>
<!-- Dropdown Test -->

<div class="navbar-fixed">
	<nav class="z-depth-1 white">
	  <div class="nav-wrapper white">
	   <div class="container">

		<a href="#" class="brand-logo fontt"><img class="center-align" src="asset/img/logoo.png" style="width: 45px; " alt=""></a>
		<ul class="right hide-on-med-and-down" style="margin-right: -100px;">

			<li><a class="dropdown-trigger" href="#!" data-target="dropdown2"><b>admin</b><i class="material-icons right" style="margin-left: 10px;">arrow_drop_down</i></a></li>
		</ul>
		<div id="nav-mobile" class="right hide-on-med-and-down" style="margin-right: 30px">
		</div>
		</div>
	  </div>
	</nav>
</div>


	<div class="container">
		<div class="row">
			<div class="col s12">
				<div style="margin-top: 50px">
				<table id="data-table-simple" class="striped">
				  <thead>
					<tr>
					  <th>NO</th>
					  <th>NIM</th>
					  <th>NAMA</th>
					  <th>USERNAME</th>
					  <th>TGL LAHIR</th>
					  <th>HP</th>
					  <th style="width: 5px;">Status</th>
					  <th style="width: 5px;">Edit</th>
					</tr>
				  </thead>
				  <tbody>
					<?php $i=1; foreach($mhs as $peserta) { ?>
                   	<tr>
                   		<td><?= $i; ?></td>
											<td><?= $peserta->NIM; ?></td>
											<td><?= $peserta->NAMA; ?></td>
											<td><?= $peserta->USERNAME; ?></td>
											<td><?= date_format(date_create($peserta->TTL),"d F Y"); ?></td>
											<td><?= $peserta->HP; ?></td>
                       	<?php if ($peserta->STATUS==1) { ?>
                       		<td><a href="?deaktivasi=<?= $peserta->ID;?>" class="waves-effect waves-light green btn"><i class="material-icons status">done</i></td>
                       	<?php } else {?>
                       		<td><a href="?aktivasi=<?= $peserta->ID;?>" class="waves-effect waves-light red btn"><i class="material-icons status">highlight_off</i></td>
                        <?php } ?>
                        <td><a href="<?= base_url();?>admin/Wakwaw/edit/<?= $peserta->ID;?>" class="waves-effect waves-light blue btn"><i class="material-icons">edit</i></td>
					</tr>
					<?php $i++; } ?>
				  </tbody> 
				</table>
				</div>
			</div>
		</div>
	</div>
	<!--  Scripts-->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="<?php echo base_url();?>asset/js/materialize.js"></script>
	<script src="<?php echo base_url();?>asset/plugin/data-tables/js/jquery.dataTables.min.js"></script>
  	<script src="<?php echo base_url();?>asset/plugin/data-tables/js/data-tables.js"></script>
	<script src="<?php echo base_url();?>asset/js/init.js"></script>

	<script>
		$('.status').hover(function(){
			if($(this).html() == 'done'){
				$(this).html('highlight_off');
				$(this).parent().attr('style','background-color : #F44336 !important');
			} else {
				$(this).html('done');
				$(this).parent().attr('style','background-color : #4CAF50 !important');
			}
		})
	</script>
							
</body>
</html>