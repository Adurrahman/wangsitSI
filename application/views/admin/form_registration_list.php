<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
    <div class="mdc-layout-grid">
      <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
          <div class="mdc-card p-0">
            <div class="container pt-4 pb-4">
              <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                  <div id="accordion">
                    <div class="card collapse-item">
                      <div class="card-header" id="headingOne">
                        <button class="btn btn-block bg-transparent" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true">
                          Collapsible Group Item #1
                        </button>
                      </div>
      
                      <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Asperiores maxime laborum rem nostrum velit dolorem quis hic quae error laboriosam?
                          <button data-target="#headingOne" data-wrapper="#collapseOne" class="btn btn-outline-success btn-block mt-3 form-choice-trigger"> Pilih form </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>
