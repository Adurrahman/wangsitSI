<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
    <div class="mdc-layout-grid">
      <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
          <div class="mdc-card p-0">
            <div class="d-inline p-3">
              <h6 class="d-inline card-title"> Daftar Event </h6>
              <button data-toggle="modal" data-target="#addNewEvent" class="mdc-button mdc-button--outlined mdc-ripple-upgraded float-right modal-trigger <?= isAuthorized() ? '' : 'd-none' ?> ">
                <i class="material-icons mdc-button__icon">add</i>
                Tambah Event
              </button>
            </div>
            <div class="container pb-4">
              <div class="row mb-5">

                <?php foreach($events as $event) : ?>
                  <div class="col-lg-6 col-md-6 col-12 eventDetail mb-3">
                    <div class="card event-card shadow-sm border-0" data-id="<?= $event->id_event ?>" data-manager="<?= $event->ID ?>">
                      <div class="row event-card--body no-gutters">
                        <div class="col-md-4">
                          <img src="https://i.picsum.photos/id/861/536/354.jpg" class="card-img event-card--image" alt="">
                        </div>
                        <div class="col-md-8">
                          <div class="card-body">
                            <h5 class="card-title event-card--title"> <?= $event->name ?> </h5>
                            <p class="card-text"> <?= substr($event->description, 0, 75) ?>... </p>
                            <div class="event-card--manager">
                              <img src="<?= $event->GAMBAR ?>" class="event-card--manager-photo" alt="">
                              <div class="event-card--info-manager">
                                <p class="name"> <?= $event->NAMA ?>: </p>
                                <small class="text-muted"> <?= $event->NIM ?>: </small>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endforeach ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>


<!-- Modal -->
<div class="modal fade" id="addNewEvent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle"> Tambah Program Kerja </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('admin/addEvent') ?>" method="post">
        <div class="modal-body">
          <div class="container">

            <div class="form-group">
              <label for="name"> Nama Event </label>
              <input type="text" name="name" id="name" class="form-control" placeholder="E.g Event legendaris">
            </div>

            <label for=""> Pilih Penanggung Jawab </label>
            <select name="res_person" class="selectpicker form-control" data-live-search="true" title="Pilih Penanggung Jawab kegiatan">
              <?php foreach($members as $user) : ?>
                <option value="<?= $user->id_user ?>"> <?= $user->fullname ?> _ <?= $user->nim ?> </option>
              <?php endforeach ?>
            </select>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="mdc-button text-button--dark mdc-ripple-upgraded" data-dismiss="modal"> Batal </button>
          <button type="submit" class="btn btn-success"> Tambah </button>
        </div>
      </form>  
    </div>
  </div>
</div>