	<div class="container">
		<div class="section">
			<div class="row">
				<form method="POST">
				<div class="col s12">
					<div class="card">
						<div class="card-content" style="padding-bottom: 0px;">
							<div class="card-title">
								<b>Blog Post</b>
							</div>
							<div class="row" style="margin-top: 10px;">
								<div class="input-field col s12" style="margin-bottom: 0px;">
									<input name="judul" placeholder="Masukan Judul yang tepat untuk tulisan anda" id="input_text" type="text" class="validate" data-length="100" autocomplete="off">
									<label class="active" for="first_name">Judul</label>
								</div>
								<div class="input-field col s12">
									<select name="katagori">
									  <option value="Default" disabled >Choose your option</option>
									  <option value="General" selected >General</option>
									  <option value="Review">Review</option>
									  <option value="Akademik">Akademik</option>
									</select>
									<label>Katagori</label>
							  	</div>
							  	<div class="input-field col s12">
					            	<textarea name="desc" placeholder="Deskripsi yang akan di tampilkan di list article" id="textarea2" class="materialize-textarea" data-length="255"></textarea>
					            	<label for="textarea2">Description</label>
					          	</div>
							  	<div class="col s12">
							  		<label>Sharing Option :</label>	
							  	</div>
							  	
							  	<div class="input-field col s12">
							  		  <div class="switch">
									    <label>
									      Public
									      <input name="opsi" type="checkbox" value="PRVT" checked="checked">
									      <span class="lever"></span>
									      Private
									    </label>
									  </div>
									  <p class="grey-text" style="font-size: 12px;">private hanya tampil didalam halaman anda, tidak akan tampil di halaman article</p>
							  	</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col s12">
						<textarea name="artikel" id="summernote"></textarea>
				</div>
				<div id="modal1" class="modal">
	              <div class="modal-content center-align">
	                <h5>Data yang sudah di insert tidak dapat di ganti untuk saat ini, Apakah anda sudah yakin dengan data yang anda isi ? <br>
	                </h5>
	                <div class="row" style="margin-top: 20px;">
	                  <div class="col s6">
	                    <a style="width: 100%" class=" btn btn-wangsit btn-large waves-effect red modal-close">Tidak</a>
	                  </div>
	                  <div class="col s6">
	                    <button style="width: 100%" type="submit" name="submit" class=" btn btn-wangsit btn-large waves-effect green modal-close">Ya</button>
	                  </div>
	                </div>
	              </div>
	            </div>
				<div class="col s12 center-align" style="margin-top: 10px;">
					 <a href="#modal1" class="modal-trigger"><button class="btn btn-wangsit btn-large waves-effect wangsit-color">SUBMIT <i class="material-icons right">send</i></button></a>
				</div>
			</form>
			</div>
		</div>

		
	</div>