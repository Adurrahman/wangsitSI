	<div class="container">
		<div class="section">
			<div class="row">

				<div class="col s12" style="margin-top: 25px; display: <?= $isActiveUser ?>">
					<div class="chip red white-text" style="width: 100%; border-radius: 5px;">
						Akun anda belum teraktivasi, silakan menguhubungi staff P2S untuk melakukan aktivasi 
					</div>
				</div>

				<?php if (!isset($_SESSION['userData']) || $_SESSION['userData'] ==null ) { ?>
					<div class="col s12 l12">
					<?php } else { ?>
						<div class="col s12 l8">	
						<?php } ?>
						<div class="card">
							<ul class="tabs tabs-fixed-width wangsit-color white-text z-depth-1 scrollbar-ngilang">
								<li class="tab col s3 "><a class="<?= ($activeTab==0 ? "active" : ""); ?>" target="_self" href="<?= base_url();?>article/"><b>Semua</b></a></li>
								<li class="tab col s3 "><a class="<?= ($activeTab==1 ? "active" : ""); ?>" target="_self" href="<?= base_url();?>article/General"><b>General</b></a></li>
								<li class="tab col s3 "><a class="<?= ($activeTab==2 ? "active" : ""); ?>" target="_self" href="<?= base_url();?>article/Akademik"><b>Akademik</b></a></li>
								<li class="tab col s3 "><a class="<?= ($activeTab==3 ? "active" : ""); ?>" target="_self" href="<?= base_url();?>article/Review"><b>Review</b></a></li>
							</ul>	
						</div>

						<?php 
						if ($allArticle == null) { ?>
							<div class="card">
								<div class="card-content">
									Belum ada Article
								</div>
							</div>
							<?php
						}
						foreach ($allArticle as $article) { ?>

							<div class="card" style="margin-bottom: 0px;">		
								<div class="card-content" style="padding-bottom: 50px;">
									<div class="right grey-text"><?= date_format(date_create($article->TANGGAL),"d F Y");?></div>
									<div class="card-title" style="margin-bottom: 0px;"><b><?= $article->JUDUL;?></b></div>
									<div class="grey-text" style="margin-bottom: 10px;">By <?= $article->USERNAME;?></div>
									<div style="margin-bottom: 10px;">
										<p><?= $article->DESKRIPSI;?></p>
									</div>
									<div class="right">
										<a href="<?= base_url("$article->USERNAME/$article->SLUG")?>" class="btn wangsit-color">View More</a>
									</div>
								</div>

							</div>
						<?php } ?>
						<?= $pagination;?>
					</div>
					<?php if (isset($_SESSION['userData'])){ ?>


						<div class="col l4 s12 hide-on-med-and-down">
							<div class="card">
								<a href="<?= base_url("article/create"); ?>" class="<?= $isButtonActive;?> btn btn-large" style="width: 100%;"><i class="material-icons right">add</i>Add Article</a>
							</div>
							<div class="card">
								<div class="card-image" style="z-index: 0">
									<img src="<?= base_url();?>asset/img/bg-bawah.jpg" style="height: 120px;" alt="">
								</div>
								<div class="card-content" style="padding-bottom: 0px;">	
									<center>
										<div style="position: relative;">
											<img class="circle z-depth-3 materialboxed" src="<?= $_SESSION['userData']['GAMBAR']; ?>" style="border: solid white; width: 100px; height: 100px; margin-top: -75px;  z-index: 10;">
										</div>
									</center>
								<div class="center-align" style="margin-top: 20px;">
									<h5 class="font" style="font-size: 18px;"><b><?= $_SESSION['userData']['USERNAME'] ?></b></h5>
									<h5 class="font grey-text" style="font-size: 14px;"><?= $_SESSION['userData']['NAMA'] ?></h5>
									<h5 style="font-size: 14px"><?= $_SESSION['userData']['NIM'] ?></h5>
								</div>
							</div>
							<div class="card-action" style="padding-bottom: 5px; margin-top: 20px;">
								<div class="row" style="margin-left: -20px; margin-right: -20px;">
									<div class="col s12 center-align">
										<i class="material-icons" style="font-size: 40px;">person</i>
										<div class="font">Angkatan <?= '20'.substr($_SESSION['userData']['NIM'],0,2); ?></div>
									</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>