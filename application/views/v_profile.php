<div class="parallax-container" style="height: 450px;">
	<div class="container" style="margin-top: 80px;">
		<div class="center">
			<img src="<?= $user->GAMBAR;?>" style="width: 100px;" class="circle center-align">

		</div>
		<h2 class="header center white-text logoo"><b><?= $user->NAMA; ?></b></h2>
		<div class="row center">
			<h5 class="header col s12 white-text">- <?= $user->USERNAME; ?> -</h5>
		</div>
	</div>
	<div class="parallax"><img src="<?= base_url('asset/img/bg.jpg');?>"></div>
</div>

<div class="container">
	<div class="section">
		<div class="row">
			<div class="col l8 s12">
				<div class="card">
					<div class="card-content wangsit-color white-text" style="padding: 10px 30px 10px 15px;;">
						<div class="fontt"><b>Article</b></div>
					</div>
				</div>

				<?php if ($Article == null) { ?>
					
					<div class="card">
						<div class="card-content">
							Belum ada Article
						</div>
					</div>

				<?php } ?>
				<?php foreach ($Article as $key) { ?>

					<div class="card" style="margin-bottom: 0px;">		
						<div class="card-content" style="padding-bottom: 50px;">
							<div class="right grey-text"><?= date_format(date_create($key->TANGGAL),"d F Y");?></div>
							<div class="card-title" style="margin-bottom: 0px;"><b><?= $key->JUDUL;?></b></div>
							<div class="grey-text" style="margin-bottom: 10px;">By <?= $key->USERNAME;?></div>
							<div style="margin-bottom: 10px;">
								<p><?= $key->DESKRIPSI;?></p>
							</div>
							<div class="right">
								<a href="<?= base_url();?><?= $key->USERNAME;?>/<?= $key->SLUG;?>" class="btn wangsit-color">View More</a>
							</div>
						</div>	
					</div>

				<?php } ?>

				<?= $pagination;?>
			</div>
			<div class="col l4 s12">
				<div class="card" style="display:<?= $isUserProfile ? 'block' : 'none'; ?> ">
					<a href="<?= base_url('Article/My/post') ?>" class="btn btn-large wangsit-color" style="width: 100%;">
						<i class="material-icons right">add</i> Add Article 
					</a>
				</div>

				<div class="card">
					<div class="card-image" style="z-index: 0">
						<img src="<?= base_url();?>asset/img/bg-bawah.jpg" style="height: 120px;" alt="">
					</div>
					<div class="card-content" style="padding-bottom: 0px;">	
						<center>
						<div style="position: relative;">
							<img class="circle z-depth-3 materialboxed" src="<?= $user->GAMBAR; ?>" style="border: solid white; width: 100px; height: 100px; margin-top: -75px;  z-index: 10;">
						</div>
						</center>
						<div class="center-align" style="margin-top: 20px;">
							<h5 class="font" style="font-size: 18px;"><b><?= $user->USERNAME; ?></b></h5>
							<h5 class="font grey-text" style="font-size: 14px;"><?= $user->NAMA; ?></h5>
							<h5 style="font-size: 14px"><?= $user->NIM; ?></h5>
						</div>
					</div>
					<div class="card-action" style="padding-bottom: 5px; margin-top: 20px;">
						<div class="row" style="margin-left: -20px; margin-right: -20px;">
							<div class="col s12 center-align">
								<i class="material-icons" style="font-size: 40px;">person</i>
								<div class="font">Angkatan <?= '20'.substr($user->NIM,0,2); ?></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
