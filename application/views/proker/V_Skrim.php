<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
	<title>Pendaftaran Skrim</title>
	<?php $this->load->view('Fragment/Styling'); ?> 
</head>

<body style="overflow: hidden;">
	<?php $this->load->view('Fragment/Navbar'); ?> 
	<div class="container">
		<div class="section">
			<div class="row">
				<div class="col s12">
					<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdClnkUhrYPk_mRjT_WEONX49uHlHyE7IUHy_g1ly8x-lL8LA/viewform?embedded=true" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height:89%;width:100%;position:absolute;top:70px;left:0px;right:0px; bottom: 0px;">Loading...</iframe>
				</div>
			</div>
		</div>
	</div>





	<!--  Scripts-->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="<?php echo base_url();?>asset/js/materialize.js"></script>

	<script src="<?php echo base_url();?>asset/js/owl.carousel.min.js"></script>
	<script>
		$(document).ready(function(){
			$('.owl-carousel').owlCarousel({
				loop: true,
				margin: 10,
				responsiveClass: true,
				responsive: {
					0: {
						items: 1,
						nav: true
					},
					600: {
						items: 3,
						nav: false
					},
					1000: {
						items: 3,
						nav: false,
						loop: false,
						margin: 10
					}
				}
			});
		});
	</script>
	<script src="<?php echo base_url();?>asset/js/init.js"></script>
</body>
</html>

