<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
  <link rel="stylesheet" href="<?= base_url('assets/eventPsdm/styleformulir.css'); ?>">

  <link rel="icon" type="image/png" href="#" style="width: 512px; height: 512px;">
  <link rel="icon" href="<?php echo base_url() ?>assets/starship/logo.png" />
  <title>STARSHIP 2021</title>
  <style>
    /* .bg {
      background-image: url("../starship/bg.png");
      margin-left: 0px;
      margin-top: 0px;
      object-fit: cover;
    } */

    .kata {
      font-family: 'Poppins', sans-serif !important;
      color: black;
      margin: auto;
      /* margin-top: 50px; */
      box-shadow: 2px 0px 10px rgb(135, 135, 135);
    }

    .kata h6 {
      font-weight: bold;
    }

    .kata img {
      width: 200px;
      margin: 10px;
    }

    .kata .date {
      margin: auto;
    }

    .isi-form {
      font-family: 'Poppins', sans-serif !important;
      font-size: 14px !important;
    }

    button {
      font-family: 'Poppins', sans-serif !important;
      width: 150px;
    }

    .img-thumbnail {
      background-color: transparent;
      border: none;
    }
  </style>
</head>

<body>
  <img id="aa" src="<?= base_url('assets/starship/bg.png'); ?>" alt="" style="height: 100vh !important;">

  <div class="container">
    <div class="row">
      <div class="col-12 text-center mt-3">
        <img class="img-fluid img-thumbnail" src="<?= base_url('assets/starship/judul.png'); ?>" alt="">
      </div>
    </div>
  </div>

  <div class="container">
    <div class="col-12 kata mb-5">
      <h6 class="mt-4 text-center" style="font-size: 25px;">Form Pendaftaran</h6>
      <div class="text-center download mt-4 mx-5">
        <span>Silahkan download berkas-berkas pendaftaran </span> <a target="_blank" href="https://drive.google.com/drive/folders/1fOdyaFzvXtFBnTbJu8mOUXPehZzcBVG3?usp=sharing")">disini</a>
      </div>


      <div class="isi-form my-3 text-left">
        <form action=" <?= base_url('event/submitStarship'); ?> " method="post">
          <div class="row">
            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label">Nama Lengkap</label>
              <input type="text" class="form-control" value="<?= $user->NAMA ?>" disabled>
            </div>
            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label">NIM</label>
              <input type="text" class="form-control" value="<?= $user->NIM ?>" disabled>
            </div>
            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label">ID LINE</label>
              <input type="text" class="form-control" name="idLine" value="<?= set_value('idLine'); ?>" required>
            </div>
            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label">Email</label>
              <input type="email" class="form-control" name="email" value="<?= set_value('email'); ?>" required>
            </div>
          </div>

          <h4 class="mt-3">Pilihan Departemen dan Komisi</h4>
          <p style="color: red;"><?= $this->session->flashdata('messagescreening'); ?></p>
          <div class="row">
            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label">Pilihan Departemen 1</label>
              <select class="form-control" name="dept1" required>
                <option value="" disabled selected hidden>Pilih departemen</option>
                <option value="Advokesma">Advokasi dan Kesejahteraan Mahasiswa</option>
                <option value="Sosma">Sosial Mahasiswa</option>
                <option value="PSDM">Pengembangan Sumber Daya Manusia</option>
                <option value="Medkominfo">Multimedia, Komunikasi dan Informasi</option>
                <option value="P2S">Penelitian dan Pengembangan Studi</option>
                <option value="KWU">Kewirausahaan</option>
              </select>
            </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label">Pilihan Komisi</label>
              <select class="form-control" name="komisi" required>
                <option value="" disabled selected hidden>Pilih komisi</option>
                <option value="Hukum">Komisi I : Hukum</option>
                <option value="Kominfo">Komisi II : Komunikasi dan Informasi</option>
              </select>
            </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label">Pilihan Departemen 2</label>
              <select class="form-control" name="dept2" required>
                <option value="" disabled selected hidden>Pilih departemen</option>
                <option value="Advokesma">Advokasi dan Kesejahteraan Mahasiswa</option>
                <option value="Sosma">Sosial Mahasiswa</option>
                <option value="PSDM">Pengembangan Sumber Daya Manusia</option>
                <option value="Medkominfo">Multimedia, Komunikasi dan Informasi</option>
                <option value="P2S">Penelitian dan Pengembangan Studi</option>
                <option value="KWU">Kewirausahaan</option>
              </select>
            </div>

            <div class="form-group col-md-6 col-sm-12"></div>


            <h4 class="mt-3 col-12">Pilihan Jadwal Screening</h4>
            <div class="col-12">
              <p style="color: red;"><?= $this->session->flashdata('messageschedule');?></p>
            </div>
            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label">Pilihan Jadwal Screening 1</label>
              <select class="form-control" name="tanggal1" required>
                <option value="" disabled selected hidden>Tanggal</option>
                <?php
                for ($row = 0; $row < sizeof($hari[0]); $row++) :
                  for ($col = 0; $col < sizeof($hari[1]); $col++) :
                    if ($hari[$row][$col] == "") continue;
                    else if ($col == 0) { ?>
                      <option value="<?= $hari[$row][0]; ?>"><?= $hari[$row][0]; ?></option>
                    <?php } else { ?>
                      <option value="<?= $hari[$row][$col]; ?>"><?= $hari[$row][$col]; ?></option>
                <?php }
                  endfor;
                endfor;
                ?>
                <!-- <option value="Sabtu">Sabtu, 2 Oktober 2021</option>
                <option value="Minggu">Minggu, 3 Oktober 2021</option>
                <option value="Senin">Senin, 4 Oktober 2021</option>
                <option value="Selasa">Selasa, 5 Oktober 2021</option>
                <option value="Rabu">Rabu, 6 Oktober 2021</option>
                <option value="Kamis">Kamis, 7 Oktober 2021</option> -->
              </select>
              <!-- <select class="form-control mt-3" name="waktu1" required>
                <option value="" disabled selected hidden>Jam</option>
                <option value="11:00 - 11:45">11:00 - 11:45 WIB (Khusus hari Minggu)</option>
                <option value="13:00 - 13:45">13:00 - 13:45 WIB</option>
                <option value="13:45 - 14:30">13:45 - 14:30 WIB</option>
                <option value="14:30 - 15:15">14:30 - 15:15 WIB</option>
                <option value="16:00 - 16:45">16:00 - 16:45 WIB</option>
                <option value="16:45 - 17:30">16:45 - 17:30 WIB</option>
                <option value="19:00 - 19:45">19:00 - 19:45 WIB</option>
                <option value="19:45 - 20:30">19:45 - 20:30 WIB</option>
                <option value="20:30 - 21:15">20:30 - 21:15 WIB</option>
              </select> -->
            </div>

            <div class="form-group col-md-6 col-sm-12">
              <label class="control-label">Pilihan Jadwal Screening 2</label>
              <select class="form-control" name="tanggal2" required>
                <option value="" disabled selected hidden>Tanggal</option>
                <?php
                for ($row = 0; $row < sizeof($hari[0]); $row++) :
                  for ($col = 0; $col < sizeof($hari[1]); $col++) :
                    if ($hari[$row][$col] == "") continue;
                    else if ($col == 0) { ?>
                      <option value="<?= $hari[$row][0]; ?>"><?= $hari[$row][0]; ?></option>
                    <?php } else { ?>
                      <option value="<?= $hari[$row][$col]; ?>"><?= $hari[$row][$col]; ?></option>
                <?php }
                  endfor;
                endfor;
                ?>
                <!-- <option value="Sabtu">Sabtu, 2 Oktober 2021</option>
                <option value="Minggu">Minggu, 3 Oktober 2021</option>
                <option value="Senin">Senin, 4 Oktober 2021</option>
                <option value="Selasa">Selasa, 5 Oktober 2021</option>
                <option value="Rabu">Rabu, 6 Oktober 2021</option>
                <option value="Kamis">Kamis, 7 Oktober 2021</option> -->
              </select>
              <!-- <select class="form-control mt-3" name="waktu2" required>
                <option value="" disabled selected hidden>Jam</option>
                <option value="11:00 - 11:45">11:00 - 11:45 WIB</option>
                <option value="13:00 - 13:45">13:00 - 13:45 WIB</option>
                <option value="13:45 - 14:30">13:45 - 14:30 WIB</option>
                <option value="14:30 - 15:15">14:30 - 15:15 WIB</option>
                <option value="16:00 - 16:45">16:00 - 16:45 WIB</option>
                <option value="16:45 - 17:30">16:45 - 17:30 WIB</option>
                <option value="19:00 - 19:45">19:00 - 19:45 WIB</option>
                <option value="19:45 - 20:30">19:45 - 20:30 WIB</option>
                <option value="20:30 - 21:15">20:30 - 21:15 WIB</option>
              </select> -->
            </div>

            <div class="form-group col-md-12 col-sm-12 mt-3">
              <label class="control-label">Link Post Twibbon</label>
              <input type="text" class="form-control" name="twibbon" value="<?= set_value('twibbon'); ?>" required>
              <span style="color:grey;">Pastikan akun instagram tidak diprivate</span>
            </div>

            <div class="form-group col-md-12 col-sm-12 mt-3">
              <label class="control-label">Link pengumpulan berkas pendaftaran (Link Google Drive)</label><br>
              <input type="text" class="form-control mt-2" name="link" value="<?= set_value('link'); ?>" required placeholder="link drive">
              <span style="color:grey;">Pastikan permission link sudah di set menjadi <span style="color:red;">"anyone with this link"</span></span>
            </div>
            <div class="form-group ml-4 mt-3 pb-5">
              <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
              <label class="form-check-label" for="exampleCheck1">Saya sudah yakin bahwa data yang saya isi sudah benar dan sesuai dengan ketentuan yang berlaku.</label>
            </div>
            <div class="text-right pt-2 mt-5">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>


</body>

</html>