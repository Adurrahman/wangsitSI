<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="<?= base_url('assets/eventPsdm/style.css'); ?>">
  <link rel="icon" type="image/png" href="#" style="width: 512px; height: 512px;">
  <link rel="icon" href="<?php echo base_url() ?>assets/starship/logo.png" />
  <title>Status</title>
</head>

<body>
  <img id="aa" src="<?= base_url('assets/starship/bg.png'); ?>" alt="">
  <div class="container">
    <div class="kata mb-5">
      <h1 style="color:#FED136;"><b><?= $this->session->flashdata('message'); ?></b></h1>
      <!--<h4 style="color:white;">Tunggu pengumuman selanjutnya yaa.</h4>-->
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

</body>

<script type="text/javascript">
  setTimeout(
    function() {
      window.location = "https://wangsit.kbmsi.or.id/events"
    },
    5000); // waktu tunggu atau delay
</script>

</html>