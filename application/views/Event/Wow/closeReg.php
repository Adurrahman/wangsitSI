<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <!-- 
        		Meta tag name viewport untuk memerintahkan browser agar layout dapat menyesuaikan dibanyak perangkat secara bersamaan 
        	-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>WOW SI 2022</title>

    <!-- Link ke file Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- CSS Custom -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/eventPsdm/formulirWOW2022.css'); ?>">

    <link rel="stylesheet" type="text/css" href="	https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <link href="http://fonts.cdnfonts.com/css/games" rel="stylesheet">
    <link href="http://fonts.cdnfonts.com/css/segoe-pro" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">

</head>

<body>
    <div id="bg" style="height:100vh">
        <div class="container form-wow text-center shadow boxPosi position-absolute top-50 start-50 translate-middle">
            <h5>Mohon Maaf pendaftaran sebagai Calon Staff WOW SI 2022 telah ditutup</h5>
        </div>
    </div>
</body>

<!-- Balik ke dashboard -->
<script type="text/javascript">
    setTimeout(
        function() {
            window.location = "https://wangsit.kbmsi.or.id/events"
        },
        5000); // waktu tunggu atau delay
</script>

</html>