<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">

  <!-- 
		Meta tag name viewport untuk memerintahkan browser agar layout dapat menyesuaikan dibanyak perangkat secara bersamaan 
	-->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Link ke file Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <!-- CSS Custom -->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/eventPsdm/formulirWOW2022.css'); ?>">

  <link rel="stylesheet" type="text/css" href="	https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <link href="http://fonts.cdnfonts.com/css/games" rel="stylesheet">
  <link href="http://fonts.cdnfonts.com/css/segoe-pro" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">

  <style>
    #select-options-6c5045c3-0b71-94e9-889d-302c9a81dee6,
    .select-dropdown,
    .dropdown-trigger,
    .caret {
      display: none !important;
    }

    label {
      background-color: transparent !important;
      color: #0C5774;
    }
    
    .btn{
        background-color: #0C5774;
    }
  </style>
</head>

<body>
  <div id="bg">
    <!-- Judul or Header -->
    <div class="heading col-md-12 text-center">
      <h3>Selamat Datang di</h3>
      <h1>OPEN RECRUITMENT WOW SI 2022</h2>
    </div>
    <!-- End of Judul -->

    <!-- Form -->
    <div class="container form-wow m-auto">
      <form action="/event/submitwow" method="POST">
        <div class="row">
          <div class="col-md-12">
            <h4 class="kartu-2">Guidebook & Form Komitmen</h4>
            <button class="btn mb-3"style="margin-left: 20px; color: white;"><a href="https://bit.ly/KetentuanOPRECWOWSI2022" target="_blank" style="color: white;"> Klik Di Sini </a></button>
          </div>

          <div class="col-md-12">
            <h4 class="kartu-2">Form Pengisian</h4>
          </div>
          <div class="col-md-6 mb-3">
            <label class="form-label">Nama Lengkap<span class="red-star">*</span></label>
            <input type="text" name="nama_lengkap" class="form-control" value="<?= $user->NAMA;  ?>" disabled name="nama">
          </div>

          <div class="col-md-6 mb-3">
            <label class="form-label">NIM<span class="red-star">*</span></label>
            <input type="text" name="nim" class="form-control" value="<?= $user->NIM;  ?>" disabled name="nim">
          </div>

          <div class="col-md-6 mb-3">
            <label class="form-label">Nomor Telepon<span class="red-star">*</span></label>
            <input type="text" class="form-control" required name="noPhone">
          </div>

          <div class="col-md-6 mb-3">
            <label class="form-label">ID Line<span class="red-star">*</span></label>
            <input type="text" class="form-control" required name="idLine">
          </div>

          <div class="col-md-12 mb-3">
            <label class="form-label">Tempat, Tanggal Lahir<span class="red-star">*</span></label>
            <input type="text" name="ttl" class="form-control" required name="ttl">
          </div>


          <div class="col-md-6 mb-3">
            <label class="form-label">Kelebihan Diri<span class="red-star">*</span></label>
            <textarea class="form-control" rows="3" name="kelebihan"></textarea>
          </div>


          <div class="col-md-6 mb-3">
            <label class="form-label">Kekurangan Diri<span class="red-star">*</span></label>
            <textarea class="form-control" rows="3" name="kekurangan"></textarea>
          </div>

          <div class="col-md-12 mb-5">
            <label class="form-label">Motivasi dan Tujuan Mendaftar Kepanitian WOW SI 2022<span class="red-star">*</span></label>
            <textarea class="form-control" rows="4" name="motivasi"></textarea>
          </div>

          <div class="col-md-12">
            <h4 class="kartu-2">Pilihan Divisi</h4>
          </div>

          <p style="color: red;"><?= $this->session->flashdata('messagedept'); ?></p>
          <!-- Pilihan Divisi -->
          <div class="col-md-6 mb-3">
            <label class="form-label">Pilihan Divisi 1 (Wajib)<span class="red-star">*</span></label>
            <select class="form-select" name="dept1">
              <option value="Acara">Acara</option>
              <option value="Pendamping">Pendamping</option>
              <option value="Humas">Humas</option>
              <option value="DDM">DDM</option>
              <option value="Kemankes">Kemankes</option>
              <option value="Komdanus">Komdanus</option>
              <option value="PJL">PJL</option>
              <option value="Perkab">Perlengkapan</option>
            </select>
          </div>

          <div class="col-md-6 mb-3">
            <label class="form-label">Pilihan Divisi 2 (Opsional)</label>
            <select class="form-select" name="dept2">
              <option value="Acara">Acara</option>
              <option value="Pendamping">Pendamping</option>
              <option value="Humas">Humas</option>
              <option value="DDM">DDM</option>
              <option value="Kemankes">Kemankes</option>
              <option value="Komdanus">Komdanus</option>
              <option value="PJL">PJL</option>
              <option value="Perkab">Perlengkapan</option>
            </select>
          </div>

          <div class="col-md-6 mb-3">
            <textarea class="form-control" rows="5" placeholder="Alasan memilih divisi pertama" name="alasan1"></textarea>
          </div>


          <div class="col-md-6 mb-3">
            <textarea class="form-control" rows="5" placeholder="Alasan memilih divisi kedua" name="alasan2"></textarea>
          </div>
          <!-- End of Pilihan Divisi -->

          <div class="col-md-12">
            <h4 class="kartu-2">Pilihan Jadwal Screening</h4>
          </div>

          <p style="color: red;"><?= $this->session->flashdata('messageschedule'); ?></p>
          <!-- Pilihan Screening -->
          <div class="col-md-6 mb-3">
            <label class="form-label">Pilihan Jadwal Screening 1</label>
            <select class="form-select mb-2" name="jadwal1">
              <option value="Kamis, 12 Mei 2022 Jam 19.00 - 19.40 WIB">Kamis, 12 Mei 2022 Jam 19.00 - 19.40 WIB</option>
              <option value="Kamis, 12 Mei 2022 Jam 19.45 - 20.25 WIB">Kamis, 12 Mei 2022 Jam 19.45 - 20.25 WIB</option>
              <option value="Kamis, 12 Mei 2022 Jam 20.30 - 21.10 WIB">Kamis, 12 Mei 2022 Jam 20.30 - 21.10 WIB</option>

              <option value="Jumat, 13 Mei 2022 Jam 19.00 - 19.40 WIB">Jumat, 13 Mei 2022 Jam 19.00 - 19.40 WIB</option>
              <option value="Jumat, 13 Mei 2022 Jam 19.45 - 20.25 WIB">Jumat, 13 Mei 2022 Jam 19.45 - 20.25 WIB</option>
              <option value="Jumat, 13 Mei 2022 Jam 20.30 - 21.10 WIB">Jumat, 13 Mei 2022 Jam 20.30 - 21.10 WIB</option>

              <option value="Sabtu, 14 Mei 2022 : 09.00 - 09.40 WIB">Sabtu, 14 Mei 2022 : 09.00 - 09.40 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 09.45 - 10.25 WIB">Sabtu, 14 Mei 2022 : 09.45 - 10.25 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 10.30 - 11.10 WIB">Sabtu, 14 Mei 2022 : 10.30 - 11.10 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 11.15 - 11.55 WIB">Sabtu, 14 Mei 2022 : 11.15 - 11.55 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 13.00 - 13.40 WIB">Sabtu, 14 Mei 2022 : 13.00 - 13.40 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 13.45 - 14.25 WIB">Sabtu, 14 Mei 2022 : 13.45 - 14.25 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 14.30 - 15.10 WIB">Sabtu, 14 Mei 2022 : 14.30 - 15.10 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 15.15 - 15.55 WIB">Sabtu, 14 Mei 2022 : 15.15 - 15.55 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 16.00 - 16.40 WIB">Sabtu, 14 Mei 2022 : 16.00 - 16.40 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 19.00 - 19.40 WIB">Sabtu, 14 Mei 2022 : 19.00 - 19.40 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 19.45 - 20.25 WIB">Sabtu, 14 Mei 2022 : 19.45 - 20.25 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 20.30 - 21.10 WIB">Sabtu, 14 Mei 2022 : 20.30 - 21.10 WIB</option>

              <option value="Minggu, 15 Mei 2022 : 09.00 - 09.40 WIB">Minggu, 15 Mei 2022 : 09.00 - 09.40 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 09.45 - 10.25 WIB">Minggu, 15 Mei 2022 : 09.45 - 10.25 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 10.30 - 11.10 WIB">Minggu, 15 Mei 2022 : 10.30 - 11.10 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 11.15 - 11.55 WIB">Minggu, 15 Mei 2022 : 11.15 - 11.55 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 13.00 - 13.40 WIB">Minggu, 15 Mei 2022 : 13.00 - 13.40 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 13.45 - 14.25 WIB">Minggu, 15 Mei 2022 : 13.45 - 14.25 WIB</option>

              <option value="Minggu, 15 Mei 2022 : 14.30 - 15.10 WIB">Minggu, 15 Mei 2022 : 14.30 - 15.10 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 15.15 - 15.55 WIB">Minggu, 15 Mei 2022 : 15.15 - 15.55 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 16.00 - 16.40 WIB">Minggu, 15 Mei 2022 : 16.00 - 16.40 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 19.00 - 19.40 WIB">Minggu, 15 Mei 2022 : 19.00 - 19.40 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 19.45 - 20.25 WIB">Minggu, 15 Mei 2022 : 19.45 - 20.25 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 20.30 - 21.10 WIB">Minggu, 15 Mei 2022 : 20.30 - 21.10 WIB</option>
            </select>
            <!-- <select class="form-select mb-2" name="jam1">
              <option value="19.00 - 19.40 WIB">19.00 - 19.40 WIB</option>
              <option value="19.45 - 20.25 WIB">19.45 - 20.25 WIB</option>
              <option value="20.30 - 21.10 WIB">20.30 - 21.10 WIB</option>
            </select> -->
            <p class="fw-bold">(option example : Senin 9 Mei 2022 19.00 - 19.40)</p>
          </div>

          <div class="col-md-6 mb-3">
            <label class="form-label">Pilihan Jadwal Screening 2</label>
            <select class="form-select mb-2" name="jadwal2">
              <option value="Kamis, 12 Mei 2022 Jam 19.00 - 19.40 WIB">Kamis, 12 Mei 2022 Jam 19.00 - 19.40 WIB</option>
              <option value="Kamis, 12 Mei 2022 Jam 19.45 - 20.25 WIB">Kamis, 12 Mei 2022 Jam 19.45 - 20.25 WIB</option>
              <option value="Kamis, 12 Mei 2022 Jam 20.30 - 21.10 WIB">Kamis, 12 Mei 2022 Jam 20.30 - 21.10 WIB</option>

              <option value="Jumat, 13 Mei 2022 Jam 19.00 - 19.40 WIB">Jumat, 13 Mei 2022 Jam 19.00 - 19.40 WIB</option>
              <option value="Jumat, 13 Mei 2022 Jam 19.45 - 20.25 WIB">Jumat, 13 Mei 2022 Jam 19.45 - 20.25 WIB</option>
              <option value="Jumat, 13 Mei 2022 Jam 20.30 - 21.10 WIB">Jumat, 13 Mei 2022 Jam 20.30 - 21.10 WIB</option>

              <option value="Sabtu, 14 Mei 2022 : 09.00 - 09.40 WIB">Sabtu, 14 Mei 2022 : 09.00 - 09.40 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 09.45 - 10.25 WIB">Sabtu, 14 Mei 2022 : 09.45 - 10.25 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 10.30 - 11.10 WIB">Sabtu, 14 Mei 2022 : 10.30 - 11.10 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 11.15 - 11.55 WIB">Sabtu, 14 Mei 2022 : 11.15 - 11.55 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 13.00 - 13.40 WIB">Sabtu, 14 Mei 2022 : 13.00 - 13.40 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 13.45 - 14.25 WIB">Sabtu, 14 Mei 2022 : 13.45 - 14.25 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 14.30 - 15.10 WIB">Sabtu, 14 Mei 2022 : 14.30 - 15.10 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 15.15 - 15.55 WIB">Sabtu, 14 Mei 2022 : 15.15 - 15.55 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 16.00 - 16.40 WIB">Sabtu, 14 Mei 2022 : 16.00 - 16.40 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 19.00 - 19.40 WIB">Sabtu, 14 Mei 2022 : 19.00 - 19.40 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 19.45 - 20.25 WIB">Sabtu, 14 Mei 2022 : 19.45 - 20.25 WIB</option>
              <option value="Sabtu, 14 Mei 2022 : 20.30 - 21.10 WIB">Sabtu, 14 Mei 2022 : 20.30 - 21.10 WIB</option>

              <option value="Minggu, 15 Mei 2022 : 09.00 - 09.40 WIB">Minggu, 15 Mei 2022 : 09.00 - 09.40 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 09.45 - 10.25 WIB">Minggu, 15 Mei 2022 : 09.45 - 10.25 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 10.30 - 11.10 WIB">Minggu, 15 Mei 2022 : 10.30 - 11.10 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 11.15 - 11.55 WIB">Minggu, 15 Mei 2022 : 11.15 - 11.55 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 13.00 - 13.40 WIB">Minggu, 15 Mei 2022 : 13.00 - 13.40 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 13.45 - 14.25 WIB">Minggu, 15 Mei 2022 : 13.45 - 14.25 WIB</option>

              <option value="Minggu, 15 Mei 2022 : 14.30 - 15.10 WIB">Minggu, 15 Mei 2022 : 14.30 - 15.10 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 15.15 - 15.55 WIB">Minggu, 15 Mei 2022 : 15.15 - 15.55 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 16.00 - 16.40 WIB">Minggu, 15 Mei 2022 : 16.00 - 16.40 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 19.00 - 19.40 WIB">Minggu, 15 Mei 2022 : 19.00 - 19.40 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 19.45 - 20.25 WIB">Minggu, 15 Mei 2022 : 19.45 - 20.25 WIB</option>
              <option value="Minggu, 15 Mei 2022 : 20.30 - 21.10 WIB">Minggu, 15 Mei 2022 : 20.30 - 21.10 WIB</option>
            </select>
            <!-- <select class="form-select mb-2" name="jam2">
              <option value="19.00 - 19.40 WIB">19.00 - 19.40 WIB</option>
              <option value="19.45 - 20.25 WIB">19.45 - 20.25 WIB</option>
              <option value="20.30 - 21.10 WIB">20.30 - 21.10 WIB</option>
            </select> -->
            <p class="fw-bold">(option example : Senin 9 Mei 2022 19.00 - 19.40)</p>
          </div>

        </div>

        <!-- End of Pilihan Screening -->

        <div class="bottom-content">
          <div class="kartu mb-3">


            <p>Submit link folder google drive yang berisi:</p>
            <ul>
              <li>Scan surat komitmen</li>
              <li>Scan KTM/KTMS</li>
              <li>Scan sertifikat kepanitian/organisasi yang dimiliki (bila ada)</li>
              <li>Portfolio design (bila ada, Khusus DDM)</li>
            </ul>
            <p class="bold-af">*Catatan</p>
            <ol type="1">
              <li>Unggah berkas yang telah diisi beserta softfile foto 4x6 dan portofolio (jika ada, untuk pilihan divisi DDM) pada folder Google Drive pribadi
              </li>
              <li>Akses link Google Drive diubah menjadi public
              </li>
              <li>Format penamaan folder Google Drive = “Divisi1_Divisi2_Nama Lengkap”</li>
              <li>Informasi terkait jadwal screening akan disampaikan oleh narahubung WOW SI 2022 melalui Line/WhatsApp</li>
            </ol>
          </div>
        </div>

        <div class="col-md-12 mb-1">
          <label class="form-label">Link GDrive<span class="red-star">*</span></label>
          <input type="text" name="linkDrive" class="form-control" required>
        </div>


        <div class="col-md-12 mb-3 mt-3" >
          <button type="submit" name="save" class="btn" style="color: white;"> Submit</button>
        </div>

    </div>
    </form>
  </div>
  <!-- End of Form -->
  </div>

  <!-- JavaScript Bootstrap JS dan Popper (bundle)-->
  <script type="text/javascript" src="bootstrap5/js/bootstrap.bundle.js"></script>

</body>

</html>