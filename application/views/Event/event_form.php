<div class="event__bg">
  <!-- <img src="<?= base_url('asset/img/left_ornament.svg') ?>" alt=""> -->
</div>
<div class="bege">
  <img src="<?= base_url('asset/img/cry2.png') ?>" alt=""> 
</div>
<!--<div class="container" style="margin-top: 2.5rem; display: flex; justify-content: center; ">-->
    <div class="container" style="padding-bottom:10px; padding-top:10px; display: flex; justify-content: center; ">
  <div class="row">
    <div class="col l12 m12 s12 ">
      <h4 class="event-title" style="text-transform: uppercase; color: #55254e; font-weight:bolder"> Form Pendaftaran <?= $event->name ?> </h4>
      <form action="<?= base_url("event/registerValidation") ?>" method="post">
        <input type="hidden" name="id_event" value="<?= $event->id_event ?>">
        <input type="hidden" name="id_user" value="<?= $user->ID ?>">
        
        <div class="input-field">
          <input id="NAMA" name = "NAMA" type="text" class="validate event__form" value="<?= $user->NAMA ?>" required pattern=".*\S+.*" title="Jangan diisi spasi aja ya!" pattern=".*\S+.*" title="Jangan diisi spasi aja ya!">
          <label for="NAMA"  class="event__label" style="color:#000000 !important; background-color:#ffffff !important;" > Nama Lengkap </label> 
        </div>
        <div class="input-field">
          <input id="NIM" name="NIM" type="text" class="validate event__form" value="<?= $user->NIM ?>" required pattern=".*\S+.*" title="Jangan diisi spasi aja ya!">
          <label for="NIM"  class="event__label" style="color:#000000 !important; background-color:#ffffff !important;"> NIM </label>
        </div>
        <div class="input-field">
          <input id="angkatan" type="text" disabled class="validate event__form" value="<?= $user->angkatan ?>">
          <label for="angkatan" class="event__label"style="color:#000000 !important; background-color:#ffffff !important;"> Angkatan </label>
        </div>
        <div class="input-field">
          <input id="HP" name = "HP" type="text" class="validate event__form" value="<?= $user->HP ?>" required pattern=".*\S+.*" title="Jangan diisi spasi aja ya!">
          <label for="HP" class="event__label" style="color:#000000 !important; background-color:#ffffff !important;"> No Handphone </label>
        </div>
        <div class="input-field">
          <input name="id_line" id="idLine" type="text" class="validate event__form" value="<?= $user->id_line != null ? $user->id_line : ''  ?>" required pattern=".*\S+.*" title="Jangan diisi spasi aja ya!">
          <label for="idLine" class="event__label" style="color:#000000 !important; background-color:#ffffff !important;"> ID Line </label>
        </div>
        <button class="btn wangsit-color" style="width: 100%; font-weight: 600;"> Submit </button>
      </form>
    </div>
  </div>
</div>