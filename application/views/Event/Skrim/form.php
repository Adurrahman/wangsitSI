<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
  <link rel="stylesheet" href="<?= base_url('assets/eventPsdm/styleformulir.css'); ?>">
  <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
  <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>

  <title>SKRIM Berprestasi Webinar</title>
  <style>
    .kata {
      font-family: 'Poppins', sans-serif !important;
      color: black;
      margin: auto;
      margin-top: 50px;
      box-shadow: 2px 0px 10px rgb(135, 135, 135);
    }

    .kata h6 {
      font-weight: bold;
    }

    .kata img {
      width: 200px;
      margin: 10px;
    }

    .kata .date {
      margin: auto;
    }

    .isi-form {
      font-family: 'Poppins', sans-serif !important;
      font-size: 14px !important;
    }

    button {
      font-family: 'Poppins', sans-serif !important;
      width: 150px;
    }
  </style>
</head>

<body>
  <img id="aa" src="<?= base_url('assets/eventP2S/bg.png'); ?>">

  <div class="container-fluid">
    <div class="row">
      <div class="kata mb-5">
        <h6 class="mt-4 text-center" style="font-size: 25px;">Form Pendaftaran SKRIM : Berprestasi</h6>
        <h6 class="text-center" style="font-size: 18px;">“BE YOUR OWN CHAMPION”</h6>
        <img src="<?= base_url('assets/eventP2S/doxa.png'); ?>">
        <img src="<?= base_url('assets/eventP2S/safira.png'); ?>">
        <img src="<?= base_url('assets/eventP2S/alip.png'); ?>">
        <div class="d-flex justify-content-center">
          <p class="text-left mt-3">
            📆 : Minggu, 29 Agustus 2021 <br>
            ⌚️ : 09.30 WIB - Selesai <br>
            📌 : Zoom Meeting</p>
        </div>

        <div class="isi-form my-3 text-left">
          <form action=" <?= base_url('event/submitSkrim'); ?> " method="post">
            <div class="row">
              <div class="form-group col-md-6 col-sm-12">
                <label class="control-label"><b>Nama Lengkap</b></label>
                <input type="text" class="form-control" value="<?= $user->NAMA;  ?>" disabled>
              </div>
              <div class="form-group col-md-6 col-sm-12">
                <label class="control-label"><b>NIM</b></label>
                <input type="text" class="form-control" value="<?= $user->NIM;  ?>" disabled>
              </div>
              <div class="form-group col-md-6 col-sm-12">
                <label class="control-label"><b>Angkatan</b></label>
                <input type="text" class="form-control" name="akt" required>
              </div>
              <div class="form-group col-md-6 col-sm-12">
                <label class="control-label"><b>ID LINE</b></label>
                <input type="text" class="form-control" name="idLine" required>
              </div>
              <div class="form-group col-md-12 col-sm-12">
                <label class="form-label"><b>Pertanyaan untuk pemateri (opsional)</b></label>
                <div class="form-floating">
                  <textarea class="form-control" style="height: 80px" name="tanya"></textarea>
                </div>
              </div>
            </div>
            <div class="text-center mt-3">
              <button type="submit" class="btn btn-primary">Daftar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>


</body>

</html>