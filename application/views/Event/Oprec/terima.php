<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="<?= base_url('assets/css/styleoprec.css'); ?>"> 
  <link rel="icon" type="image/png" href="#" style="width: 512px; height: 512px;">
  <link rel="icon" href="<?php echo base_url() ?>assets/opreckbmsi/logokbmsi.png" />
  <title>Status</title>
</head>

<body>
  <img id="aa" src="<?= base_url('assets/opreckbmsi/asetbg/bg1.png'); ?>" alt="" style="height: 100vh !important;">
  <img class="or1" src="<?= base_url('assets/opreckbmsi/asetbg/asset1.png'); ?>" alt="">
  <!--<img class="or2" src="<?= base_url('assets/opreckbmsi/asetbg/asset2.png'); ?>" alt="">-->
  <!--<img class="or3" src="<?= base_url('assets/opreckbmsi/asetbg/asset3.png'); ?>" alt="">-->
  <!--<img class="or4" src="<?= base_url('assets/opreckbmsi/asetbg/asset4.png'); ?>" alt="">-->
  <!-- <div class="container"> -->
  <!-- <div class="kata mb-5"> -->
  <!-- <h1 style="color:#FED136;"><b><?= $this->session->flashdata('message'); ?></b></h1> -->
  <!--<h4 style="color:white;">Tunggu pengumuman selanjutnya yaa.</h4>-->
  <!-- <h1 style="color:#FED136;"><b><?= $this->session->flashdata('message'); ?></b></h1> -->
  <!-- </div> -->
  <!-- </div> -->
  <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script> -->

  <div class="text-center pb-5 px-sm-5">
    <div class="kata pt-5">
      <div class="text-left p-5" style="background-color:white; border-radius:20px; width:70%; margin:auto; margin-top:50px;">
        <h1 class="btn-company mb-3" style="font-weight:bold;"><?= $this->session->flashdata('message') . ", " . $this->session->flashdata('nama') . "!👋"; ?></h1>
        <!-- <h1 style="font-size:25px; margin-bottom:20px;"><?= $this->session->flashdata('nama'); ?></h1> -->
        <p style="color:black; font-size:20px; font-weight:normal;">Selamat, kamu telah lolos seleksi Staff Lembaga KBMSI Periode 9🤩</p>
        <p style="color:black; font-size:20px; font-weight:normal;">Untuk informasi selanjutnya akan diinfokan melalui personal chat. Dipantau terus ya Line nya...</a></p>
        <p style="color:black; font-size:20px; font-weight:normal;">Semangat, Do Your Best!☺️</p>
      </div>
    </div>
  </div>
</body>

<!-- <script type="text/javascript">
    setTimeout(
      function(){
        window.location = "https://wangsit.kbmsi.or.id/events"
      },
      5000); // waktu tunggu atau delay
    </script> -->

</html>