<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" href="<?= base_url('assets/css/styleoprec.css'); ?>"> 
  <link rel="icon" type="image/png" href="#" style="width: 512px; height: 512px;">
  <link rel="icon" href="<?php echo base_url() ?>assets/opreckbmsi/logokbmsi.png" />
  <title>Dashboard Oprec Staff Lembaga KBMSI Periode 9</title>
</head>

<body>
<img id="aa" src="<?= base_url('assets/opreckbmsi/asetbg/bg1.png'); ?>" alt="" style="height: 100vh !important;">

  <nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <a class="navbar-brand" href="#" style="color:#ffc107;"><b>Oprec Staff Lembaga KBMSI Periode 9</b></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav ml-auto">
        <a class="nav-item nav-link btn btn-warning" href="<?= base_url('event/logoutOprec'); ?>"><b style="color: white;">Logout</b></a>
      </div>
    </div>
  </nav>

  <div class="container-fluid">
    <center>
      <div class="kotak my-5" style="width:95%; height:90%; background-color: white; overflow:auto; ">
        <table class="table table-striped">
          <thead class="thead-light">
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Nim</th>
              <th>ID_Line</th>
              <th>Email</th>
              <th>Pilihan_Dept_1</th>
              <th>Pilihan_Dept_2</th>
              <th>Pilihan_Komisi</th>
              <th>Jadwal_1</th>
              <th>Jadwal_2</th>
              <th>Link_Berkas</th>
              <th>Status_Diterima</th>
              <th>Ubah_Status</th>
            </tr>
          </thead>
          <tbody>
            <?php $NO = 1;
            foreach ($pendaftar as $data) { ?>
              <tr>
                <td><?= $NO; ?></td>
                <td><?= $data->nama; ?></a></td>
                <td><?= $data->nim; ?></td>
                <td><?= $data->id_line; ?></td>
                <td><?= $data->email; ?></td>
                <td><?= $data->pildept1; ?></td>
                <td><?= $data->pildept2; ?></td>
                <td><?= $data->pilkomisi; ?></td>
                <td><?= $data->jadwal1; ?></td>
                <td><?= $data->jadwal2; ?></td>
                <td><a target="_blank" href="<?= $data->link_berkas; ?>"><?= $data->link_berkas; ?></a></td>
                <td align='center'>
                  <?php if ($data->status == 1) { ?>
                    <i class="fa fa-check fa-2x" aria-hidden="true" style="color:green;"></i>
                  <?php } else { ?>
                    <i class="fa fa-times fa-2x" aria-hidden="true" style="color:red;"></i>
                  <?php } ?>
                </td>
                <td align='center'>
                  <?php if ($data->status == 1) { ?>
                    <a href="<?= base_url() ?>AdminEventOprec/ubah/<?= $data->id_regist; ?>/0" class="btn btn-danger">TOLAK</a>
                  <?php } else { ?>
                    <a href="<?= base_url() ?>AdminEventOprec/ubah/<?= $data->id_regist; ?>/1" class="btn tn btn-warning text-light">TERIMA</a>
                  <?php } ?>
                </td>
              </tr>
            <?php $NO++;
            }; ?>
          </tbody>
        </table>

      </div>
    </center>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>