<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url('assets/eventPsdm/style.css'); ?>">
    <title>Login</title>
  </head>
  <body>
    <img id="aa" src="<?= base_url('assets/eventSosma/webbg-Bimsak.png'); ?>" alt="">
    <div class="container">
    <div class="kata">
    <div class="text-center">
    <h3 class="mb-4 align-center">Open Recruitment Staff WOW SI</h3>
    <center>
     <div class="login text-left">
      <?= $this->session->flashdata('messageStatus'); ?>
      <form action=" <?= base_url('event/cek'); ?> " method="post">
        <div class="form-group">
          <label for="exampleInputEmail1">NIM</label>
          <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nim" required>
          </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" class="form-control" id="exampleInputPassword1" name="password" required>
        </div>

        <span style="color: red; font-weight:400;"> <?= $this->session->flashdata('message'); ?> </span>
        <center>
        <button type="submit" class="btn"><b>Login</b></button>
      </center>
      </form>
      </div>
   </center>
</div>
</div>
</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

  </body>
</html>
