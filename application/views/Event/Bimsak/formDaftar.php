<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    

    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/eventSosma/formulirBimsak2022.css'); ?>">
    <!--  Fonts Poppins -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap" rel="stylesheet">

    <title>BIMASAKTI 7.0</title>
    <style>
        #select-options-6c5045c3-0b71-94e9-889d-302c9a81dee6,
        .select-dropdown,
        .dropdown-trigger,
        .caret {
            display: none !important;
        }

        input[type=text] {
        
        color: white !important;
        }

        input[type=number] {
        
        color: white !important;
        }

        input[type=email] {
        
        color: white !important;
        }

        label {
          background-color: transparent !important;
          color: #fff;
        }
        
        a{
          text-decoration: none;
        }
        
        body .container {
          background-color: #1a1a40;
          border-radius: 10px;
          border: 0px !important;
        }
      
    </style>
  </head>
  <body>
    <!-- <div id="bg">
      <img src="asset/bgBimaSakti7.0.png" alt="" />
    </div> -->
    <div class="judul text-center m-5 text-light " style="border:none ;">
      <p class="display-6 " style="font-family: 'Poppins';">Halo, Selamat Datang di</p>
      <p class="display-3 "style="font-family: 'Poppins';">Open Recruitment Staff BIMA SAKTI 7.0</p>
    </div>

    <div class="container top-border w-75 p-5 mb-5">
      <form action="<?= base_url('/event/submitBimsak'); ?>" method="POST">
        <div class="row">
          <div class="col-md-12">
            <div class="text-start">
              <h4 class="subtitle-1">Guidebook dan Form Komitmen</h4>
              <p>File Guidebook dan Form Pernyataan dapat diunduh di bawah ini</p>
            </div>
            <a href="https://bit.ly/Guidebook-FormPernyataan" target="_blank" class=""> <button type="button" class="btn btn-primary mb-3">KLIK DISINI</button></a>
          </div>
          <!-- IDENTITAS -->
          <div class="col-md-12 mt-3">
            <h4 class="subtitle-1">Identitas</h4>
          </div>

          <div class="col-md-6 mb-1">
            <label class="form-label">Nama lengkap <span class="text-danger">*</span></label>
            <input type="text" class="form-control" value="<?= $user->NAMA;  ?>" name="nama" disabled/>
          </div>

          <div class="col-md-6 mb-1">
            <label class="form-label">NIM <span class="text-danger">*</span></label>
            <input type="number" class="form-control" value="<?= $user->NIM;  ?>" name="nim" disabled />
          </div>

          <div class="col-md-6 mb-1">
            <label class="form-label">Jenis Kelamin<span class="text-danger">*</span></label>

            <select class="form-select" aria-label="Default select example" name="jenis_kelamin" required>
              <option value="Perempuan">Perempuan</option>
              <option value="Laki-laki">Laki-laki</option>
            </select>
          </div>

          <div class="col-md-6 mb-1">
            <label class="form-label">Tempat, Tanggal Lahir<span class="text-danger">*</span></label>
            <input type="text" class="form-control" required name="ttl" placeholder="tempat, tanggal-bulan-tahun" />
          </div>

          <div class="col-md-6 mb-1">
            <label class="form-label">Email <span class="text-danger">*</span></label>
            <input type="email" class="form-control" required name="email" />
          </div>

          <div class="col-md-6 mb-1">
            <label class="form-label">Alamat di Malang / Alamat asal<span class="note"> (bila belum di malang)</span> <span class="text-danger">*</span></label>
            <input type="text" class="form-control" required name="alamat" />
          </div>

          <div class="col-md-6 mb-1">
            <label class="form-label">No Handphone <span class="text-danger">*</span></label>
            <input type="number" class="form-control" required name="noPhone" />
          </div>

          <div class="col-md-6 mb-1">
            <label class="form-label">ID LINE <span class="text-danger">*</span></label>
            <input type="text" class="form-control" required name="idLine" />
          </div>
          <!-- AKHIR IDENTITAS -->

          <!-- PILIHAN DIVISI -->
          <div class="col-md-12 mt-3">
            <h4 class="subtitle-1">Pilihan Divisi</h4>
          </div>
          <p style="color: red;"><?= $this->session->flashdata('messagedept'); ?></p>
          <div class="col-md-6 mb-3">
            <label class="form-label">Pilihan Divisi 1 <span class="note"> Wajib</span><span class="text-danger">*</span></label>

            <select class="form-select" name="dept1" required>
              <option value="Acara">Acara</option>
              <option value="Pengajar">Pengajar</option>
              <option value="Humas">Humas</option>
              <option value="DDM">DDM</option>
              <option value="Perkap">Perkap</option>
              <option value="Komdanus">Komdanus</option>
              <option value="Creative">Creative</option>
            </select>
          </div>

          <div class="col-md-6 mb-3">
            <label class="form-label">Pilihan Divisi 2 <span class="note">Wajib</span><span class="text-danger">*</span></label>
            <select class="form-select" name="dept2" required>
              <option value="Acara">Acara</option>
              <option value="Pengajar">Pengajar</option>
              <option value="Humas">Humas</option>
              <option value="DDM">DDM</option>
              <option value="Perkap">Perkap</option>
              <option value="Komdanus">Komdanus</option>
              <option value="Creative">Creative</option>
            </select>
          </div>

          <div class="col-md-6 mb-3">
            <textarea class="form-control" rows="5" placeholder="Alasan memilih divisi pertama" name="alasan1"></textarea>
          </div>

          <div class="col-md-6 mb-3">
            <textarea class="form-control" rows="5" placeholder="Alasan memilih divisi kedua" name="alasan2"></textarea>
          </div>

          <!-- AKHIR PEMILIHAN DIVISI -->

          <!-- PEMILIHAN SCREENING -->

          <div class="col-md-12 mt-3">
            <h4 class="subtitle-1">Pilihan Jadwal Screening</h4>
          </div>
          
          <p style="color: red;"><?= $this->session->flashdata('messageschedule'); ?></p>
          
          <div class="col-md-6 mb-3">
            <label class="form-label">Pilihan Jadwal Screening 1</label>
            <select class="form-select mb-2" name="jadwal1">
              <option value="Jumat, 13 Mei 2022 Jam 13.00 - 14.00 WIB">Jumat, 13 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Jumat, 13 Mei 2022 Jam 16.00 - 17.00 WIB">Jumat, 13 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Jumat, 13 Mei 2022 Jam 19.30 - 20.30 WIB">Jumat, 13 Mei 2022 Jam 19.30 - 20.30 WIB</option>

              <option value="Sabtu, 14 Mei 2022 Jam 13.00 - 14.00 WIB">Sabtu, 14 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Sabtu, 14 Mei 2022 Jam 16.00 - 17.00 WIB">Sabtu, 14 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Sabtu, 14 Mei 2022 Jam 19.30 - 20.30 WIB">Sabtu, 14 Mei 2022 Jam 19.30 - 20.30 WIB</option>

              <option value="Minggu, 15 Mei 2022 Jam 13.00 - 14.00 WIB">Minggu, 15 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Minggu, 15 Mei 2022 Jam 16.00 - 17.00 WIB">Minggu, 15 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Minggu, 15 Mei 2022 Jam 19.30 - 20.30 WIB">Minggu, 15 Mei 2022 Jam 19.30 - 20.30 WIB</option>
              

              <option value="Senin, 16 Mei 2022 Jam 13.00 - 14.00 WIB">Senin, 16 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Senin, 16 Mei 2022 Jam 16.00 - 17.00 WIB">Senin, 16 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Senin, 16 Mei 2022 Jam 19.30 - 20.30 WIB">Senin, 16 Mei 2022 Jam 19.30 - 20.30 WIB</option>
              
              <option value="Selasa, 17 Mei 2022 Jam 13.00 - 14.00 WIB">Selasa, 17 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Selasa, 17 Mei 2022 Jam 16.00 - 17.00 WIB">Selasa, 17 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Selasa, 17 Mei 2022 Jam 19.30 - 20.30 WIB">Selasa, 17 Mei 2022 Jam 19.30 - 20.30 WIB</option>

              <option value="Rabu, 18 Mei 2022 Jam 13.00 - 14.00 WIB">Rabu, 18 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Rabu, 18 Mei 2022 Jam 16.00 - 17.00 WIB">Rabu, 18 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Rabu, 18 Mei 2022 Jam 19.30 - 20.30 WIB">Rabu, 18 Mei 2022 Jam 19.30 - 20.30 WIB</option>

              <option value="Kamis, 19 Mei 2022 Jam 13.00 - 14.00 WIB">Kamis, 19 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Kamis, 19 Mei 2022 Jam 16.00 - 17.00 WIB">Kamis, 19 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Kamis, 19 Mei 2022 Jam 19.30 - 20.30 WIB">Kamis, 19 Mei 2022 Jam 19.30 - 20.30 WIB</option>

            </select>
        
            <p class="fw-bold">(option example : Senin 9 Mei 2022 19.00 - 19.40)</p>
          </div>
          <div class="col-md-6 mb-3">
            <label class="form-label">Pilihan Jadwal Screening 2</label>
            <select class="form-select mb-2" name="jadwal2">
              <option value="Jumat, 13 Mei 2022 Jam 13.00 - 14.00 WIB">Jumat, 13 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Jumat, 13 Mei 2022 Jam 16.00 - 17.00 WIB">Jumat, 13 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Jumat, 13 Mei 2022 Jam 19.30 - 20.30 WIB">Jumat, 13 Mei 2022 Jam 19.30 - 20.30 WIB</option>

              <option value="Sabtu, 14 Mei 2022 Jam 13.00 - 14.00 WIB">Sabtu, 14 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Sabtu, 14 Mei 2022 Jam 16.00 - 17.00 WIB">Sabtu, 14 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Sabtu, 14 Mei 2022 Jam 19.30 - 20.30 WIB">Sabtu, 14 Mei 2022 Jam 19.30 - 20.30 WIB</option>

              <option value="Minggu, 15 Mei 2022 Jam 13.00 - 14.00 WIB">Minggu, 15 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Minggu, 15 Mei 2022 Jam 16.00 - 17.00 WIB">Minggu, 15 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Minggu, 15 Mei 2022 Jam 19.30 - 20.30 WIB">Minggu, 15 Mei 2022 Jam 19.30 - 20.30 WIB</option>
              

              <option value="Senin, 16 Mei 2022 Jam 13.00 - 14.00 WIB">Senin, 16 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Senin, 16 Mei 2022 Jam 16.00 - 17.00 WIB">Senin, 16 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Senin, 16 Mei 2022 Jam 19.30 - 20.30 WIB">Senin, 16 Mei 2022 Jam 19.30 - 20.30 WIB</option>
              
              <option value="Selasa, 17 Mei 2022 Jam 13.00 - 14.00 WIB">Selasa, 17 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Selasa, 17 Mei 2022 Jam 16.00 - 17.00 WIB">Selasa, 17 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Selasa, 17 Mei 2022 Jam 19.30 - 20.30 WIB">Selasa, 17 Mei 2022 Jam 19.30 - 20.30 WIB</option>

              <option value="Rabu, 18 Mei 2022 Jam 13.00 - 14.00 WIB">Rabu, 18 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Rabu, 18 Mei 2022 Jam 16.00 - 17.00 WIB">Rabu, 18 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Rabu, 18 Mei 2022 Jam 19.30 - 20.30 WIB">Rabu, 18 Mei 2022 Jam 19.30 - 20.30 WIB</option>

              <option value="Kamis, 19 Mei 2022 Jam 13.00 - 14.00 WIB">Kamis, 19 Mei 2022 Jam 13.00 - 14.00 WIB</option>
              <option value="Kamis, 19 Mei 2022 Jam 16.00 - 17.00 WIB">Kamis, 19 Mei 2022 Jam 16.00 - 17.00 WIB</option>
              <option value="Kamis, 19 Mei 2022 Jam 19.30 - 20.30 WIB">Kamis, 19 Mei 2022 Jam 19.30 - 20.30 WIB</option>

            </select>
            
          </div>


          <!-- AKHIR DARI PEMILIHAN SCREENING -->
          <!-- PENGALAMAN -->
          <div class="col-md-12 mt-3">
            <h4 class="subtitle-1">Pengalaman</h4>
          </div>
          <!-- Pengalaman 1 -->
          <div class="col-md-6">
            <p class="text-center">Pengalaman 1</p>
            <div class="mb-2">
              <input type="text" class="form-control" name="jabatan1" placeholder="Jabatan" aria-label="jabatan1" />
            </div>
            <div class="mb-2">
              <input type="text" class="form-control" name="kegiatan1" placeholder="Kegiatan atau Organisasi" aria-label="kegiatan1" />
            </div>
            <div class="mb-3">
              <input type="number" name="tahun1" class="form-control" placeholder="Tahun Organisasi" aria-label="tahun1" min="2000" max="2022" />
            </div>
          </div>
          <!-- Pengalaman 2 -->
          <div class="col-md-6 mb-3">
            <p class="text-center">Pengalaman 2</p>
            <div class="mb-2">
              <input type="text" class="form-control" name="jabatan2" placeholder="Jabatan" aria-label="jabatan2" />
            </div>
            <div class="mb-2">
              <input type="text" class="form-control" name="kegiatan1" placeholder="Kegiatan atau Organisasi" aria-label="kegiatan2" />
            </div>
            <div class="mb-2">
              <input type="number" name="tahun2" class="form-control" placeholder="Tahun Organisasi" aria-label="tahun2" min="2000" max="2022" />
            </div>
          </div>
          <!-- Pengalaman 3 -->
          <div class="col-md-6">
            <p class="text-center">Pengalaman 3</p>
            <div class="mb-2">
              <input type="text" class="form-control" name="jabatan3" placeholder="Jabatan" aria-label="jabatan3" />
            </div>
            <div class="mb-2">
              <input type="text" class="form-control" name="kegiatan3" placeholder="Kegiatan atau Organisasi" aria-label="kegiatan3" />
            </div>
            <div class="mb-2">
              <input type="number" name="tahun3" class="form-control" placeholder="Tahun Organisasi" aria-label="tahun3" min="2000" max="2022" />
            </div>
          </div>
          <!-- Pengalaman 4 -->
          <div class="col-md-6 mb-3">
            <p class="text-center">Pengalaman 4</p>
            <div class="mb-2">
              <input type="text" class="form-control" name="jabatan4" placeholder="Jabatan" aria-label="jabatan4" />
            </div>
            <div class="mb-2">
              <input type="text" class="form-control" name="kegiatan4" placeholder="Kegiatan atau Organisasi" aria-label="kegiatan4" />
            </div>
            <div class="mb-2">
              <input type="number" name="tahun4" class="form-control" placeholder="Tahun Organisasi" aria-label="tahun4" min="2000" max="2022" />
            </div>
          </div>
          <!-- Pengalaman 5 -->
          <div class="col-md-6 mb-3">
            <p class="text-center">Pengalaman 5</p>
            <div class="mb-2">
              <input type="text" class="form-control" name="jabatan5" placeholder="Jabatan" aria-label="jabatan5" />
            </div>
            <div class="mb-2">
              <input type="text" class="form-control" name="kegiatan5" placeholder="Kegiatan atau Organisasi" aria-label="kegiatan5" />
            </div>
            <div class="mb-2">
              <input type="number" name="tahun5" class="form-control" placeholder="Tahun Organisasi" aria-label="tahun5" min="2000" max="2022" />
            </div>
          </div>
          <!-- Pengalaman 6 -->
          <div class="col-md-6">
            <p class="text-center">Pengalaman 6</p>
            <div class="mb-2">
              <input type="text" class="form-control" name="jabatan6" placeholder="Jabatan" aria-label="jabatan6" />
            </div>
            <div class="mb-2">
              <input type="text" class="form-control" name="kegiatan6" placeholder="Kegiatan atau Organisasi" aria-label="kegiata6" />
            </div>
            <div class="mb-2">
              <input type="number" name="tahun6" class="form-control" placeholder="Tahun Organisasi" aria-label="tahun6" min="2000" max="2022" />
            </div>
          </div>
          <!-- AKHIR DARI PENGALAMAN -->

          <!-- KELEBIHAN DAN KEKURANGAN -->
          <div class="col-md-12 mt-3">
            <h4 class="subtitle-1">Kelebihan & Kekurangan</h4>
          </div>
          <!-- Kelebihan 1 -->
          <div class="kelebihan col-md-6">
            <p class="text-start mb-0">Kelebihan<span class="text-danger">*</span></p>
            <div class="note mb-2">
              Format penulisan <strong>KELEBIHAN, PENJELASAN</strong> contoh : <br />
              - Baik dalam multitasking, hal tersebut menjadi kelebihan saya karena saya termasuk orang yang mudah fokus. <br />
              - Kelebihan B <br />
              - Kelebihan C <br />
            </div>
            <div class="col-md-12 mb-3">
              <textarea class="form-control" rows="5" placeholder="ketikkan kelebihan disini" name="kelebihan" required></textarea>
            </div>
          </div>
          <div class="kekurangan col-md-6">
            <p class="text-start mb-0">Kekurangan<span class="text-danger">*</span></p>
            <div class="note mb-2">
              Format penulisan <strong>KEKURANGAN, PENJELASAN</strong> contoh : <br />
              - Time management buruk, hal tersebut termasuk ke dalam kekurangan saya karena saya sulit untuk menentukan prioritas <br />
              - Kekurangan B <br />
              - Kekurangan C <br />
            </div>
            <div class="col-md-12 mb-3">
              <textarea class="form-control" rows="5" placeholder="ketikkan kekurangan disini" name="kekurangan" required></textarea>
            </div>
          </div>
          <!-- AKHIR DARI KELEBIHAN DAN KEKURANGAN -->
          <!-- ALASAN MEMILIH -->
          <div class="col-md-12 mb-3">
            <label class="form-label mt-3">Mengapa anda cocok diterima di Bima Sakti 7.0?<span class="text-danger">*</span></label>
            <textarea class="form-control" rows="5" placeholder="ketikkan disini" name="alasanDiterima" required></textarea>
          </div>
          <!-- AKHIR ALASAN MEMILIH -->

          <!-- LINK PENGUMPULAN -->
          <div class="col-md-12 mt-3">
            <h4 class="subtitle-1">Link Pengumpulan</h4>
          </div>

          <div class="col-md-12">
            <label class="form-label">Link Gdrive <span class="text-danger">*</span></label>
            <div class="note">
              <p class="mb-0">Link Google Drive dengan akses public berisi:</p>
              <ul>
                <li>File scan surat komitmen yang telah diisi, format PDF.</li>
                <li>File scan KTM/KTMS, format PDF.</li>
                <li>Softfile foto diri formal 4x6, format JPG/PNG.</li>
                <li>Folder portofolio design (khusus pendaftar divisi DDM)</li>
              </ul>
              <p class="mb-0">Format penamaan folder Google Drive:</p>
              <ul>
                <li>PilihanDivisi1_PilihanDivisi2_NamaLengkap</li>
              </ul>
            </div>
            <input type="link" class="form-control" required name="linkDrive" placeholder="Masukkan link gdrive" />
          </div>
          <div class="text-right mt-4">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
    </div>

    <div class="text-center mb-5">
      <p>Made with <b>❤</b> by P2S EMSI 2022</p>
    </div>
    <script script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
