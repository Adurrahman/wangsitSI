<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url('assets/eventPsdm/styleformulir.css'); ?>">
    <!-- <link href="https://fonts.googleapis.com/css2?family=Candal&display=swap" rel="stylesheet"> -->

    <title>BIMASAKTI 6.0</title>
</head>
  <body>

    <img id="aa" src="<?= base_url('assets/eventSosma/webbg-Bimsak.png'); ?>" alt="">


<div class="container">
  <div class="kata mb-5">
    <h6 class="mt-4">Halo, Selamat Datang</h6>
    <h4 style="font-weight:bold;"><?= $user->NAMA;  ?></h4>
    <div class="text-left download mt-4 mx-5">
      <span>Download formulir surat pernyataan </span> <a href="<?= base_url('assets/eventSosma/Form_Pernyataan_BIMASAKTI.docx'); ?>">disini</a>
    </div>


    <div class="isi-form my-3 text-left">
      <form action=" <?= base_url('event/submitBimsak'); ?> " method="post">
      <div class="row">
        <div class="form-group col-md-6 col-sm-12">
          <label class="control-label">Nama Lengkap</label>
          <input type="text" class="form-control" value="<?= $user->NAMA;  ?>" disabled>
        </div>
        <div class="form-group col-md-6 col-sm-12">
          <label class="control-label">NIM</label>
          <input type="text" class="form-control" value="<?= $user->NIM;  ?>" disabled>
        </div>
        <div class="form-group col-md-6 col-sm-12">
          <label class="control-label">No Handphone</label>
          <input type="text" class="form-control" name="noHp" value="<?= set_value('noHp'); ?>" required>
        </div>
        <div class="form-group col-md-6 col-sm-12">
          <label class="control-label">ID LINE</label>
          <input type="text" class="form-control" name="idLine" value="<?= set_value('idLine'); ?>" required>
        </div>
      </div>

      <h4 class="mt-3">Pilihan Divisi</h4>
      <div class="row">
      <div class="form-group col-md-6 col-sm-12">
      <label class="control-label">Pilihan Divisi 1 (wajib)</label>
      <select class="form-control" name="divisi1" required>
        <option value="" disabled selected hidden>Pilih divisi</option>
        <option value="Pengajar">Pengajar</option>
        <option value="Acara">Acara</option>
        <option value="DDM">DDM</option>
        <option value="Komdanus">Komdanus</option>
        <option value="Humas">Humas</option>
        <option value="Perkap">Perkap</option>
      </select>
        <div class="form-floating mt-3">
            <textarea class="form-control" style="height: 100px" name="alasan1" placeholder="alasan memilih" required></textarea>

        </div>
      </div>

      <div class="form-group col-md-6 col-sm-12">
      <label class="">Pilihan Divisi 2 (opsional)</label>
      <select class="form-control" name="divisi2">
        <option value="" disabled selected hidden>Pilih divisi</option>
        <option value="Pengajar">Pengajar</option>
        <option value="Acara">Acara</option>
        <option value="DDM">DDM</option>
        <option value="Komdanus">Komdanus</option>
        <option value="Humas">Humas</option>
        <option value="Perkap">Perkap</option>
      </select>
        <div class="form-floating mt-3">
            <textarea class="form-control" style="height: 100px" name="alasan2" placeholder="alasan memilih"></textarea>
        </div>
      </div>
      </div>


      <!-- <h4 class="mt-3">Pilihan jadwal screening</h4>
      < ?= $this->session->flashdata('messagescreening'); ?>
      <div class="row">
      <div class="form-group col-md-6 col-sm-12">
      <label class="control-label">Pilihan Jadwal Screening 1</label>
      <select class="form-control" name="tanggal1" required>
        <option value="" disabled selected hidden>tanggal</option>
        <option value="4 mei 2021">4 Mei 2021</option>
        <option value="5 mei 2021">5 Mei 2021</option>
        <option value="6 mei 2021">6 Mei 2021</option>
        <option value="7 mei 2021">7 Mei 2021</option>
      </select>
      <select class="form-control mt-3" name="waktu1" required>
        <option value="" disabled selected hidden>jam</option>
        <option value="16.00 - 16.30">16.00 - 16.30 WIB</option>
        <option value="20.00 - 20.30">20.00 - 20.30 WIB</option>
        <option value="20.35 - 21.05">20.35 - 21.05 WIB</option>
        <option value="21.10 - 21.40">21.10 - 21.40 WIB</option>
        <option value="21.45 - 22.15">21.45 - 22.15 WIB</option>
      </select>
      </div> -->

      <!-- <div class="form-group col-md-6 col-sm-12">
      <label class="control-label">Pilihan Jadwal Screening 2</label>
      <select class="form-control" name="tanggal2" required>
        <option value="" disabled selected hidden>tanggal</option>
        <option value="4 mei 2021">4 Mei 2021</option>
        <option value="5 mei 2021">5 Mei 2021</option>
        <option value="6 mei 2021">6 Mei 2021</option>
        <option value="7 mei 2021">7 Mei 2021</option>
      </select>
      <select class="form-control mt-3" name="waktu2" required>
        <option value="" disabled selected hidden>jam</option>
        <option value="16.00 - 16.30">16.00 - 16.30 WIB</option>
        <option value="20.00 - 20.30">20.00 - 20.30 WIB</option>
        <option value="20.35 - 21.05">20.35 - 21.05 WIB</option>
        <option value="21.10 - 21.40">21.10 - 21.40 WIB</option>
        <option value="21.45 - 22.15">21.45 - 22.15 WIB</option>
      </select>
      </div>
      </div> -->

      <div class="form-group mt-3">
        <label class="control-label">"Link pengumpulan berkas pendaftaran (Link Google Drive)</label><br>
        <span  class="mt-2">Drive berisi:</span>
        <ul style="color:grey; padding-left:30px;">
            <li> Pas Foto (Wajib)</li>
            <li> Surat Pernyataan (wajib)</li>
            <li> Screenshot profil SIAM atau scan KTM/KTMS (wajib)</li>
            <li> Scan sertifikat organisasi/dan prestasi yang dimiliki (bila ada)</li>
            <li> Portfolio design untuk pendaftar divisi DDM (bila ada)</li>
        </ul>
        <input type="text" class="form-control mt-2" name="link" value="<?= set_value('link'); ?>" required placeholder="link drive">
        <span style="color:grey;">Pastikan permission link sudah di set menjadi <span style="color:red;">"anyone with this link"</span></span>
      </div>
      <div class="form-check">
       <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
       <label class="form-check-label" for="exampleCheck1">Saya sudah yakin bahwa data yang saya isi sudah benar dan sesuai dengan ketentuan yang berlaku.</label>
      </div>
      <div class="text-right mt-4">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
   </div>
      </div>
    </div>


    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>


  </body>
</html>
