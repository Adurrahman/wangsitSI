<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url('assets/eventPsdm/style.css'); ?>">
    <title>Status</title>
  </head>
  <body>
    <img id="aa" src="<?= base_url('assets/eventSosma/webbg-Bimsak.png'); ?>" alt="">
    <div class="text-center pb-5 px-sm-5">
        <div class="kata pt-5">
          <div class="text-left p-5" style="background-color:white; border-radius:20px; width:70%; margin:auto; margin-top:50px;">
            <h1 class="btn-company mb-3"style="font-size:30px; font-weight:bold;"><?= $this->session->flashdata('message').", ".$this->session->flashdata('nama'); ?></h1>
            <p style="color:black; font-size:20px; font-weight:normal;">Kami belum bisa menerima kamu untuk menjadi bagian dari kepanitiaan BIMA SAKTI 6.0</p>
            <p style="color:black; font-size:20px; font-weight:normal;">Terimakasih atas perjuangannya, Sampai jumpa di kesempatan selanjutnya.</p>
          </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

  </body>

    <!-- <script type="text/javascript">
    setTimeout(
      function(){
        window.location = "https://wangsit.kbmsi.or.id/events"
      },
      5000); // waktu tunggu atau delay
    </script> -->
</html>
