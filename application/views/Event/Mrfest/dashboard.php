<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <title>Dashboard oprec</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <div class="container">
      <a class="navbar-brand" href="#" style="color:#ffc107;"><b>MRFEST 7.0</b></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse d-flex flex-row-reverse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-item nav-link btn btn-warning" href="<?= base_url('admin/logoutMRFEST'); ?>"><b style="color: white;">Logout</b></a>
        </div>
      </div>
    </div>
  </nav>

  <div class="container my-5">
    <div class="accordion" id="accordionExample">
      <?php $NO = 1;
      foreach ($pendaftar as $data) { ?>

        <div class="accordion-item">
          <h2 class="accordion-header" id="headingOne<?= $data->idPeserta; ?>">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne<?= $data->idPeserta; ?>" aria-expanded="false" aria-controls="collapseOne<?= $data->idPeserta; ?>">
              <div class="d-flex justify-content-between w-100 px-4">
                <p class="m-0"><?= $data->nama; ?></p>
                <?php if ($data->diTerima == 1) { ?>
                  <p class="m-0 text-success fw-bold">diterima</p>
                <?php } else { ?>
                  <p class="m-0 text-danger fw-bold">ditolak</p>
                <?php } ?>

              </div>
            </button>
          </h2>
          <div id="collapseOne<?= $data->idPeserta; ?>" class="accordion-collapse collapse" aria-labelledby="headingOne<?= $data->idPeserta; ?>" data-bs-parent="#accordionExample">
            <div class="accordion-body">
              <div class="row">
                <p class="col-4 fw-bold">Nim</p>
                <p class="col"><?= $data->nim; ?></p>
              </div>

              <div class="row">
                <p class="col-4 fw-bold">No HP</p>
                <p class="col"><?= $data->noPhone; ?></p>
              </div>

              <div class="row">
                <p class="col-4 fw-bold">Id Line</p>
                <p class="col"><?= $data->idLine; ?></p>
              </div>

              <div class="row">
                <p class="col-4 fw-bold">motivasi mendaftar</p>
                <p class="col"><?= $data->motivasi; ?></p>
              </div>

              <div class="row">
                <p class="col-4 fw-bold">Divisi 1</p>
                <p class="col"><?= $data->dept1; ?></p>
              </div>

              <div class="row">
                <p class="col-4 fw-bold">Alasan 1</p>
                <p class="col"><?= $data->alasan1; ?></p>
              </div>

              <div class="row">
                <p class="col-4 fw-bold">Divisi 2</p>
                <p class="col"><?= $data->dept2; ?></p>
              </div>

              <div class="row">
                <p class="col-4 fw-bold">Alasan 2</p>
                <p class="col"><?= $data->alasan2; ?></p>
              </div>

              <div class="row">
                <p class="col-4 fw-bold">Link Drive</p>
                <a target="_blank" class="col" href="<?= $data->linkDrive; ?>"><?= $data->linkDrive; ?></a>
              </div>

              <a href="/tesaja">
                <?php if ($data->diTerima == 1) { ?>
                  <a href="<?= base_url() ?>AdminEventPsdm2/ubah/<?= $data->idPeserta; ?>/0">
                    <button class="mt-5 btn btn-success w-100">Ubah status menjadi <span class="fw-bold">ditolak</span></button>
                  </a>
                <?php } else { ?>
                  <a href="<?= base_url() ?>AdminEventPsdm2/ubah/<?= $data->idPeserta; ?>/1">
                    <button class="mt-5 btn btn-danger w-100">Ubah status menjadi <span class="fw-bold">diterima</span></button>
                  </a>
                <?php } ?>
              </a>

            </div>
          </div>
        </div>
      <?php $NO++;
      }; ?>
    </div>
  </div>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>