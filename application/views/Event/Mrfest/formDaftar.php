<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>WANGSIT | STAFF MRFEST 7.0</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link href="http://fonts.cdnfonts.com/css/alice-in-wonderland" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/eventPsdm/formulirMRFest2022.css'); ?>">

    <style>
        body {
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            background-attachment: fixed;
            font-family: "Times New Roman", Times, serif;
        }

        .judul {
            font-family: "Alice in Wonderland", serif;
            border: none;
        }

        input {
            background-color: white !important;
            border-radius: 6px !important;
            box-sizing: border-box !important;
            padding: 0px 10px !important;
        }

        #select-options-6c5045c3-0b71-94e9-889d-302c9a81dee6,
        .select-dropdown,
        .dropdown-trigger,
        .caret {
            display: none !important;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input[type=number] {
            -moz-appearance: textfield;
        }

        label {
            background-color: transparent !important;
        }
    </style>
</head>

<body>
    <br /><br /><br />
    <!-- JUDUL -->
    <div class="judul text-light text-center">
        <h2 class="gg display-4">Halo, selamat datang di</h2>
        <h1 class="mb-5 fw-bold display-3">OPEN RECRUITMENT STAFF MRFEST 7.0</h1>
    </div>

    <div class="container bg-dark text-white rounded-3 p-5 w-75 bg-opacity-75 text-opacity-100 fs-4 mb-5">
        <div class="text-start">
            <p class="fs-5"> Silahkan Unduh File Guidebook </p>
            <button class="btn btn-link mb-3"><a href="http://bit.ly/GuideBookOPRECMRFEST" target="_blank"> Klik Di Sini </a></button>
        </div>
        <!-- identitas diri -->
        <!-- form -->
        <form action="/event/submitmrfest" method="POST">
            <div class="dvs fs-1 text-center">Identitas</div>
            <!-- left form -->
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label class="form-label">Nama Lengkap <span class="asterik text-danger">*</span></label>
                    <input type="text" value="<?= $user->NAMA ?>" name="nama" disabled class="form-control" />
                </div>
                <div class="col-md-6 mb-3">
                    <label class="form-label">NIM <span class="asterik text-danger">*</span></label>
                    <input type="number" name="nim" value="<?= $user->NIM ?>" class="form-control" disabled />
                </div>
                <div class="col-md-6 mb-3">
                    <label class="form-label">Id Line <span class="asterik text-danger">*</span></label>
                    <input type="text" name="idLine" class="form-control" required />
                </div>
                <div class="col-md-6 mb-3">
                    <label class="form-label">No Hanphone <span class="asterik text-danger">*</span> </label>
                    <input type="text" name="noPhone" class="form-control" required />
                </div>
                <div class="col-md-3 mb-3">
                    <label class="form-label">Tempat Lahir <span class="asterik text-danger">*</span></label>
                    <input type="text" name="tempatLahir" class="form-control" required />
                </div>
                <div class="col-md-3 mb-3">
                    <label class="form-label">Tanggal Lahir <span class="asterik text-danger">*</span></label>
                    <input type="date" name="tanggalLahir" class="form-control" required />
                </div>
                <div class="col-md-6 mb-3">
                    <label class="form-label">Alamat Malang <span class="asterik text-danger">*</span></label>
                    <input type="text" name="alamatMalang" class="form-control" required />
                </div>
                <div class="mb-3">
                    <label class="form-label">Motivasi Mengikuti MR FEST 7.0 <span class="asterik text-danger">*</span></label>
                    <textarea class="form-control" rows="5" name="motivasi"></textarea>
                </div>
                <div class="divisi fs-1 text-center">Pemilihan Divisi</div>
                <p style="color: red;"><?= $this->session->flashdata('messagedept'); ?></p>
                <div class="col-md-6 mb-3">
                    <label class="form-label">Pilihan divisi 1 <span class="asterik text-danger">*</span> </label>
                    <select class="form-select" name="dept1">
                        <option value="Acara">Acara</option>
                        <option value="Humas">Humas</option>
                        <option value="Sponsor">Sponsor</option>
                        <option value="Transkoper">Transkoper</option>
                        <option value="Kreatif">Kreatif</option>
                        <option value="DDM">DDM</option>
                        <option value="Korlap">Korlap</option>
                        <option value="kodanus">Kodanus</option>
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <label class="form-label">Pilihan divisi 2 <span class="asterik text-danger">*</span> </label>
                    <select class="form-select" name="dept2">
                        <option value="Acara">Acara</option>
                        <option value="Humas">Humas</option>
                        <option value="Sponsor">Sponsor</option>
                        <option value="Transkoper">Transkoper</option>
                        <option value="Kreatif">Kreatif</option>
                        <option value="DDM">DDM</option>
                        <option value="Korlap">Korlap</option>
                        <option value="kodanus">Kodanus</option>
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <label class="form-label">Alasan memilih divisi 1 <span class="asterik text-danger">*</span></label>
                    <textarea class="form-control" rows="5" name="alasan1"></textarea>
                </div>
                <div class="col-md-6 mb-3">
                    <label class="form-label">Alasan memilih divisi 2 <span class="asterik text-danger">*</span></label>
                    <textarea class="form-control" rows="5" name="alasan2"></textarea>
                </div>

                <!-- tabel pengalaman -->
                <div class="divisi fs-1 text-center mb-3">Pengalaman</div>
                <div class="row">
                    <div class="col-6">
                        <!-- pengalaman 1 -->
                        <p class="text-center">Pengalaman 1</p>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="jabatan1" placeholder="Jabatan" aria-label="Jabatan1" />
                        </div>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="kegiatan1" placeholder="Kegiatan atau Organisasi" aria-label="kegiatan1" />
                        </div>
                        <div class="mb-2">
                            <input type="number" name="tahun1" class="form-control" placeholder="Tahun Organisasi" aria-label="tahun1" />
                        </div>

                        <!-- Pengalaman 2 -->
                        <p class="text-center">Pengalaman 2</p>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="jabatan2" placeholder="Jabatan" aria-label="jabatan2" />
                        </div>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="kegiatan2" placeholder="Kegiatan atau Organisasi" aria-label="kegiatan2" />
                        </div>
                        <div class="mb-2">
                            <input type="number" name="tahun2" class="form-control" placeholder="Tahun Organisasi" aria-label="tahun2" />
                        </div>

                        <!-- Pengalaman 3 -->
                        <p class="text-center">Pengalaman 3</p>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="jabatan3" placeholder="Jabatan" aria-label="jabatan3" />
                        </div>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="kegiatan3" placeholder="Kegiatan atau Organisasi" aria-label="kegiatn3" />
                        </div>
                        <div class="mb-2">
                            <input type="number" name="tahun3" class="form-control" placeholder="Tahun Organisasi" aria-label="tahun3" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- pengalaman 1 -->
                        <p class="text-center">Pengalaman 4</p>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="jabatan4" placeholder="Jabatan" aria-label="jabatan4" />
                        </div>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="kegiatan4" placeholder="Kegiatan atau Organisasi" aria-label="kegiatan4" />
                        </div>
                        <div class="mb-2">
                            <input type="number" name="tahun4" class="form-control" placeholder="Tahun Organisasi" aria-label="tahun4" />
                        </div>

                        <!-- Pengalaman 2 -->
                        <p class="text-center">Pengalaman 5</p>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="jabatan5" placeholder="Jabatan" aria-label="jabatan5" />
                        </div>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="kegiatan5" placeholder="Kegiatan atau Organisasi" aria-label="kegiatan5" />
                        </div>
                        <div class="mb-2">
                            <input type="number" name="tahun5" class="form-control" placeholder="Tahun Organisasi" aria-label="tahun5" />
                        </div>

                        <!-- Pengalaman 3 -->
                        <p class="text-center">Pengalaman 6</p>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="jabatan6" placeholder="Jabatan" aria-label="jabatan6" />
                        </div>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="kegiatan6" placeholder="Kegiatan atau Organisasi" aria-label="kegiatan6" />
                        </div>
                        <div class="mb-2">
                            <input type="number" name="tahun6" class="form-control" placeholder="Tahun Organisasi" aria-label="tahun6" />
                        </div>
                    </div>
                </div>

                <div class="fs-5">
                    Link pengumpulan berkas pendaftaran(Link Google Drive)<br />drive berisi : <br />
                    <ol>
                        <li>Sertakan pass foto berukuran 4x6 dalam bentuk JPG.</li>
                        <li>Sertakan 1 softfile KTM/KTMS dalam bentuk PDF.</li>
                        <li>Form Pernyataan yang sudah ditandatangani dan bermaterai wajib di scan dan dikumpulkan dalam bentuk PDF</li>
                        <li>Sertakan scan sertifikat organisasi/kepanitiaan yang dimiliki dalam bentuk PDF</li>
                        <li>Bagi calon staff Divisi DDM dengan kemampuan desain poster, video maupun fotografi di wajibkan untuk melampirkan portofolio dalam bentuk softfile</li>
                        <li>Semua berkas di-upload pada satu folder google drive dan diberikan akses “anyone can view this link” dengan nama folder yaitu MRFEST7.0 - NAMA LENGKAP.</li>
                        <li>Upload link google drive yang telah dibuat pada website pendaftaran yang telah disediakan di atas.</li>
                    </ol>
                    <p>
                        Link download form pernyataan dapat diunduh
                        <a href="https://bit.ly/FormPernyataanMRFEST7" target="_blank">disini</a>
                    </p>
                    <div class="mb-3">
                        <label for="linkgdrive" class="form-label">link</label>
                        <input type="url" class="form-control" name="linkDrive" placeholder="Masukkan link google drive" required />
                    </div>
                </div>
            </div>


            <div class="w-100 m-auto mt-5" style="height: 60px">
                <button type="button " class="btn btn-success w-100 h-100 fs-3">SUBMIT</button>
            </div>
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>

</html>