<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url('assets/eventPsdm/styleformulir.css'); ?>">
    <!-- <link href="https://fonts.googleapis.com/css2?family=Candal&display=swap" rel="stylesheet"> -->

    <title>formulir MRFEST 6.0</title>
</head>
  <body>

    <img id="aa" src="<?= base_url('assets/eventPsdm/BGMrfest.png'); ?>" alt="">


<div class="container">
  <div class="kata mb-5">
    <h6 class="mt-4">Halo, Selamat Datang</h6>
    <h4 style="font-weight:bold;"><?= $nama;  ?></h4>
    <div class="text-left download mt-4 mx-5">
      <span>Download formulir surat pernyataan </span> <a href="<?= base_url('assets/eventPsdm/Form_Pernyataan_MRFEST.docx'); ?>">disini</a>
    </div>


    <div class="isi-form my-3 text-left">
      <form action=" <?= base_url('event/submitMRFEST'); ?> " method="post">
      <div class="row">
        <div class="form-group col-md-6 col-sm-12">
          <label class="control-label">Nama Lengkap</label>
          <input type="text" class="form-control" value="<?= $nama;  ?>" disabled>
        </div>
        <div class="form-group col-md-6 col-sm-12">
          <label class="control-label">NIM</label>
          <input type="text" class="form-control" value="<?= $nim;  ?>" disabled>
        </div>
        <div class="form-group col-md-6 col-sm-12">
          <label class="control-label">Tempat Lahir</label>
          <input type="text" class="form-control" name="tempatL" value="<?= set_value('tempatL'); ?>" required>
        </div>
        <div class="form-group col-md-6 col-sm-12">
          <label class="control-label">Tanggal Lahir</label>
          <input type="date" class="form-control" name="ttl" value="<?= set_value('ttl'); ?>" required>
        </div>
        <div class="form-group col-md-12 col-sm-12">
          <label class="control-label">Alamat</label>
          <input type="text" class="form-control" name="alamat" value="<?= set_value('alamat'); ?>" required>
        </div>
        <div class="form-group col-md-6 col-sm-12">
          <label class="control-label">Jenis Kelamin</label><br>
          <div class="ml-3" style="color:black;">
            <input type="radio" name="gender" value="L" checked="checked">&nbsp; Laki-Laki
            <input class="mt-2 ml-4" type="radio" name="gender" value="P">&nbsp; Perempuan
          </div>
          </div>
        <div class="form-group col-md-6 col-sm-12">
          <label class="control-label">ID LINE</label>
          <input type="text" class="form-control" name="idLine" value="<?= set_value('idLine'); ?>" required>
        </div>
        <div class="form-group col-md-6 col-sm-12">
          <label class="control-label">Email</label>
          <input type="email" class="form-control" name="email" value="<?= set_value('email'); ?>" required>
        </div>
        <div class="form-group col-md-6 col-sm-12">
          <label class="control-label">Moto Hidup</label>
          <textarea class="form-control" style="height: 100px" name="moto" placeholder="Moto hidup" required></textarea>
        </div>
      </div>

      <h4 class="mt-3">Pilihan Divisi</h4>
      <div class="row">
      <div class="form-group col-md-6 col-sm-12">
      <label class="control-label">Pilihan Divisi 1 (wajib)</label>
      <select class="form-control" name="divisi1" required>
        <option value="" disabled selected hidden>Pilih divisi</option>
        <option value="Acara">Acara</option>
        <option value="Humas">Humas</option>
        <option value="Sponsor">Sponsor</option>
        <option value="Transkoper">Transkoper</option>
        <option value="Kemankes">Kemankes</option>
        <option value="DDM">DDM</option>
        <option value="Korlap">Korlap</option>
        <option value="Kodanus">Kodanus</option>
      </select>
        <div class="form-floating mt-3">
            <textarea class="form-control" style="height: 100px" name="alasan1" placeholder="alasan memilih" required></textarea>
        </div>
      </div>

      <div class="form-group col-md-6 col-sm-12">
      <label class="">Pilihan Divisi 2 (opsional)</label>
      <select class="form-control" name="divisi2">
        <option value="" disabled selected hidden>Pilih divisi</option>
        <option value="Acara">Acara</option>
        <option value="Humas">Humas</option>
        <option value="Sponsor">Sponsor</option>
        <option value="Transkoper">Transkoper</option>
        <option value="Kemankes">Kemankes</option>
        <option value="DDM">DDM</option>
        <option value="Korlap">Korlap</option>
        <option value="Kodanus">Kodanus</option>
      </select>
        <div class="form-floating mt-3">
            <textarea class="form-control" style="height: 100px" name="alasan2" placeholder="alasan memilih"></textarea>
        </div>
      </div>
      </div>


    <h4 class="mt-3">Pengalaman</h4>
    <div class="row">
      <?php for ($i=1; $i <= 4; $i++) { ?>
        <div class="form-group col-md-6 col-sm-12">
          <label class="">Pengalaman <?= $i; ?></label>
          <input type="text" class="form-control" name="jabatan<?= $i; ?>" value="<?= set_value('jabatan'); ?>" placeholder="jabatan">
          <input type="text" class="form-control mt-2" name="kegiatan<?= $i; ?>" value="<?= set_value('kegiatan'); ?>" placeholder="kegiatan atau organisasi">
          <input type="text" class="form-control mt-2" name="tahun<?= $i; ?>" value="<?= set_value('tahun'); ?>" placeholder="tahun jabatan">
        </div>
      <?php } ?>
    </div>

    <h4 class="mt-3">SWOT Anda</h4>
    <div class="row">
      <div class="form-group col-md-6 col-sm-12">
        <div class="form-floating mt-3">
          <label class="control-label">Strengths</label>
            <textarea class="form-control" style="height: 100px" name="strengths" placeholder="strengths" required></textarea>
        </div>
      </div>
      <div class="form-group col-md-6 col-sm-12">
        <div class="form-floating mt-3">
          <label class="control-label">Weaknesses</label>
            <textarea class="form-control" style="height: 100px" name="weaknesses" placeholder="weaknesses" required></textarea>
        </div>
      </div>
      <div class="form-group col-md-6 col-sm-12">
        <div class="form-floating mt-3">
          <label class="control-label">Opportunities</label>
            <textarea class="form-control" style="height: 100px" name="opportunities" placeholder="opportunities" required></textarea>
        </div>
      </div>
      <div class="form-group col-md-6 col-sm-12">
        <div class="form-floating mt-3">
          <label class="control-label">Threats</label>
            <textarea class="form-control" style="height: 100px" name="threats" placeholder="threats" required></textarea>
        </div>
      </div>
      <div class="form-group col-md-12 col-sm-12">
        <label class="control-label">Berikan alasan agar kami memilih anda</label>
        <input type="text" class="form-control" name="alasanDiterima" value="<?= set_value('alasanDiterima'); ?>" required placeholder="beri alasan">
      </div>
    </div>


      <!-- <h4 class="mt-3">Pilihan jadwal screening</h4>
      < ?= $this->session->flashdata('messagescreening'); ?>
      <div class="row">
      <div class="form-group col-md-6 col-sm-12">
      <label class="control-label">Pilihan Jadwal Screening 1</label>
      <select class="form-control" name="tanggal1" required>
        <option value="" disabled selected hidden>tanggal</option>
        <option value="4 mei 2021">4 Mei 2021</option>
        <option value="5 mei 2021">5 Mei 2021</option>
        <option value="6 mei 2021">6 Mei 2021</option>
        <option value="7 mei 2021">7 Mei 2021</option>
      </select>
      <select class="form-control mt-3" name="waktu1" required>
        <option value="" disabled selected hidden>jam</option>
        <option value="16.00 - 16.30">16.00 - 16.30 WIB</option>
        <option value="20.00 - 20.30">20.00 - 20.30 WIB</option>
        <option value="20.35 - 21.05">20.35 - 21.05 WIB</option>
        <option value="21.10 - 21.40">21.10 - 21.40 WIB</option>
        <option value="21.45 - 22.15">21.45 - 22.15 WIB</option>
      </select>
      </div> -->

      <!-- <div class="form-group col-md-6 col-sm-12">
      <label class="control-label">Pilihan Jadwal Screening 2</label>
      <select class="form-control" name="tanggal2" required>
        <option value="" disabled selected hidden>tanggal</option>
        <option value="4 mei 2021">4 Mei 2021</option>
        <option value="5 mei 2021">5 Mei 2021</option>
        <option value="6 mei 2021">6 Mei 2021</option>
        <option value="7 mei 2021">7 Mei 2021</option>
      </select>
      <select class="form-control mt-3" name="waktu2" required>
        <option value="" disabled selected hidden>jam</option>
        <option value="16.00 - 16.30">16.00 - 16.30 WIB</option>
        <option value="20.00 - 20.30">20.00 - 20.30 WIB</option>
        <option value="20.35 - 21.05">20.35 - 21.05 WIB</option>
        <option value="21.10 - 21.40">21.10 - 21.40 WIB</option>
        <option value="21.45 - 22.15">21.45 - 22.15 WIB</option>
      </select>
      </div>
      </div> -->

      <div class="form-group mt-3">
        <label class="control-label">"Link pengumpulan berkas pendaftaran (Link Google Drive)</label><br>
        <span  class="mt-2">Drive berisi:</span>
        <ul style="color:grey; padding-left:30px;">
            <li> Pas foto formal (wajib)</li>
            <li> Surat komitmen</li>
            <li> Portfolio design (untuk DDM)</li>
        </ul>
        <input type="text" class="form-control mt-2" name="link" value="<?= set_value('link'); ?>" required placeholder="link drive">
        <span style="color:grey;">Pastikan permission link sudah di set menjadi <span style="color:red;">"anyone with this link"</span></span>
      </div>
      <div class="form-check">
       <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
       <label class="form-check-label" for="exampleCheck1">Saya sudah yakin bahwa data yang saya isi sudah benar dan sesuai dengan ketentuan yang berlaku.</label>
      </div>
      <div class="text-right mt-4">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
   </div>
      </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

  </body>
</html>
