<div class="container">
  <img src="<?= base_url('asset/img/kbmsi_event_text.svg') ?>" class="event-text" alt="">

  <form action="<?= base_url('events/department') ?>" method="GET">
    <div class="form-group btn-group haha" role="group" aria-label="Basic example">
      <a href="<?= base_url('events') ?>" class="btn btn-filter btn-event <?= ($this->input->get('filter') == null) ? "active" : ""; ?>">Aktif</a>
      <button type="submit" name="filter" value="semua" class="btn btn-filter btn-event <?= ($this->input->get('filter') == "semua") ? "active" : ""; ?>">Semua</button>
      <?php foreach ($department as $dept) : ?>
        <button type="submit" name="filter" value="<?= $dept['singkatan'] ?>" class="btn btn-filter btn-event <?= ($this->input->get('filter') == $dept['singkatan']) ? 'active' : " "; ?> "><?= $dept['singkatan']; ?></button>
      <?php endforeach; ?>
    </div>
  </form>

  <div class="row">
    <?php foreach ($events as $event) : ?>
      <div class="col l4 m6 s12">
        <div class="card">
          <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" src="<?= base_url($event->img_path) ?>">
          </div>
          <div class="card-content">
            <span class="card-title activator grey-text text-darken-4"> <?= $event->name ?> <i class="material-icons right">more_vert</i></span>
            <?php if ($event->isFull && !$event->isRegistered) : ?>
              <p><a href="<?= 'javascript:void(0)' ?>"> <button disabled class="waves-effect waves-light btn wangsit-color" style=" width: 100%"> Maaf Kuota Penuh </button> </a></p>
            <?php else : ?>
              <?php if ($event->id_event == 9) { ?>
                <p><a href="<?= !$event->isRegistered && !$event->isDone ? base_url("event/register/$event->id_event") : 'javascript:void(0)' ?>"> <button <?= ($event->isRegistered || $event->isDone) ? 'disabled' : '' ?> class="waves-effect waves-light btn wangsit-color" style=" width: 100%"> <?= $event->isRegistered ? 'Sudah Terdaftar' : 'Registration Closed' ?> </button> </a></p>
              <?php  } else { ?>
                <p><a href="<?= !$event->isRegistered && !$event->isDone ? base_url("event/register/$event->id_event") : 'javascript:void(0)' ?>"> <button <?= ($event->isRegistered || $event->isDone) ? 'disabled' : '' ?> class="waves-effect waves-light btn wangsit-color" style=" width: 100%">
                      <?php
                      if ($event->isRegistered) :
                        echo 'Sudah Terdaftar';
                      elseif ($event->isDone) :
                        echo 'Registration Closed';
                      elseif ($event->start_date < date("Y-m-d")) :
                        echo 'Announcement';
                      else :
                        echo 'Register';
                      // echo 'Announcement';
                      endif; ?>
                    </button> </a></p>
              <?php } ?>
            <?php endif ?>
          </div>
          <div class="card-reveal">
            <span class="card-title grey-text text-darken-4"> <b> <?= $event->name ?> </b> <i class="material-icons right">close</i></span>
            <p> <?= $event->description ?> </p>
          </div>
        </div>
      </div>
    <?php endforeach ?>
  </div>

  <!-- <div class="row">
    <?php foreach ($events as $event) : ?>
      <div class="col l4 m6 s12">
      <div class="card">
        <div class="card-image waves-effect waves-block waves-light">
          <img class="activator" src="<?= base_url($event->img_path) ?>">
        </div>
        <div class="card-content">
          <span class="card-title activator grey-text text-darken-4"> <?= $event->name ?> <i class="material-icons right">more_vert</i></span>
         <?php if ($event->isFull && !$event->isRegistered) : ?>
          <p><a href="<?= 'javascript:void(0)' ?>" > <button disabled class="waves-effect waves-light btn wangsit-color" style=" width: 100%"> Maaf Kuota Penuh </button> </a></p>
          <?php else : ?> 
             <?php if ($event->id_event == 9) { ?>
              <p><button disabled class="waves-effect waves-light btn wangsit-color" style=" width: 100%"> <?= $event->isRegistered ? 'Sudah Terdaftar' : 'Pengumuman' ?> </button></p>
          <?php  } else { ?>
            <p><button disabled class="waves-effect waves-light btn wangsit-color" style=" width: 100%"> <?= $event->isRegistered ? 'Sudah Terdaftar' : 'Registration Closed' ?> </button></p>
          <?php } ?>
          <?php endif ?>
        </div>
        <div class="card-reveal">
          <span class="card-title grey-text text-darken-4"> <b> <?= $event->name ?> </b> <i class="material-icons right">close</i></span>
          <p> <?= $event->description ?> </p>
        </div>
      </div>
    </div>
    <?php endforeach; ?> -->
</div>
</div>