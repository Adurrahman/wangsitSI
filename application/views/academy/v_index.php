<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127828297-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-127828297-1');
	</script>
	<meta charset="utf-8"/>
	<meta http-equiv="x-ua-compatible" content="ie=edge"/>
	<title>Wangsit Academy</title>
	<meta content="" name="description"/>
	<meta content="" name="keywords"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta content="telephone=no" name="format-detection"/>
	<meta name="HandheldFriendly" content="true"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="asset/css/academy.css"/>
	<link rel="icon" type="image/x-icon" href="favicon.png"/>
</head>
<body>

	<section>
		<div class="academy-header">
			<a href="<?php echo base_url();?>">					
					<img class="center-align" src="<?php echo base_url();?>asset/img/text-logo.png" alt="logo-wangsit">
			</a>
			<h1> Academy </h1>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-12">
					<ul class="academy__navigation">
						<li>
							<a href="#" class="cat-trigger active">Semua</a>
						</li>
						<?php foreach($categories as $category) : ?>
							<li>
								<a href="#" class="cat-trigger"> <?= $category->category ?> </a>
							</li>
						<?php endforeach ?>
					</ul>
					<input type="text" class="search-form" id="search" placeholder="Search Eg. investasi, Etc">
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<?php foreach ($courses as $course) : ?>
					<div class="academy-card col-lg-4 col-sm-12" data-category="<?= $course->category ?>" data-courseName="<?= str_replace(" ","-",$course->name) ?>">
						<div class="academy-card__content">
							<div class="academy-card__img-item">
								<img src="assets/media/components/b-gallery/gallery-1.jpg" alt="/" class="img-responsive">
								<a href="<?= base_url("Academy/redirect/$course->id") ?>">
									<div class="academy-card__overlay-hover"></div>
								</a>
							</div>
							<div class="academy-card__desc-wrapper">
								<p class="academy-card__title">
									<?= $course->name ?>
								</p>
								<p class="academy-card__category">
									<?= $course->category ?>
								</p>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</section>

	<footer>
		<div class="container text-center" style="color:#55254e">
			<br>
			Made with &#10084; by<a href="http://kbmsi.filkom.ub.ac.id/lembaga/emsi/p2s/"> <b style="color:#55254e">P2S EMSI 2021</b></a>
			<br>
		</div>
	</footer>
	<!-- MAIN SCRIPTS-->
	<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	<!-- Bootstrap-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="asset/js/wangsitAcademy.js"></script>
</body>
</html>