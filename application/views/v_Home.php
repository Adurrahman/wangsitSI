<div class="container">
	<div class="section">
		<div class="row">
			<?= $belumAktivasi; ?>
			<div class="col l8 s12">
				<div class="card">
					<div class="card-content wangsit-color white-text" style="padding: 10px 30px 10px 15px;;">
						<div class="fontt"><b>Apps </b></div>
					</div>
					<div class="card-action">
						<div id="kbmsiProduct" class="owl-carousel owl-theme">
							<div class="item"><a href="https://ecomplaint.kbmsi.or.id/" target="_BLANK" style="margin: 0px;"><img src="<?= base_url(); ?>asset/img/pinjam-ruang.jpg" style="width: 100%; border-radius: 10px;"></a></div>
							<div class="item"><a href="http://kbmsi.filkom.ub.ac.id" target="_BLANK" style="margin: 0px;"><img src="<?= base_url(); ?>asset/img/pinjam-ruangg.jpg" style="width: 100%; border-radius: 10px;"></a></div>
							<div class="item"><a href="https://line.me/R/ti/p/%40hvu9898p" target="_BLANK" style="margin: 0px;"><img src="<?= base_url(); ?>asset/img/pinjam-ruanggg.jpg" style="width: 100%; border-radius: 10px;"></a></div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-content wangsit-color white-text" style="padding: 10px 30px 10px 15px;;">
						<div class="fontt"><b>Filkom Information</b></div>
					</div>

					<ul class="collection" style="margin-top: 0px;">

						<?php

						foreach ($information as $key) { ?>

							<li class="collection-item">
								<div class="row" style="margin-bottom: 0px;">
									<div class="col s10 m11">
										<b><?= $key->nama ?></b>
										<br>
										<span class="grey-text"><?= $key->tanggal ?></span>
									</div>
									<div class="col s2 m1" style="position: relative; top: 10px;">
										<a href="<?= $key->info ?>" target="_BLANK"><i class="material-icons">send</i></a>
									</div>
								</div>
							</li>
						<?php
						}
						?>
					</ul>


				</div>
			</div>

			<div class="col l4 s12">
				<!-- <div class="card">
					<a href="<?= base_url('article/create') ?>" class="<?= $isActive; ?> btn btn-large" style="width: 100%;"><i class="material-icons right">add</i>Add Article</a>
				</div> -->
				<div class="card">
					<div class="card-image" style="z-index: 0">
						<img src="<?= base_url(); ?>asset/img/bg-bawah.jpg" style="height: 120px;" alt="">
					</div>
					<div class="card-content" style="padding-bottom: 0px;">
						<center>
							<div style="position: relative;">
								<img class="circle z-depth-3 materialboxed" src="<?= $_SESSION['userData']['GAMBAR']; ?>" style="border: solid white; width: 100px; height: 100px; margin-top: -75px;  z-index: 10;">
							</div>
						</center>
						<div class="center-align" style="margin-top: 20px;">
							<h5 class="font" style="font-size: 18px;"><b><?= $_SESSION['userData']['USERNAME'] ?></b></h5>
							<h5 class="font grey-text" style="font-size: 14px;"><?= $_SESSION['userData']['NAMA'] ?></h5>
							<h5 style="font-size: 14px"><?= $_SESSION['userData']['NIM'] ?></h5>
						</div>
					</div>
					<div class="card-action" style="padding-bottom: 5px; margin-top: 20px;">
						<div class="row" style="margin-left: -20px; margin-right: -20px;">
							<div class="col s12 center-align">
								<i class="material-icons" style="font-size: 40px;">person</i>
								<div class="font">Angkatan <?= '20' . substr($_SESSION['userData']['NIM'], 0, 2); ?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-content wangsit-color white-text" style="padding: 10px 30px 10px 15px;;">
						<div class="fontt"><b>Happy Birthday!</b></div>
					</div>
					<ul class="collection" style="margin-top: 0px;">
						<?php if ($sbds == null) { ?>
							<li class="collection-item center-align">
								Tidak ada yang berulang tahun hari ini
							</li>
						<?php
						}
						foreach ($sbds as $sbd) {
						?>
							<li class="collection-item">
								<span style="position: absolute;">
									<img src="<?= $sbd->GAMBAR; ?>" class="circle" width="42px;" style="margin-left: -10px;" alt="">
								</span>
								<div style="margin-left: 45px;" class="wangsit-text">
									<b><?= $sbd->NAMA; ?></b>
									<br>
									<div class="grey-text"><?= $sbd->TTL ?></div>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>