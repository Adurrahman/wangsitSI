<?php 

if(!function_exists('isAuthorized')){
    function isAuthorized(){        
      return $_SESSION['admin']->role == 'ketua' ||
             $_SESSION['admin']->role == 'wakil' ||
             $_SESSION['admin']->id_user == 14   ||
             $_SESSION['admin']->id_user == 72   ||
             $_SESSION['admin']->id_user == 33;
    }
}

if(!function_exists('isResearchDepartment')){
    function isResearchDepartment(){        
      return $_SESSION['admin']->division == 'PENELITIAN DAN PENGEMBANGAN STUDI';
    }
}

if(!function_exists('getGenerationRange')){
    function getGenerationRange(){
      $year   = substr($_SESSION['admin']->nim, 0, 2);
      $range  = new stdClass();

      $range->min = $year + 2;
      $range->mid = $year + 1;
      $range->max = $year;

      return $range;
    }
}

if(!function_exists('getGeneration')){
    function getGeneration($nim){
      $year = substr($nim, 0, 2);
      return "20". $year;
    }
}

if(!function_exists('isSuperAdmin')){
    function isSuperAdmin(){
      return $_SESSION['admin']->id_user == 14 ||
             $_SESSION['admin']->id_user == 72 ||
             $_SESSION['admin']->id_user == 33;
    }
}

if(!function_exists('isLoggedIn')){
  function isLoggedIn(){
    if(!isset($_SESSION['admin']))
     redirect('admin-login');
  }
}
