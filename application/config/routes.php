<?php
defined('BASEPATH') or exit('No direct script access allowed');

//ADMIN
$route['admin-login']               = 'Admin/User';
$route['admin/dashboard']           = 'Admin/Dashboard';
$route['admin/wangsitAcademy']      = 'Admin/Academy';
$route['admin/academyCategory']     = 'Admin/Academy/category';
$route['admin/members']             = 'Admin/Division';
$route['admin/addMember']           = 'Admin/Division/addMember';
$route['admin/divisions']           = 'Admin/Division/showDivisions';
$route['admin/events']              = 'Admin/Event';
$route['admin/addEvent']            = 'Admin/Event/addEvent';
$route['admin/eventDetail/(:num)']  = 'Admin/Event/eventDetail';
$route['admin/updateEvent']         = 'Admin/Event/updateEvent';
$route['admin/forms/(:num)']        = 'Admin/Event/choiceForm';
$route['admin/users']               = 'Admin/User/listUser';
$route['admin/activation']          = 'Admin/User/Activation';
$route['admin/activate']            = 'Admin/User/activate';
$route['admin/editUser']            = 'Admin/User/editUser';
$route['admin/bannedUsers']         = 'Admin/User/listBannedUsers';
$route['admin/restoreUser']         = 'Admin/User/restoreUser';
$route['admin/banUser']             = 'Admin/User/banUser';



// USER
$route['login']           = 'Authentication';
$route['logout']          = 'Authentication/logout';
$route['register']        = 'Authentication/Daftar';
$route['profile/(:any)']  = 'Authentication';
$route['dashboard']       = 'Home';
$route['coming-soon']     = 'Home/comingSoon';

$route['events']                      = 'Event';
$route['events/department']           = 'Event/department';
$route['event/register/(:num)']       = 'Event/register';
$route['event/registerValidation']    = 'Event/registerValidation';

$route['event/statusDaftarEvent']     = 'Event/statusDaftar';

// Wow
$route['event/login']                     = 'EventPsdm/loginWOW';
$route['event/submitwow']                 = 'Event/validasiWow';
$route['pendaftaranberhasil']             = 'EventPsdm2/submit_formWow';
$route['event/form']                      = 'EventPsdm/isi_form';
$route['event/cek']                       = 'EventPsdm/cek_akun';
$route['adminwow']                       = 'AdminEventPsdm/pageLogin';
$route['admin/loginAdminwow']                = 'AdminEventPsdm/login';
$route['admin/dashboardwow']                 = 'AdminEventPsdm/dashboard';
$route['admin/logoutwow']                    = 'AdminEventPsdm/logout';
// $route['event/submit']                    = 'EventPsdm/submit_form';
$route['event/status']                    = 'EventPsdm/status';


// Bimsak
// $route['event/login']                           = 'EventPsdm/loginWOW';
$route['event/formBimsak']                      = 'EventSosma/isi_form';
// $route['event/cekBimsak']                       = 'EventSosma/cek_akun';
// $route['event/submitBimsak']                    = 'EventSosma/submit_form';
$route['event/submitBimsak']                    = 'Event/validasiBimsak';
$route['pendaftaranBimsakBerhasil']             = 'EventSosma/submit_formBimsak';
$route['event/statusBimsak']                    = 'EventSosma/status';
$route['event/statusDaftarBimsak']              = 'EventSosma/statusDaftar';
$route['event/terima']                          = 'EventSosma/terima';
$route['event/tolak']                           = 'EventSosma/tolak';
$route['adminbimsak']                           = 'AdminEventSosma/pageLogin';//baru mo dibuat
$route['event/loginAdminBimsak']                = 'AdminEventSosma/login';
$route['event/dashboardBimsak']                 = 'AdminEventSosma/dashboard';
$route['event/logoutBimsak']                    = 'AdminEventSosma/logout';

// Starship
$route['event/formStarship']                      = 'EventStarship/isi_form';
$route['event/submitStarship']                    = 'EventStarship/submit_form';
$route['event/statusStarship']                    = 'EventStarship/status';
$route['event/statusDaftarStarship']              = 'EventStarship/statusDaftar';
$route['event/terimaStarship']                    = 'EventStarship/terima';
$route['event/tolakStarship']                     = 'EventStarship/tolak';
$route['event/loginAdminStarship']                = 'AdminEventStarship/login';
$route['event/dashboardStarship']                 = 'AdminEventStarship/dashboard';
$route['event/logoutStarship']                    = 'AdminEventStarship/logout';

// Oprec
$route['event/formOprec']                      = 'EventOprec/isi_form';
$route['event/submitOprec']                    = 'EventOprec/submit_form';
$route['event/statusOprec']                    = 'EventOprec/status';
$route['event/statusDaftarOprec']              = 'EventOprec/statusDaftar';
$route['event/terima']                         = 'EventOprec/terima';
$route['event/tolak']                          = 'EventOprec/tolak';
$route['event/loginAdminOprec']                = 'AdminEventOprec/login';
$route['event/dashboardOprec']                 = 'AdminEventOprec/dashboard';
$route['event/logoutOprec']                    = 'AdminEventOprec/logout';

// SKRIM
$route['event/formSkrim']                      = 'EventP2S/isi_form';
$route['event/submitSkrim']                    = 'EventP2S/submit_form';
$route['event/statusSkrim']                    = 'EventP2S/status';
$route['event/loginAdminSkrim']                = 'AdminEventP2S/login';
$route['event/dashboardSkrim']                 = 'AdminEventP2S/dashboard';
$route['event/logoutSkrim']                    = 'AdminEventP2S/logout';

// Routes for Oprec MRFEST 2022
// ******** KBMSI Periode 2022 *********//
$route['event/submitmrfest']                 = 'Event/validasiMrfest';
$route['registerasiberhasil']               = 'EventPsdm2/submit_formMrfest';
$route['adminmrfest']                       = 'AdminEventPsdm2/pageLogin';
$route['admin/loginAdminMRFEST']                       = 'AdminEventPsdm2/login';
$route['admin/dashboardMRFEST']                 = 'AdminEventPsdm2/dashboard';
$route['admin/logoutMRFEST']                    = 'AdminEventPsdm2/logout';
// ******** KBMSI Periode 2022 *********//

// $route['event/loginMRFEST']                     = 'EventPsdm2/login';
// $route['event/formMRFEST']                      = 'EventPsdm2/isi_form';
// $route['event/cekMRFEST']                       = 'EventPsdm2/cek_akun';
// $route['event/submitMRFEST']                    = 'EventPsdm2/submit_form';
// $route['event/statusMRFEST']                    = 'EventPsdm2/status';

$route['article']               = 'Article';
$route['article/create']        = 'Article/create';
$route['article/(:num)']        = 'Article';
$route['article/(:any)']        = 'Article/Kategori/';

$route['wkwkwkwk']              = 'admin/Wakwaw';
$route['admin/event']           = 'admin/Wakwaw/event';
$route['admin/event/(:num)']    = 'admin/event';
$route['academy']         = 'Academy';
$route['(:any)']          = 'Home/userProfile';
$route['(:any)/(:any)']         = 'Article/Detail/';
$route['article/(:any)/(:num)'] = 'Article/Kategori/';
// $route['(:any)/(:num)']   = 'Home/Profil_Orang';
//wangsit.kbmsi.or.id/artcile/$username
//wangsit.kbmsi.or.id/artcile/$username/$slug


$route['404_override'] = '';
$route['default_controller'] = 'Authentication';
$route['translate_uri_dashes'] = FALSE;
