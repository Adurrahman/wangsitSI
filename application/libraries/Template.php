<?php 

class Template{

  protected $_ci;

  function __construct() {
      $this->_ci = &get_instance();
  }

  function render( $page = NULL, $data = NULL){
    if ($page != NULL){
      $data['_head']           = $this->_ci->load->view('layout/head', $data, TRUE);
      $data['_css']            = $this->_ci->load->view('layout/css', $data, TRUE);
      $data['_navbar']         = $this->_ci->load->view('layout/navbar', $data, TRUE);
      $data['_body']           = $this->_ci->load->view( $page, $data, TRUE);
      $data['_footer']         = $this->_ci->load->view('layout/footer', $data, TRUE);
      $data['_js']             = $this->_ci->load->view('layout/js', $data, TRUE);
      echo $data['_template']  = $this->_ci->load->view('layout/template', $data, TRUE);
    }
  }
  
  function renderAdminView($page = NULL, $data = NULL){
    if ($page != NULL){
      $data['_head']           = $this->_ci->load->view('layout/admin/head', $data, TRUE);
      if(isset($_SESSION['admin'])){
        $data['_navbar']         = $this->_ci->load->view('layout/admin/navbar', $data, TRUE);
        $data['_sidebar']        = $this->_ci->load->view('layout/admin/sidebar', $data, TRUE);
      }
      $data['_body']           = $this->_ci->load->view( $page, $data, TRUE);
      $data['_footer']         = $this->_ci->load->view('layout/admin/footer', $data, TRUE);
      $data['_js']             = $this->_ci->load->view('layout/admin/js', $data, TRUE);
      echo $data['_template']  = $this->_ci->load->view('layout/admin/template', $data, TRUE);
    }
  }

}