<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminEventP2S extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->model('M_EventP2S');
  }

  public function login()
  {
    if ($this->session->userdata('user') != "") {
      redirect('event/dashboardSkrim');
    }
    $akun = $this->input->post('akun');
    $pass = $this->input->post('password');
    if ($akun == "adminP2S" && $pass == "suksesTerus") {
      $this->session->set_userdata('user', $akun);
      redirect('event/dashboardSkrim');
    } else {
      $this->load->view('Event/Skrim/loginAdmin');
    }
  }

  public function dashboard()
  {
    if ($this->session->userdata('user') != "") {
      $data['pendaftar'] = $this->M_EventP2S->getAllPendaftar();
      $this->load->view('Event/Skrim/dashboard', $data);
    } else {
      redirect('event/loginAdminSkrim');
    }
  }
  public function logout()
  {
    $this->session->unset_userdata('user');
    redirect('event/loginAdminSkrim');
  }

  public function ubah($id_event, $key)
  {
    $this->M_EventP2S->ubahStatus($id_event, $key);
    redirect('event/dashboardSkrim');
  }
}
