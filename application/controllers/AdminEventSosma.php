<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminEventSosma extends CI_Controller {
  function __construct(){
    parent::__construct();
      $this->load->library('session');
      $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('M_EventSosma');
  }

   //new login page
  public function pageLogin()
  {
    if ($this->session->userdata('user') != "") {
      redirect('event/dashboardBimsak');
    } else {
      $this->load->view('Event/Bimsak/loginAdmin');
    }
  }

  public function login(){
    if ($this->session->userdata('user')!="") {
				redirect('event/dashboardBimsak');
		}
    $akun= $this->input->post('akun');
    $pass= $this->input->post('password');
    if ($akun=="adminSosma" && $pass=="suksesYaa") {
  			$this->session->set_userdata('user', $akun);
        redirect('event/dashboardBimsak');
      }
      else{
        $this->load->view('Event/Bimsak/loginAdmin');
      }
    }

  public function dashboard(){
    if ($this->session->userdata('user')!="") {
    $data['pendaftar']=$this->M_EventSosma->getAllPendaftar();
    $this->load->view('Event/Bimsak/dashboard',$data);
  }
  else {
    redirect('event/loginAdminBimsak');
  }
}
  public function logout(){
    $this->session->unset_userdata('user');
	redirect('adminbimsak');
  }

  public function ubah($id_event, $key)
 {
	 $this->M_EventSosma->ubahStatus($id_event, $key);
	 redirect('event/dashboardBimsak');
 }
}
