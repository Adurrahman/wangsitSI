<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crystal extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('login')==false){
			redirect('');
		} 
	}
	public function index()
	{
		$this->load->view('proker/V_Skrim');		
	}

}

/* End of file Skrim.php */
/* Location: ./application/controllers/proker/Skrim.php */