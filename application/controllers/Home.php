<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Kbmsi');
		$this->load->library('pagination');
	}

	public function index(){
		$activationMessage = '<div class="col s12" style="margin-top: 25px;"> <div class="chip red white-text" style="width: 100%; border-radius: 5px;"> Akun anda belum teraktivasi, silakan menguhubungi staff P2S untuk melakukan aktivasi </div> </div>';
		if($this->session->userdata('login')){
			$filkomAnnounceData = json_decode(file_get_contents('http://api.kbmsi.or.id/scrapping/scrapppengumumanfilkom.php'));
// 			echo "<script>console.log('Debug Objects: " . $filkomAnnounceData . "' );</script>";
			$announcement = $this->getTopFiveData($filkomAnnounceData);

			$data['information'] 		= $announcement;
			$data['belumAktivasi'] 	= $this->isActivated() ? '' : $activationMessage;
			$data['isActive'] 			= $this->isActivated() ? '' : 'disabled';			
			$data['userdata'] 			= $this->session->userdata();
			$data['sbds'] 					= $this->M_Kbmsi->getBDayData();
			$data['title']					= "Dashboard | Wangsit";
			$data['library']				= "owlCarousel";
			$data['kbmsiBirthday']	= $this->M_Kbmsi->getBDayData();;
			$this->template->render('v_Home',$data);
		}
		else
			redirect($_SERVER['HTTP_REFERER']);		
	}
	
	public function userProfile(){
		$username 		= $this->uri->segment(1);

		if (!isset($_SESSION['userData'])) {
			$_SESSION['userData'] = null;
		}
		
		if($this->M_Kbmsi->cekUsername($username)){
			$this->config->load('Custom_Halaman',TRUE);
			$config = $this->config->item('Custom_Halaman');
			$config['use_page_numbers'] = TRUE; 
			$config['base_url'] 				= base_url().'/'.$this->M_Kbmsi->getID($username);
			$config['total_rows'] 			= $this->db->get_where('article_article', array('PENULIS' => $username))->num_rows();
			$config['per_page'] 				= 5;
			$this->pagination->initialize($config);

			$index = $this->uri->segment(2) == null ? 0 : $this->uri->segment(2)*5 - 5;
			$data['pagination']			=	$this->pagination->create_links();
			$data['isUserProfile'] 	= $_SESSION['userData']['USERNAME'] == $username;
			$data['Article'] 				= $this->M_Kbmsi->getArticleUser(5,$index,$username);
			$data['user'] 					= $this->M_Kbmsi->getDetailMahasiswa($username);
			$data['title']					= "$username | Wangsit";
			$data['library']				= NULL;
			$this->template->render('v_profile', $data);
		}
		else
			redirect('','refresh');
	}

	function comingSoon(){
		$data['title'] = "Coming Soon";

		if(!empty($_SESSION['admin']))
			$this->template->renderAdminView('layout/coming_soon.php', $data);
		else 
			$this->template->render('layout/coming_soon.php', $data);
	}

	function getTopFiveData($data){
		$counter 	= 0;
		$result 	= [];

		foreach ($data as $item) {
			if($counter++ < 5)
				array_push($result, $item);
			else break;
		}
		return $result;
	}

	function isActivated(){
		$session = $this->session->userdata('userData');
		return $this->M_Kbmsi->getStatus($session['USERNAME']) != 0;
	}

}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */