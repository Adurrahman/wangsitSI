<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EventSosma extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (!isset($_SESSION['userData']))
			redirect('/');

		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('M_EventSosma');
		$this->load->model('M_user', 'user');
		$this->load->model('M_Event', 'event');
		$this->load->model('M_EventPsdm2');
	}

	public function isi_form()
	{
		$data['user']           = $this->user->getUserData($_SESSION['userData']['ID']);
		$cek = $this->M_EventSosma->cekDaftar($data['user']->NIM);
		$angkatan = substr($data['user']->NIM, 0, 5);
		if ($angkatan != "19515" && $angkatan != "20515") {
			$this->session->set_flashdata('message', 'Maaf Pendaftar bukan angkatan 2019 atau 2020');
			redirect('event/statusDaftarBimsak');
		}
		$data['cek'] = $cek;
		if ($cek != 0) {

			$pendaftar = $this->M_EventSosma->Pendaftar($data['user']->NIM)->row_array();
			if ($pendaftar['diterima'] == 1) {
				$this->session->set_flashdata('message', 'Halo');
				$this->session->set_flashdata('nama', $data['user']->NAMA);
				redirect('event/terima');
			} else {
				$this->session->set_flashdata('message', 'Mohon maaf ya');
				$this->session->set_flashdata('nama', $data['user']->NAMA);
				redirect('event/tolak');
			}
		}
		$this->load->view('Event/Bimsak/tidakDaftar');
	}

	public function submit_formBimsak()
	{
		$data['user']           = $this->user->getUserData($_SESSION['userData']['ID']);
		$eventWithId = $this->event->getEventById('17');
		if ($eventWithId->start_date > date("Y-m-d")) {
			$this->template->render('Event/Bimsak/afterRegister');
		} else {
			$data["cekPengumuman"] = $this->M_EventSosma->cekDiterima($data['user']->NIM);
			$this->load->view('Event/Bimsak/AnnouncementLolos', $data);
		}
	}

	public function terima()
	{
		$this->load->view('Event/Bimsak/terima');
	}

	public function tolak()
	{
		$this->load->view('Event/Bimsak/tolak');
	}




	public function submit_form()
	{

		$data['user']           = $this->user->getUserData($_SESSION['userData']['ID']);
		$nama = $data['user']->NAMA;
		$nim = $data['user']->NIM;
		$no_tlp = $this->input->post('noHp');
		$id_line = $this->input->post('idLine');
		$divisi1 = $this->input->post('divisi1');
		$alasan1 = $this->input->post('alasan1');
		$divisi2 = $this->input->post('divisi2');
		$alasan2 = $this->input->post('alasan2');

		$link = $this->input->post('link');

		$data = array(
			'nama' => $nama,
			'nim' => $nim,
			'no_tlp' => $no_tlp,
			'id_line' => $id_line,
			'divisi1' => $divisi1,
			'alasan1' => $alasan1,
			'divisi2' => $divisi2,
			'alasan2' => $alasan2,

			'link' => $link
		);
		$this->M_EventSosma->insertForm($data);
		redirect('event/statusBimsak');
	}

	public function status()
	{
		$this->load->view('Event/Bimsak/status');
	}

	public function statusDaftar()
	{
		$this->load->view('Event/Bimsak/statusDaftar');
	}
}
