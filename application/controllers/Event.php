<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Event extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    if (!isset($_SESSION['userData']))
      redirect('/');

    $this->load->library('form_validation');
    $this->load->model('M_Event', 'event');
    $this->load->model('M_EventPsdm');
    $this->load->model('M_EventPsdm2');
    $this->load->model('M_EventSosma');
    $this->load->model('M_user', 'user');
  }

  public function index()
  {
    $data['title']  = "Event";
    $events         = $this->event->getEvents();
    $pastEvents     = $this->event->getPastEvents();
    $data['department'] = $this->event->getAllDepartment();

    foreach ($events as $event) {
      $event->isRegistered  = $this->event->checkRegisterStatus($event->id_event);
      $event->isFull        = $this->event->getQuota($event->id_event)->jumlah >= $event->quota;
      $event->isDone        = $this->event->isFinishedEvent($event->id_event);
    }

    $data['events'] = $events;
    $data['pastEvents'] = $pastEvents;
    $this->template->render('Event/dashboard', $data);
  }

  public function register()
  {
    $id_event           = $this->uri->segment(3);
    if ($this->event->checkRegisterStatus($id_event))
      redirect('/events');

    // if ($id_event==7) {
    // 	redirect('event/login');
    // }

    $data['title']          = "Register";
    $data['event']          = $this->event->getEventById($id_event);
    $data['user']           = $this->user->getUserData($_SESSION['userData']['ID']);
    // $cek = $this->M_EventSosma->cekDaftar($data['user']->NIM);
    // $data['user']->angkatan = "20" . substr($data['user']->NIM, 0, 2);
    $data['user']->angkatan = "20" . substr($data['user']->NIM, 0, 2);
    $angkatan = $data['user']->angkatan;
    if ($id_event == 13) {
      if (!($angkatan == "2021")) {
        $this->session->set_flashdata('message', 'Maaf Pendaftar bukan angkatan 2021');
        redirect('event/statusDaftarEvent');
      }
    }

    if ($id_event == 9) {
      redirect('event/formBimsak', $data);
    }

    if ($id_event == 10) {
      redirect('event/formSkrim', $data);
    }

    if ($id_event == 12) {
      redirect('event/formStarship', $data);
    }

    if ($id_event == 14) {
      redirect('event/formOprec', $data);
    }

    if ($id_event == 15) {
      $cekStatusDaftar = $this->M_EventPsdm->cekDaftar($data['user']->NIM);
      $eventWithId = $this->event->getEventById('15');

      if ($cekStatusDaftar) {
        redirect('pendaftaranberhasil');
      } else {
        if ($eventWithId->end_date < date("Y-m-d")) {
          $this->load->view('Event/Wow/closeReg'); //ini nanti gannti yaa
        } else {
          $this->template->render('Event/Wow/formDaftar', $data);
        }
      }
    }

    if ($id_event == 16) {
      $cekStatusDaftar = $this->M_EventPsdm2->cekDaftar($data['user']->NIM);
      $eventWithId = $this->event->getEventById('16');

      if ($cekStatusDaftar) {
        redirect('registerasiberhasil');
      } else {
        if ($eventWithId->end_date < date("Y-m-d")) {
          $this->load->view('Event/Mrfest/closeReg'); //ini nanti gannti yaa
        } else {
          $this->template->render('Event/Mrfest/formDaftar', $data);
        }
      }
    }

    //BIMASAKTI 7.0
    if ($id_event == 17) {
      $cekStatusDaftar = $this->M_EventSosma->cekDaftar($data['user']->NIM);
      $eventWithId = $this->event->getEventById('17');

      if ($cekStatusDaftar) {
        redirect('pendaftaranBimsakBerhasil');
      } else {
        if ($eventWithId->end_date < date("Y-m-d")) {
          $this->load->view('Event/Bimsak/closeReg');
        } else {
          $this->template->render('Event/Bimsak/formDaftar', $data);
        }
      }
    }
  }

  public function validasiWow()
  {
    $data['user']           = $this->user->getUserData($_SESSION['userData']['ID']);
    $nama = $data['user']->NAMA;
    $nim = $data['user']->NIM;
    $id_line = $this->input->post('idLine');
    $ttl = $this->input->post('ttl');
    $noPhone = $this->input->post('noPhone');
    $kelebihan = $this->input->post('kelebihan');
    $kekurangan = $this->input->post('kekurangan');
    $motivasi = $this->input->post('motivasi');
    $dept1 = $this->input->post('dept1');
    $dept2 = $this->input->post('dept2');
    $alasan1 = $this->input->post('alasan1');
    $alasan2 = $this->input->post('alasan2');
    $jadwal1 = $this->input->post('jadwal1');
    $jadwal2 = $this->input->post('jadwal2');
    $linkDrive = $this->input->post('linkDrive');

    $data2 = array(
      'nama' => $nama,
      'nim' => $nim,
      'ttl' => $ttl,
      'noPhone' => $noPhone,
      'kelebihan' => $kelebihan,
      'kekurangan' => $kekurangan,
      'motivasi' => $motivasi,
      'idLine' => $id_line,
      'dept1' => $dept1,
      'dept2' => $dept2,
      'jadwal1' => $jadwal1,
      'jadwal2' => $jadwal2,
      'alasan1' => $alasan1,
      'alasan2' => $alasan2,
      'linkDrive' => $linkDrive,
      'diTerima' => false
    );
    if ($jadwal1 == $jadwal2 && $dept1 == $dept2) {
      $this->session->set_flashdata('messageschedule', 'Opsi jadwal screening tidak boleh sama');
      $this->session->set_flashdata('messagedept', 'Opsi divisi tidak boleh sama');
      redirect('event/register/15', $data);
    } else if ($jadwal1 == $jadwal2) {
      $this->session->set_flashdata('messageschedule', 'Opsi jadwal screening tidak boleh sama');
      redirect('event/register/15', $data);
    } else if ($dept1 == $dept2) {
      $this->session->set_flashdata('messagedept', 'Opsi divisi tidak boleh sama');
      redirect('event/register/15', $data);
    } else {
      $this->M_EventPsdm->insertForm($data2);
      redirect('pendaftaranberhasil');
    }
  }

  public function validasiMrfest()
  {
    $data['user']           = $this->user->getUserData($_SESSION['userData']['ID']);
    $nama = $data['user']->NAMA;
    $nim = $data['user']->NIM;
    $id_line = $this->input->post('idLine');
    $tempatLahir = $this->input->post('tempatLahir');
    $tanggalLahir = $this->input->post('tanggalLahir');
    $noPhone = $this->input->post('noPhone');
    $alamatMalang = $this->input->post('alamatMalang');
    $motivasi = $this->input->post('motivasi');
    $dept1 = $this->input->post('dept1');
    $dept2 = $this->input->post('dept2');
    $alasan1 = $this->input->post('alasan1');
    $alasan2 = $this->input->post('alasan2');
    $jabatan1 = $this->input->post('jabatan1');
    $jabatan2 = $this->input->post('jabatan2');
    $jabatan3 = $this->input->post('jabatan3');
    $jabatan4 = $this->input->post('jabatan4');
    $jabatan5 = $this->input->post('jabatan5');
    $jabatan6 = $this->input->post('jabatan6');
    $tahun1 = $this->input->post('tahun1');
    $tahun2 = $this->input->post('tahun2');
    $tahun3 = $this->input->post('tahun3');
    $tahun4 = $this->input->post('tahun4');
    $tahun5 = $this->input->post('tahun5');
    $tahun6 = $this->input->post('tahun6');
    $Kegiatan1 = $this->input->post('kegiatan1');
    $Kegiatan2 = $this->input->post('kegiatan2');
    $Kegiatan3 = $this->input->post('kegiatan3');
    $Kegiatan4 = $this->input->post('kegiatan4');
    $Kegiatan5 = $this->input->post('kegiatan5');
    $Kegiatan6 = $this->input->post('kegiatan6');
    $linkDrive = $this->input->post('linkDrive');

    $data2 = array(
      'nama' => $nama,
      'nim' => $nim,
      'tempatLahir' => $tempatLahir,
      'tanggalLahir' => $tanggalLahir,
      'alamatMalang' => $alamatMalang,
      'noPhone' => $noPhone,
      'motivasi' => $motivasi,
      'idLine' => $id_line,
      'dept1' => $dept1,
      'dept2' => $dept2,
      'alasan1' => $alasan1,
      'alasan2' => $alasan2,
      'linkDrive' => $linkDrive,
      'jabatan1' => $jabatan1,
      'jabatan2' => $jabatan2,
      'jabatan3' => $jabatan3,
      'jabatan4' => $jabatan4,
      'jabatan5' => $jabatan5,
      'jabatan6' => $jabatan6,
      'tahun1' => $tahun1,
      'tahun2' => $tahun2,
      'tahun3' => $tahun3,
      'tahun4' => $tahun4,
      'tahun5' => $tahun5,
      'tahun6' => $tahun6,
      'kegiatan1' => $Kegiatan1,
      'kegiatan2' => $Kegiatan2,
      'kegiatan3' => $Kegiatan3,
      'kegiatan4' => $Kegiatan4,
      'kegiatan5' => $Kegiatan5,
      'kegiatan6' => $Kegiatan6,
      'diTerima' => false
    );

    if ($dept1 == $dept2) {
      $this->session->set_flashdata('messagedept', 'Opsi divisi tidak boleh sama');
      redirect('event/register/16', $data);
    } else {
      $this->M_EventPsdm2->insertForm($data2);
      redirect('registerasiberhasil');
    }
  }

  // Form Action Bimsak 7.0
  public function validasiBimsak()
  {
    $data['user']           = $this->user->getUserData($_SESSION['userData']['ID']);
    $nama = $data['user']->NAMA;
    $nim = $data['user']->NIM;
    $jenis_kelamin = $this->input->post('jenis_kelamin');
    $ttl = $this->input->post('ttl');
    $email = $this->input->post('email');
    $alamat = $this->input->post('alamat');
    $noPhone = $this->input->post('noPhone');
    $dept1 = $this->input->post('dept1');
    $dept2 = $this->input->post('dept2');
    $alasan1 = $this->input->post('alasan1');
    $alasan2 = $this->input->post('alasan2');
    $jabatan1 = $this->input->post('jabatan1');
    $jabatan2 = $this->input->post('jabatan2');
    $jabatan3 = $this->input->post('jabatan3');
    $jabatan4 = $this->input->post('jabatan4');
    $jabatan5 = $this->input->post('jabatan5');
    $jabatan6 = $this->input->post('jabatan6');
    $kegiatan1 = $this->input->post('kegiatan1');
    $kegiatan2 = $this->input->post('kegiatan2');
    $kegiatan3 = $this->input->post('kegiatan3');
    $kegiatan4 = $this->input->post('kegiatan4');
    $kegiatan5 = $this->input->post('kegiatan5');
    $kegiatan6 = $this->input->post('kegiatan6');
    $tahun1 = $this->input->post('tahun1');
    $tahun2 = $this->input->post('tahun2');
    $tahun3 = $this->input->post('tahun3');
    $tahun4 = $this->input->post('tahun4');
    $tahun5 = $this->input->post('tahun5');
    $tahun6 = $this->input->post('tahun6');
    $kelebihan = $this->input->post('kelebihan');
    $kekurangan = $this->input->post('kekurangan');
    $alasanDiterima = $this->input->post('alasanDiterima');
    $linkDrive = $this->input->post('linkDrive');
    $jadwal1 = $this->input->post('jadwal1');
    $jadwal2 = $this->input->post('jadwal2');
    $id_line = $this->input->post('idLine');

    $data2 = array(
      'nama' => $nama,
      'nim' => $nim,
      'jenis_kelamin' => $jenis_kelamin,
      'ttl' => $ttl,
      'email' => $email,
      'alamat' => $alamat,
      'noPhone' => $noPhone,
      'dept1' => $dept1,
      'dept2' => $dept2,
      'alasan1' => $alasan1,
      'alasan2' => $alasan2,
      'jabatan1' => $jabatan1,
      'jabatan2' => $jabatan2,
      'jabatan3' => $jabatan3,
      'jabatan4' => $jabatan4,
      'jabatan5' => $jabatan5,
      'jabatan6' => $jabatan6,
      'kegiatan1' => $kegiatan1,
      'kegiatan2' => $kegiatan2,
      'kegiatan3' => $kegiatan3,
      'kegiatan4' => $kegiatan4,
      'kegiatan5' => $kegiatan5,
      'kegiatan6' => $kegiatan6,
      'tahun1' => $tahun1,
      'tahun2' => $tahun2,
      'tahun3' => $tahun3,
      'tahun4' => $tahun4,
      'tahun5' => $tahun5,
      'tahun6' => $tahun6,
      'kelebihan' => $kelebihan,
      'kekurangan' => $kekurangan,
      'linkDrive' => $linkDrive,
      'alasanDiterima' => $alasanDiterima,
      'jadwal1' => $jadwal1,
      'jadwal2' => $jadwal2,
      'idLine' => $id_line,
      'diTerima' => false
    );

    if ($jadwal1 == $jadwal2 && $dept1 == $dept2) {
      $this->session->set_flashdata('messageschedule', 'Opsi jadwal screening tidak boleh sama');
      $this->session->set_flashdata('messagedept', 'Opsi divisi tidak boleh sama');
      redirect('event/register/17', $data);
    } else if ($jadwal1 == $jadwal2) {
      $this->session->set_flashdata('messageschedule', 'Opsi jadwal screening tidak boleh sama');
      redirect('event/register/17', $data);
    } else if ($dept1 == $dept2) {
      $this->session->set_flashdata('messagedept', 'Opsi divisi tidak boleh sama');
      redirect('event/register/17', $data);
    } else {
      $this->M_EventSosma->insertForm($data2);
      redirect('pendaftaranBimsakberhasil');
    }
  }

  public function statusDaftar()
  {

    $this->load->view('Event/status');
  }

  public function registerValidation()
  {
    $registrationData = [
      "id_event"   => $_POST['id_event'],
      "id_user"    => $_POST['id_user'],
    ];

    $this->event->regist($registrationData);
    $this->user->updateData($_POST['id_user'], $_POST['id_line'], $_POST['NAMA'], $_POST['NIM'], $_POST['HP']);

    redirect('/events');
  }


  // ------------------Event PSDM------------------------
  public function loginWOW()
  {
    $this->load->view('Event/Wow/login');
  }

  public function cek_akun()
  {
  }

  public function department()
  {
    $events = $this->event->getAllEvents();
    foreach ($events as $event) {
      $event->isRegistered  = $this->event->checkRegisterStatus($event->id_event);
      $event->isFull        = $this->event->getQuota($event->id_event)->jumlah >= $event->quota;
      $event->isDone        = $this->event->isFinishedEvent($event->id_event);
      // var_dump($event->isDone);
    }
    $nama_department = $this->input->get('filter');
    // var_dump($nama_department);
    if ($nama_department == "semua") {
      $department = $events;
      $data['events'] = $department;
    } else {
      $department = $this->event->getDepartmentByName($nama_department);
      $events = $this->event->getEventByDepartment($department['id_division']);
      foreach ($events as $event) {
        $event->isRegistered  = $this->event->checkRegisterStatus($event->id_event);
        $event->isFull        = $this->event->getQuota($event->id_event)->jumlah >= $event->quota;
        $event->isDone        = $this->event->isFinishedEvent($event->id_event);
        // var_dump($event->isDone);
      }
      $data['events'] = $events;
    }
    $data['title']  = "Event";
    $data['department'] = $this->event->getAllDepartment();


    // $data['events'] = $department;

    $this->template->render('Event/dashboard', $data);
  }
}
