<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EventOprec extends CI_Controller
{
	// var $api = "";

	function __construct()
	{
		parent::__construct();
		// $this->api="http://apiauth.kbmsi.or.id/index.php";
		if (!isset($_SESSION['userData']))
			redirect('/');

		$this->load->library('session');
		// $this->load->library('curl');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('M_EventOprec');
		$this->load->model('M_user', 'user');
	}

	public function isi_form()
	{
		$data['user'] = $this->user->getUserData($_SESSION['userData']['ID']);
		
		$data['hari'] = array(
			// Minggu ke-1
			// array("Minggu, 13 Februari 2022 16:00 - 16:45 WIB", "Minggu, 13 Februari 2022 19:15 - 20:00 WIB", "Minggu, 13 Februari 2022 20:15 - 21:00 WIB"),
			// array("Senin, 14 Februari 2022 16:00 - 16:45 WIB", "Senin, 14 Februari 2022 19:15 - 20:00 WIB", "Senin, 14 Februari 2022 20:15 - 21:00 WIB"),
			// array("Selasa, 15 Februari 2022 16:00 - 16:45 WIB", "Selasa, 15 Februari 2022 19:15 - 20:00 WIB", "Selasa, 15 Februari 2022 20:15 - 21:00 WIB"),
			// array("Rabu, 16 Februari 2022 16:00 - 16:45 WIB", "Rabu, 16 Februari 2022 19:15 - 20:00 WIB", "Rabu, 16 Februari 2022 20:15 - 21:00 WIB"),
			// array("Kamis, 17 Februari 2022 16:00 - 16:45 WIB", "Kamis, 17 Februari 2022 19:15 - 20:00 WIB", "Kamis, 17 Februari 2022 20:15 - 21:00 WIB"),
			// array("Jumat, 18 Februari 2022 16:00 - 16:45 WIB", "Jumat, 18 Februari 2022 19:15 - 20:00 WIB", "Jumat, 18 Februari 2022 20:15 - 21:00 WIB", "", "", "", ""),
			// array("Sabtu, 19 Februari 2022 10:00 - 10:45 WIB", "Sabtu, 19 Februari 2022 11:00 - 11:45 WIB", "Sabtu, 19 Februari 2022 13:00 - 13:45 WIB", "Sabtu, 19 Februari 2022 14:00 - 14:45 WIB", "Sabtu, 19 Februari 2022 16:00 - 16:45 WIB", "Sabtu, 19 Februari 2022 19:15 - 20:00 WIB", "Sabtu, 19 Februari 2022 20:15 - 21:00 WIB"),
			// Minggu ke-2
			// array("","","","","","","")
			// array("Minggu, 20 Februari 2022 10:00 - 10:45 WIB", "Minggu, 20 Februari 2022 11:00 - 11:45 WIB", "Minggu, 20 Februari 2022 13:00 - 13:45 WIB", "Minggu, 20 Februari 2022 14:00 - 14:45 WIB", "Minggu, 20 Februari 2022 16:00 - 16:45 WIB", "Minggu, 20 Februari 2022 19:15 - 20:00 WIB", "Minggu, 20 Februari 2022 20:15 - 21:00 WIB"),
			// array("Minggu, 20 Februari 2022 16:00 - 16:45 WIB", "Minggu, 20 Februari 2022 19:15 - 20:00 WIB", "Minggu, 20 Februari 2022 20:15 - 21:00 WIB"),
			// array("Senin, 21 Februari 2022 16:00 - 16:45 WIB", "Senin, 21 Februari 2022 19:15 - 20:00 WIB", "Senin, 21 Februari 2022 20:15 - 21:00 WIB"),
			// array("Selasa, 22 Februari 2022 16:00 - 16:45 WIB", "Selasa, 22 Februari 2022 19:15 - 20:00 WIB", "Selasa, 22 Februari 2022 20:15 - 21:00 WIB"),
			array("Rabu, 23 Februari 2022 16:00 - 16:45 WIB", "Rabu, 23 Februari 2022 19:15 - 20:00 WIB", "Rabu, 23 Februari 2022 20:15 - 21:00 WIB"),
			array("","","")
			);

		$cek = $this->M_EventOprec->cekDaftar($data['user']->NIM);
		$angkatan = substr($data['user']->NIM, 0, 5);
		if ($angkatan != "21515" && $angkatan != "20515" && $angkatan != "19515") {
        // if ($angkatan != "19515") {
			$this->session->set_flashdata('message', 'Maaf pendaftar hanya untuk angkatan 2019, 2020 dan 2021');
			redirect('event/statusDaftarOprec');
		}
		$data['cek'] = $cek;
		if ($cek != 0) {
			$pendaftar = $this->M_EventOprec->Pendaftar($data['user']->NIM)->row_array();
			if ($pendaftar['status'] == 1) {
				$this->session->set_flashdata('message', 'CONGRATULATION!');
				$this->session->set_flashdata('nama', $data['user']->NAMA);
				redirect('event/terima');
			} else {
				$this->session->set_flashdata('message', 'JANGAN PUTUS ASA YA!');
				$this->session->set_flashdata('nama', $data['user']->NAMA);
				redirect('event/tolak');
			}
		}

		// $cekStatus = $this->M_EventOprec->cekStatus($data['user']->NIM);
		// $data['cekStatus'] = $cekStatus;
		// $pendaftar = $this->M_EventOprec->Pendaftar($nim)->row_array();
		// if ($pendaftar['diterima']) {
		// 	$this->session->set_flashdata('message', 'Anda diterima');
		// 	redirect('event/statusDaftarOprec');
		// } else {
		// 	$this->session->set_flashdata('message', 'Anda tidak diterima');
		// 	redirect('event/statusDaftarOprec');
		// }

		// 	if($cek != 0){
		// 		$pendaftar = $this->M_EventOprec->Pendaftar($nim)->row_array();
		// 		$this->session->set_userdata('status','ada');
		// 		$this->session->set_flashdata('nama', $pendaftar['nama']);
		// 		if ($pendaftar['diterima']) {
		// 			$this->session->set_flashdata('message', 'Anda diterima');
		// 			redirect('EventOprec/pengumuman');
		// 		}
		// 		else {
		// 			$this->session->set_flashdata('message', 'Anda tidak diterima');
		// 			redirect('EventOprec/pengumuman2');
		// 		}
		// 	}
// 			else if ($cek != 0){
				// $this->session->set_flashdata('message', 'Anda sudah mendaftar sebelumnya');
			 //	redirect('event/statusDaftarOprec');
// 			}
		$this->load->view('Event/Oprec/tidakDaftar', $data);
// 		$this->load->view('Event/Oprec/form', $data);
	}

	public function terima()
	{
		// if (!isset($_SESSION['message'])) {
		// 		redirect('OprecstaffGen8/login');
		// 	}
		// $this->session->sess_destroy();
		$this->load->view('Event/Oprec/terima');
	}

	public function tolak()
	{
		// if (!isset($_SESSION['message'])) {
		// 		redirect('OprecstaffGen8/login');
		// 	}
		// $this->session->sess_destroy();
		$this->load->view('Event/Oprec/tolak');
	}




	public function submit_form()
	{
		// if (!isset($_SESSION['nama'])) {
		// 		redirect('event/login');
		// }

		// $waktu1 = $this->input->post('jadwal1')." ".$this->input->post('waktu1');
		// $waktu2 = $this->input->post('jadwal2')." ".$this->input->post('waktu2');
		//
		// if ($waktu1==$waktu2) {
		// 		$this->session->set_flashdata('messagescreening', 'opsi waktu screening tidak boleh sama');
		// 		redirect('event/form');
		// 	}
		// else{
		$data['user']           = $this->user->getUserData($_SESSION['userData']['ID']);
		$nama = $data['user']->NAMA;
		$nim = $data['user']->NIM;
		$id_line = $this->input->post('idLine');
		$email = $this->input->post('email');
		$dept1 = $this->input->post('dept1');
		$dept2 = $this->input->post('dept2');
		$komisi = $this->input->post('komisi');
		$jadwal1 = $this->input->post('jadwal1');
		$jadwal2 = $this->input->post('jadwal2');
		$link = $this->input->post('link');

		
		if ($dept1==$dept2 && $jadwal1==$jadwal2) {
			$this->session->set_flashdata('messagescreening', 'Opsi departemen screening tidak boleh sama');
			$this->session->set_flashdata('messageschedule', 'Opsi jadwal screening tidak boleh sama');
			redirect('event/formOprec');
		}

		if ($dept1==$dept2) {
			$this->session->set_flashdata('messagescreening', 'Opsi departemen screening tidak boleh sama');
			redirect('event/formOprec');
		}

		if ($jadwal1==$jadwal2) {
			$this->session->set_flashdata('messageschedule', 'Opsi jadwal screening tidak boleh sama');
			redirect('event/formOprec');
		}

		$data2 = array(
			'nama' => $nama,
			'nim' => $nim,
			'id_line' => $id_line,
			'email' => $email,
			'pildept1' => $dept1,
			'pildept2' => $dept2,
			'pilkomisi' => $komisi,
			'jadwal1' => $jadwal1,
			'jadwal2' => $jadwal2,
			'link_berkas' => $link
		);
		$this->M_EventOprec->insertForm($data2);
		// $this->session->set_flashdata('messageStatus', '<div class="alert alert-success" role="alert">Terimakasih sudah mendaftar<div>');
		redirect('event/statusOprec');
		// }
	}

	public function status()
	{
		// if (!isset($_SESSION['nama'])) {
		// 		redirect('event/login');
		// 	}
		// $this->session->sess_destroy();

		$this->load->view('Event/Oprec/status');
	}

	public function statusDaftar()
	{

		$this->load->view('Event/Oprec/statusDaftar');
	}
}
