<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminEventPsdm2 extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->model('M_EventPsdm2');
  }

  public function pageLogin()
  {
    if ($this->session->userdata('user') != "") {
      redirect('admin/dashboardMRFEST');
    } else {
      $this->load->view('Event/Mrfest/loginAdmin');
    }
  }

  public function login()
  {
    $akun = $this->input->post('akun');
    $pass = $this->input->post('password');
    if ($akun == "adminPsdm" && $pass == "suksesYaa") {
      $this->session->set_userdata('user', $akun);
      redirect('admin/dashboardMRFEST');
    } else {
      redirect('adminmrfest');
    }
  }

  public function dashboard()
  {
    if ($this->session->userdata('user') != "") {
      $data['pendaftar'] = $this->M_EventPsdm2->getAllPendaftar();
      $this->load->view('Event/Mrfest/dashboard', $data);
    } else {
      redirect('adminmrfest');
    }
  }
  public function logout()
  {
    $this->session->unset_userdata('user');
    redirect('adminmrfest');
  }

  public function ubah($id_event, $key)
  {
    $this->M_EventPsdm2->ubahStatus($id_event, $key);
    redirect('admin/dashboardMRFEST');
  }
}
