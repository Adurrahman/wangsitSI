<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EventPsdm extends CI_Controller
{
	var $api = "";

	function __construct()
	{
		parent::__construct();
		$this->api = "http://apiauth.kbmsi.or.id/index.php";
		$this->load->library('session');
		$this->load->library('curl');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('M_EventPsdm');
	}


	public function loginWOW()
	{
		if (isset($_SESSION['nama'])) {
			redirect('event/form');
		}
		$this->load->view('Event/Wow/login');
	}

	public function pengumuman()
	{
		if (!isset($_SESSION['nama'])) {
			redirect('event/login');
		}
		$nama = $this->session->userdata('nama');
		$this->session->sess_destroy();
		$this->session->set_flashdata('message', "$nama");
		$this->load->view('Event/Wow/pengumuman');
	}

	public function cek_akun()
	{
		$nim = $this->input->post('nim');
		$pass = $this->input->post('password');
		$person = [
			"nim" => $nim,
			"password" => $pass
		];
		$angkatan = substr($nim, 0, 5);
		$cek = $this->M_EventPsdm->cekDaftar($nim);
		$diterima = $this->M_EventPsdm->cekDiterima($nim);
		// echo $diterima->diterima;
		// var_dump ($diterima);
		// $cek = 0;
		$data1 = json_encode($person);
		$data2 = json_decode($this->curl->simple_post($this->api, $data1, array(CURLOPT_BUFFERSIZE => 10)));

		if ($data2 == null) {
			$this->session->set_flashdata('message', 'nim atau password salah');
			redirect('event/login');
		} elseif ($angkatan != "19515" && $angkatan != "20515") {
			$this->session->set_flashdata('message', 'Pendaftar bukan angkatan 2019 atau 2020');
			redirect('event/login');
		} elseif ($cek == 0) {
			$this->session->set_flashdata('message', 'Anda tidak terdaftar');
			redirect('event/login');
		} elseif ($diterima->diterima == 0) {
			$this->session->set_flashdata('message', 'Maaf Anda tidak diterima');
			redirect('event/login');
		} else {
			$session_data = array(
				'nama'  => $data2->data->nama,
			);
			$this->session->set_userdata($session_data);
			redirect('event/pengumuman');
		}
	}
}
