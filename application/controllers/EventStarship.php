<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EventStarship extends CI_Controller
{
	// var $api = "";

	function __construct()
	{
		parent::__construct();
		// $this->api="http://apiauth.kbmsi.or.id/index.php";
		if (!isset($_SESSION['userData']))
			redirect('/');

		$this->load->library('session');
		// $this->load->library('curl');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('M_EventStarship');
		$this->load->model('M_user', 'user');
	}

	public function isi_form()
	{
		$data['user'] = $this->user->getUserData($_SESSION['userData']['ID']);

		$data['hari'] = array(
			// array("Sabtu, 2 Oktober 2021 19:00 - 19:45 WIB", "Sabtu, 2 Oktober 2021 19:45 - 20:30 WIB", "Sabtu, 2 Oktober 2021 20:30 - 21:15 WIB"),
			// array("Minggu, 3 Oktober 2021 11:00 - 11:45 WIB", "Minggu, 3 Oktober 2021 13:00 - 13:45 WIB", "Minggu, 3 Oktober 2021 13:45 - 14:30 WIB", "Minggu, 3 Oktober 2021 14:30 - 15:15 WIB", "Minggu, 3 Oktober 2021 16:00 - 16:45 WIB", "Minggu, 3 Oktober 2021 16:45 - 17:30 WIB", "Minggu, 3 Oktober 2021 19:00 - 19:45 WIB", "Minggu, 3 Oktober 2021 19:45 - 20:30 WIB"),
			// array("Senin, 4 Oktober 2021 16:00 - 16:45 WIB", "Senin, 4 Oktober 2021 16:45 - 17:30 WIB", "Senin, 4 Oktober 2021 19:00 - 19:45 WIB", "Senin, 4 Oktober 2021 19:45 - 20:30 WIB", "Senin, 4 Oktober 2021 20:30 - 21:15 WIB"),
			// array("Selasa, 5 Oktober 2021 16:00 - 16:45 WIB", "Selasa, 5 Oktober 2021 16:45 - 17:30 WIB", "Selasa, 5 Oktober 2021 19:00 - 19:45 WIB", "Selasa, 5 Oktober 2021 19:45 - 20:30 WIB", "Selasa, 5 Oktober 2021 20:30 - 21:15 WIB"),
			// array("Rabu, 6 Oktober 2021 16:00 - 16:45 WIB", "Rabu, 6 Oktober 2021 16:45 - 17:30 WIB", "Rabu, 6 Oktober 2021 19:45 - 20:30 WIB", "Rabu, 6 Oktober 2021 20:30 - 21:15 WIB"),
			// array("Kamis, 7 Oktober 2021 16:00 - 16:45 WIB", "Kamis, 7 Oktober 2021 16:45 - 17:30 WIB", "Kamis, 7 Oktober 2021 19:45 - 20:30 WIB", "Kamis, 7 Oktober 2021 20:30 - 21:15 WIB"),
			array("Jumat, 8 Oktober 2021 14:00 - 14:45 WIB", "Jumat, 8 Oktober 2021 14:45 - 15:30 WIB", "Jumat, 8 Oktober 2021 16:00 - 16:45 WIB", "Jumat, 8 Oktober 2021 16:45 - 17:30 WIB"),
			array("Jumat, 8 Oktober 2021 19:00 - 19:45 WIB", "Jumat, 8 Oktober 2021 19:45 - 20:30 WIB", "Jumat, 8 Oktober 2021 20:30 - 21:15 WIB", "Jumat, 8 Oktober 2021 21:15 - 22:00 WIB")
			);

		$cek = $this->M_EventStarship->cekDaftar($data['user']->NIM);
		$angkatan = substr($data['user']->NIM, 0, 5);
		if ($angkatan != "21515") {
			$this->session->set_flashdata('message', 'Maaf Pendaftar Hanya Boleh Angkatan 2021');
			redirect('event/statusDaftarStarship');
		}
		$data['cek'] = $cek;
		if ($cek != 0) {
			$pendaftar = $this->M_EventStarship->Pendaftar($data['user']->NIM)->row_array();
			if ($pendaftar['diterima'] == 1) {
				$this->session->set_flashdata('message', 'CONGRATULATION!');
				$this->session->set_flashdata('nama', $data['user']->NAMA);
				redirect('event/terimaStarship');
			} else {
				$this->session->set_flashdata('message', 'JANGAN PUTUS ASA YA!');
				$this->session->set_flashdata('nama', $data['user']->NAMA);
				redirect('event/tolakStarship');
			}
		}

		// $cekStatus = $this->M_EventStarship->cekStatus($data['user']->NIM);
		// $data['cekStatus'] = $cekStatus;
		// $pendaftar = $this->M_EventStarship->Pendaftar($nim)->row_array();
		// if ($pendaftar['diterima']) {
		// 	$this->session->set_flashdata('message', 'Anda diterima');
		// 	redirect('event/statusDaftarStarship');
		// } else {
		// 	$this->session->set_flashdata('message', 'Anda tidak diterima');
		// 	redirect('event/statusDaftarStarship');
		// }

			if($cek != 0){
				$pendaftar = $this->M_EventStarship->Pendaftar($nim)->row_array();
				$this->session->set_userdata('status','ada');
				$this->session->set_flashdata('nama', $pendaftar['nama']);
				// if ($pendaftar['diterima']) {
				// 	$this->session->set_flashdata('message', 'Anda diterima');
				// 	redirect('EventStarship/pengumuman');
				// }
				// else {
				// 	$this->session->set_flashdata('message', 'Anda tidak diterima');
				// 	redirect('EventStarship/pengumuman2');
				// }
// 			}
// 			else{
				$this->session->set_flashdata('message', 'Anda sudah mendaftar Starship 2021 sebelumnya');
			 	redirect('event/statusDaftarStarship');
			}
		$this->load->view('Event/Starship/tidakDaftar', $data);
		// $this->load->view('Event/Starship/statusClose', $data);
		// $this->load->view('Event/Starship/form', $data);
	}

	public function terima()
	{
		// if (!isset($_SESSION['message'])) {
		// 		redirect('OprecstaffGen8/login');
		// 	}
		// $this->session->sess_destroy();
		$this->load->view('Event/Starship/terima');
	}

	public function tolak()
	{
		// if (!isset($_SESSION['message'])) {
		// 		redirect('OprecstaffGen8/login');
		// 	}
		// $this->session->sess_destroy();
		$this->load->view('Event/Starship/tolak');
	}




	public function submit_form()
	{
		// if (!isset($_SESSION['nama'])) {
		// 		redirect('event/login');
		// }

		// $waktu1 = $this->input->post('tanggal1')." ".$this->input->post('waktu1');
		// $waktu2 = $this->input->post('tanggal2')." ".$this->input->post('waktu2');
		//
		// if ($waktu1==$waktu2) {
		// 		$this->session->set_flashdata('messagescreening', 'opsi waktu screening tidak boleh sama');
		// 		redirect('event/form');
		// 	}
		// else{
		$data['user']           = $this->user->getUserData($_SESSION['userData']['ID']);
		$nama = $data['user']->NAMA;
		$nim = $data['user']->NIM;
		$id_line = $this->input->post('idLine');
		$email = $this->input->post('email');
		$dept1 = $this->input->post('dept1');
		$dept2 = $this->input->post('dept2');
		$komisi = $this->input->post('komisi');
		$tanggal1 = $this->input->post('tanggal1');
		// $waktu1 = $this->input->post('waktu1');
		$tanggal2 = $this->input->post('tanggal2');
		// $waktu2 = $this->input->post('waktu2');
		$twibbon = $this->input->post('twibbon');
		$link = $this->input->post('link');

		
		if ($dept1==$dept2 && $tanggal1==$tanggal2) {
			$this->session->set_flashdata('messagescreening', 'Opsi departemen screening tidak boleh sama');
			$this->session->set_flashdata('messageschedule', 'Opsi jadwal screening tidak boleh sama');
			redirect('event/formStarship');
		}

		if ($dept1==$dept2) {
			$this->session->set_flashdata('messagescreening', 'Opsi departemen screening tidak boleh sama');
			redirect('event/formStarship');
		}

		if ($tanggal1==$tanggal2) {
			$this->session->set_flashdata('messageschedule', 'Opsi jadwal screening tidak boleh sama');
			redirect('event/formStarship');
		}

		$data2 = array(
			'nama' => $nama,
			'nim' => $nim,
			'id_line' => $id_line,
			'email' => $email,
			'pildept1' => $dept1,
			'pildept2' => $dept2,
			'pilkomisi' => $komisi,
			'jadwal1_tgl' => $tanggal1,
			// 'jadwal1_jam' => $waktu1,
			'jadwal2_tgl' => $tanggal2,
			// 'jadwal2_jam' => $waktu2,
			'link_twibbon' => $twibbon,
			'link_berkas' => $link
		);
		$this->M_EventStarship->insertForm($data2);
		// $this->session->set_flashdata('messageStatus', '<div class="alert alert-success" role="alert">Terimakasih sudah mendaftar<div>');
		redirect('event/statusStarship');
		// }
	}

	public function status()
	{
		// if (!isset($_SESSION['nama'])) {
		// 		redirect('event/login');
		// 	}
		// $this->session->sess_destroy();

		$this->load->view('Event/Starship/status');
	}

	public function statusDaftar()
	{

		$this->load->view('Event/Starship/statusDaftar');
	}
}
