<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminEventPsdm extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->model('M_EventPsdm');
  }

  public function pageLogin()
  {
    if ($this->session->userdata('adminwow') != "") {
      redirect('admin/dashboardwow');
    } else {
      $this->load->view('Event/Wow/loginAdmin');
    }
  }

  public function login()
  {
    $akun = $this->input->post('akun');
    $pass = $this->input->post('password');
    if ($akun == "adminWowSI" && $pass == "SIJaya") {
      $this->session->set_userdata('adminwow', $akun);
      redirect('admin/dashboardwow');
    } else {
      redirect('adminwow');
    }
  }

  public function dashboard()
  {
    if ($this->session->userdata('adminwow') != "") {
      $data['pendaftar'] = $this->M_EventPsdm->getAllPendaftar();
      $this->load->view('Event/Wow/dashboard', $data);
    } else {
      redirect('adminwow');
    }
  }
  public function logout()
  {
    $this->session->unset_userdata('adminwow');
    redirect('adminwow');
  }

  public function ubah($id_event, $key)
  {
    $this->M_EventPsdm->ubahStatus($id_event, $key);
    redirect('admin/dashboardwow');
  }
}
