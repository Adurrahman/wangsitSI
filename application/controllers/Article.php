<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Article');
		$this->load->model('M_Kbmsi');
		$this->load->library('pagination');
	}

	public function index(){
		$config['use_page_numbers'] = TRUE; 

		// style for pagination button
		$config['full_tag_open']		= '<ul class="pagination center-align">';
		$config['full_tag_close']		= '</ul>';
		$config['next_link']				= '<i class="material-icons">chevron_right</i>';
		$config['next_tag_open']		= '<li class="waves-effect">';
		$config['next_tag_close']		= '</li>';
		$config['prev_link']				= '<i class="material-icons">chevron_left</i>';
		$config['prev_tag_open']		= '<li class="waves-effect">';
		$config['prev_tag_close']		= '</li>';
		$config['cur_tag_open']			= '<li class="active wangsit-color"><a>';
		$config['cur_tag_close']		= '</a></li>';
		$config['num_tag_open']			= '<li class="waves-effect">';
		$config['num_tag_close']		= '</li>';
		$config['base_url']					= base_url('/article');
		$config['total_rows']				= $this->M_Article->getTotalArticle();;
		$config['per_page']					= 8;

		$index	= $this->uri->segment(2) == NULL ? 0 : ($this->uri->segment(2) * $config['per_page']) - $config['per_page'];
	
		$this->pagination->initialize($config);
		$data['pagination']			= $this->pagination->create_links();
		$data['activeTab'] 			= 0;
		$data['allArticle']			= $this->M_Article->GetArticleAll($config['per_page'],$index);
		$data['isActiveUser']		= $this->M_Kbmsi->getStatus($_SESSION['userData']['USERNAME']) == 0 ? 'block' : 'none';
		$data['isButtonActive'] = $this->M_Kbmsi->getStatus($_SESSION['userData']['USERNAME']) == 0 ? 'disabled' : NULL;
		$data['title']					= "Article | Wangsit";
		$data['library']				= "none";

		$this->template->render('Article/V_Index', $data);
	}

	public function Detail(){
		$username = $this->uri->segment(1);
		$slug = $this->uri->segment(2);
		$data['slug'] = $slug;
		$data['detail'] = $this->M_Article->Read_Article_Single($slug,$username);
		$data['recent'] = $this->M_Article->GetRecent();
		$data['detailcomment'] = $this->M_Article->GetCommentByArticle($slug)!=null ? $this->M_Article->GetCommentByArticle($slug) : null ;
		
		if (!isset($_SESSION['userData'])) {
			$_SESSION['userData'] = null;
		}
		
		$data['btnVisibility'] 		= $_SESSION['userData']['USERNAME'] == $username ? "block" : 'none' ;
		$data['isCommentActive'] 	= isset($_SESSION['userData']);
		$data['library']					= 'none';
		$data['title'] 						= $data['detail']->JUDUL;
		
		if ($data['detail'] == null) {
			redirect('','refresh');
		}

		$this->template->render('Article/V_Detail_Article', $data);
	}

	public function create(){
		if(isset($_POST['submit'])){
			$article = new stdClass();
			$article->slug 				= $this->generateSlug($_POST['judul']);
			$article->judul  			= $_POST['judul'];
			$article->katagori  	= $_POST['katagori'];
			$article->opsi  			= $_POST['opsi'];
			$article->artikel  		= $_POST['artikel'];
			$article->desc  			= $_POST['desc'];
			$article->username 		= $_SESSION['userData']['USERNAME'];
			
			$this->M_Article->Insert_Article($article);

			redirect($article->username.'/'.$article->slug ,'refresh');
		}
		else{
			$data['title'] = 'Create Article | wangsit';
			$data['library'] = "summerNote";
			$this->template->render('Article/V_Insert', $data);	
		}
	}

	public function kategori(){
		$kategori = $this->uri->segment(2);
		switch ($kategori) {
			case 'General':
				$tabName = 'General';
				$data['activeTab'] = 1;
				break;
			case 'Akademik':
				$tabName = 'Akademik';
				$data['activeTab'] = 2;
				break;
			case 'Review':
				$tabName = 'Review';
				$data['activeTab'] = 3;
				break;
			
			default:
			redirect('article','refresh');
			$tabName= 'wakwaw';
			break;
		}
		$this->config->load('Custom_Halaman',TRUE);
		$config = $this->config->item('Custom_Halaman');
		$config['use_page_numbers'] 	= TRUE; 
		$config['base_url'] 					= base_url().'article'.'/'.$tabName;
		$config['total_rows'] 				= $this->db->get_where('article_article', array('KATEGORI' => $tabName))->num_rows();
		$config['per_page'] 					= 8;
		if($this->uri->segment(3) == null){
			$index =0;
		}else{
			$index = $this->uri->segment(3)*$config['per_page']-$config['per_page'];
		}
		$this->pagination->initialize($config);


		$data['pagination']=$this->pagination->create_links();
		$data['allArticle']= $this->M_Article->GetArticleKategori($config['per_page'],$index,$a);
		$data['isActiveUser']		= $this->M_Kbmsi->getStatus($_SESSION['userData']['USERNAME']) == 0 ? 'block' : NULL;
		$data['isButtonActive'] = $this->M_Kbmsi->getStatus($_SESSION['userData']['USERNAME']) == 0 ? 'disabled' : NULL;

		$this->load->view('Article/V_Index',$data);
	}

	public function generateSlug($title){
		$slug = preg_replace('~[^\\pL\d]+~u', '-', $title);
		$slug = trim($slug, '-');
		$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
		$slug = strtolower($slug);
		$slug = preg_replace('~[^-\w]+~', '', $slug);
		$slug = $slug."-".substr(md5(microtime()),rand(0,26),5);


		return $slug;
	}
}