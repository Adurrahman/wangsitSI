<?php
class Authentication extends CI_Controller{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Login'); 
	}

	public function index(){	
		if($this->isAuthenticated())
			redirect('dashboard');

		if ($this->isFormHasValue())
			$this->authenticating();
		else {
			$data['loginURL'] = $this->googleplus->loginURL();
			$this->load->view('v_Login',$data);
		}

	}

	function Daftar(){
		if(isset($_SESSION['DataGoogle'])){

			if (isset($_POST['nim'])) {
				$formData = array(
					'PROVIDER' 			=> "GGL",
					'UID_PROVIDER' 	=> $_POST['uid'],
					'NAMA' 					=> $_POST['nama'],
					'EMAIL'					=> $_POST['email'],
					'GAMBAR' 				=> $_POST['gambar'],
					'NIM' 					=> $_POST['nim'],
					'USERNAME' 			=> strtolower($_POST['username']),
					'PASSWORD' 			=> md5($_POST['password']), 
					'TTL' 					=> $_POST['lahir'], 
					'HP' 						=> $_POST['phone'],
					'STATUS' 				=> 0,
					'CREATED_DATE' 	=> date("Y-m-d")  
				);

				if ($this->M_Login->checkUsernameAvailability($formData)){
					$data['errorMessage'] = "Username atau nim sudah terdaftar";
				} else {
					$this->initSession($_POST['uid']);
					redirect('dashboard');
				}
			
			}
			$data['userData'] = $this->session->userdata('DataGoogle');
			$this->load->view('v_register',$data);
		}
		else redirect('');
	}
	
	function logout(){
		$this->session->sess_destroy();
		$this->googleplus->revokeToken();
		redirect('');
	}

	function isAuthenticated(){
		return $this->session->userdata('login');
	}

	function authenticating(){
		if($this->isAdmin())
			redirect('wkwkwkwk','refresh');
		else if(isset($_GET['code'])){
			$this->googleAuth();
		} else {
			$nim 				= $_POST['NIM'];
			$password		= md5($_POST['PASSWD']);
	
			if ($this->checkLogin($nim, $password)) {
				$this->initSession($nim);
				redirect('dashboard','refresh');
			} else {
				$data['errorMessage'] = "Username atau password salah";
				$data['loginURL'] = $this->googleplus->loginURL();
				$this->load->view('v_Login',$data);	
			}
		}
	}

	function googleAuth(){
		$this->googleplus->getAuthenticate(); 
		$this->session->set_userdata('DataGoogle',$this->googleplus->getUserInfo());

		$idGoogle = $this->googleplus->getUserInfo()['id'];
		
		if ($this->M_Login->isRegistered($idGoogle)){
			$userData = $this->M_Login->getUserData($idGoogle);
			$this->session->set_userdata('login',true);
			$this->session->set_userdata('userData',$userData);
			redirect('dashboard');
		}
		else {
			redirect('register');
		}
	}

	function isAdmin(){
		$nim 	= isset($_POST['NIM']) 		? $_POST['NIM'] 		: '';
		$pass	= isset($_POST['PASSWD']) ? $_POST['PASSWD'] 	: '';

		if($nim =="6gendolphin" && $pass=="awuawuawu") {
			$this->session->set_userdata("admin",true);
			return true;
		}
		return false;
	}

	function isFormHasValue(){
		return isset($_POST['NIM']) && isset($_POST['PASSWD']) || isset($_GET['code']);
	}

	function checkLogin($nim, $password){
		$userData = $this->M_Login->getUsernameAndPassword($nim);
		if($userData != NULL)
			return $userData->PASSWORD == $password;
		else
			return false;
	}

	function isRegistered($id){
		return $this->M_Login->isRegistered($id)->num_row() == 1;
	}

	function initSession($id){
		$userData = $this->M_Login->getUserData($id);

		$this->session->set_userdata('login',true);
		$this->session->set_userdata('userData',$userData);
	}
}
