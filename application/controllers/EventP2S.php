<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EventP2S extends CI_Controller
{
	// var $api = "";

	function __construct()
	{
		parent::__construct();
		if (!isset($_SESSION['userData']))
			redirect('/');

		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('M_EventP2S');
		$this->load->model('M_user', 'user');
	}

	public function isi_form()
	{
		$data['user']  = $this->user->getUserData($_SESSION['userData']['ID']);
		$cek = $this->M_EventP2S->cekDaftar($data['user']->NIM);
		if ($cek != 0) {
			$this->session->set_flashdata('message', 'Anda sudah mendaftar');
			redirect('event/statusSkrim');
		}
		$data['nama'] = $this->session->userdata('nama');
		$data['nim'] = $this->session->userdata('nim');
		$this->load->view('Event/Skrim/form', $data);
	}

	public function submit_form()
	{
		$data['user'] = $this->user->getUserData($_SESSION['userData']['ID']);
		$nama = $data['user']->NAMA;
		$nim = $data['user']->NIM;
		$akt = $this->input->post('akt');
		$id_line = $this->input->post('idLine');
		$tanya = $this->input->post('tanya');

		$data = array(
			'nama_lengkap' => $nama,
			'nim' => $nim,
			'angkatan' => $akt,
			'id_line' => $id_line,
			'pertanyaan' => $tanya
		);
		$this->M_EventP2S->insertForm($data);
		redirect('events');
	}
	public function status(){	
		$this->load->view('Event/Skrim/status');
	 }
}
