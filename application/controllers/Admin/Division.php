<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Division extends CI_Controller{

  public function __construct() {
    parent::__construct();

    $this->load->model('M_Division', 'division');
  }

  public function index(){
    $division     = $this->session->userdata('admin')->division;

    $data['title']    = 'Daftar Anggota';
    $data['subtitle'] = "Anggota $division";
    $data['members'] = $this->division->getDivisionMembers($division);
    $data['divisions'] = $this->division->getDivisions();
    $data['roles']     = $this->division->getRoles();
    $data['candidates'] = $this->division->getCandidates();

    $this->template->renderAdminView('admin/division_members', $data);
  }

  public function showDivisions(){
    redirect('coming-soon');
  }

  public function addMember(){
    if($this->division->addMember($_POST))
      redirect(base_url('admin/members'));
  }
}