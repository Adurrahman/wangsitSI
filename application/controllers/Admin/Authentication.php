<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller{
  public function __construct() {
    parent::__construct();

    $this->load->model('M_Login', 'auth');
  }

  public function index(){
    $data['title'] = 'KBMSI Auth';

    if(isset($_POST['submit'])){
      if($this->isHasAuth()) {
        $this->authenticateRole();
      } else {
        $this->session->set_userdata('message_status', 0);
        $this->session->set_userdata('message_content', "Maaf, Username atau password salah");
      }
    }

    $this->template->renderAdminView('admin/login', $data);
  }

  public function isHasAuth(){
    $userData = $this->auth->getUsernameAndPassword($_POST['username']);
    if(isset($userData))
      return md5($_POST['password']) == $userData->PASSWORD;
    
    return false;
  }

  public function authenticateRole(){
    $adminData = $this->auth->getRoleData($_POST['username']);

    if(isset($adminData)){
      $adminData->generation = getGeneration($adminData->nim);
      $this->session->set_userdata('admin', $adminData);
      redirect('admin/dashboard');
    } else {
      $this->session->set_userdata('message_status', 0);
      $this->session->set_userdata('message_content', "Maaf, anda bukan admin");
    }
  }
}
