<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{
  public function __construct() {
    parent::__construct();

    $this->load->model('M_Login', 'auth');
    $this->load->model('M_user', 'user');
  }

  public function index(){
    $data['title'] = 'KBMSI Auth';

    if(isset($_SESSION['admin']))
      redirect('/admin/dashboard');

    if(isset($_POST['submit'])){
      if($this->isHasAuth()) {
        $this->authenticateRole();
      } else {
        $this->session->set_userdata('message_status', 0);
        $this->session->set_userdata('message_content', "Maaf, Username atau password salah");
      }
    }

    $this->template->renderAdminView('admin/login', $data);
  }

  public function activation(){
    $data['title']    = "Aktivasi KBMSI";
    $data['subtitle'] = "Daftar User KBMSI Belum Aktif";

    $data['users']    = $this->user->getPendingUser();
    $data['action']   = 'activate';
    $this->template->renderAdminView('admin/list_users', $data);
  }

  public function listUser(){
    $data['title']    = "Daftar KBMSI";
    $data['subtitle'] = "Daftar Data KBMSI";

    $data['users']    = $this->user->getUsersData();
    $data['action']   = 'detail';
    $this->template->renderAdminView('admin/list_users', $data);
  }

  public function editUser(){
    $data['title']    = "Daftar KBMSI";
    $data['subtitle'] = "Daftar Data KBMSI";

    $data['users']    = $this->user->getUsersData();
    $data['action']   = 'edit';
    $this->template->renderAdminView('admin/list_users', $data);
  }

  public function listBannedUsers(){
    $data['title']    = "Daftar KBMSI";
    $data['subtitle'] = "Daftar Data KBMSI";

    $data['users']    = $this->user->getBannedUser();
    $data['action']   = 'banned';
    $this->template->renderAdminView('admin/list_users', $data);
  }

  public function activate(){
    $id_user = $_POST['id'];

    $status = $this->user->activateUser($id_user);
    echo json_encode(["status" => $status]);
  }

  public function restoreUser(){
    $id_user = $_POST['id'];

    $status = $this->user->restoreUser($id_user);
    echo json_encode(["status" => $status]);
  }

  public function banUser(){
    $id_user = $_POST['id'];

    $status = $this->user->bannedUser($id_user);
    echo json_encode(["status" => $status]);
  }

  private function isHasAuth(){
    $userData = $this->auth->getUsernameAndPassword($_POST['username']);
    if(isset($userData))
      return md5($_POST['password']) == $userData->PASSWORD;
    
    return false;
  }

  private function authenticateRole(){
    $adminData = $this->auth->getRoleData($_POST['username']);

    if(isset($adminData)){
      $adminData->generation = getGeneration($adminData->nim);
      $this->session->set_userdata('admin', $adminData);
      redirect('admin/dashboard');
    } else {
      $this->session->set_userdata('message_status', 0);
      $this->session->set_userdata('message_content', "Maaf, anda bukan admin");
    }
  }
}
