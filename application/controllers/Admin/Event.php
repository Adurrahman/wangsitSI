<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
    isLoggedIn();
    $this->load->model('M_Event', "event");
    $this->load->model('M_Division', "division");
  }
  
  public function index(){
    $division     = $this->session->userdata('admin')->division;

    $data['title']      = "Daftar Event";
    $data['subtitle']   = "Daftar Event by $division";
    $data['members']    = $this->division->getDivisionMembers($division);
    $data['events']     = $this->event->getEvents();
    $this->template->renderAdminView('admin/events', $data);
  }

  public function addEvent(){
    $id_division = $this->division->getIdByName($_SESSION['admin']->division);

    $data = [
      "name"                  => $_POST['name'],
      "department"            => $id_division,
      "responsible_person"    => $_SESSION['admin']->id_user
    ];

    $this->event->addEvent($data);
    redirect('admin/events');
  }

  public function CheckAuthorization(){
    $status = $this->event->checkEventManager($_POST['id_event']);

    echo json_encode(['status' => $status]);
  }

  public function eventDetail(){
    $id_event   = $this->uri->segment(3);
    $event      = $this->event->getEventById($id_event);

    $data['title']          = "Event Detail";
    $data['subtitle']       = "Event $event->name";
    $data['participants']   = $this->event->getEventParticipants($id_event);
    $data['event']          = $event; 

    $this->template->renderAdminView('admin/event_detail', $data);
  }

  public function updateEvent(){
    $data = array();

    foreach($_POST as $id => $value ){
      $data[$id] = $value;
    }

    $this->event->updateEvent($data);
    redirect('/admin/eventDetail/' . $data['id_event']);
  }

  public function changeImagePoster(){
    $id_event = $this->uri->segment(4);
    $uploadConfig = [
      'upload_path'   => "./asset/img/event/",
      'allowed_types' => 'jpg|png|jpeg',
      'encrypt_name'  => TRUE
    ];

    $this->upload->initialize($uploadConfig);
    if($this->upload->do_upload('event_poster')){
      $data = [
        "id_event" => $id_event,
        "img_path" => "asset/img/event/" . $this->upload->data('file_name')
      ];
      $response = [
        "status"    => $this->event->updateImgPath($data),
        "img_path"  => $data['img_path']
      ];
      echo json_encode($response);
    } 
  }

  public function removeEventPoster(){
    $id_event = $this->uri->segment(4);
    $data = [
      "id_event" => $id_event,
      "img_path" => NULL
    ];
    $response = $this->event->updateImgPath($data);
    echo json_encode(["status" => $response]);
  }

  public function choiceForm(){
    $data['title']    = "Form";
    $data['subtitle'] = "Data yang dibutuhkan dalam registrasi";
    $this->template->renderAdminView('admin/form_registration_list', $data);
  }

}