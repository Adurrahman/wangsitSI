<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Academy extends CI_Controller{
  public function __construct() {
    parent::__construct();
    $this->load->model("M_Academy", "academy");
  }

  public function index(){
    $data['title']      = "Wangsit Academy";
    $data['subtitle']   = "Wangsit Academy Management";
    $data['lessons']    = $this->academy->getLessons();
    $data['categories'] = $this->academy->getCategories();

    $this->template->renderAdminView("admin/wangsitAcademy", $data);
  }
  
  public function category(){
    $data['title']      = "Wangsit Academy";
    $data['subtitle']   = "Wangsit Academy Management";
    $data['categories'] = $this->academy->getCategories();
    
    $this->template->renderAdminView("admin/academyCateogries", $data);
  }

  public function addAcademy(){
    $data = [
      "course"          => $_POST['name'],
      "abbreviation	"   => $_POST['alias'],
      "link"            => $_POST['link'],
      "category"        => $_POST['category'],
      "visited"         => 0
    ];

    $this->academy->insertNewCourse($data);
    redirect('/admin/wangsitAcademy');
  }

  public function addCategory(){
    $data = ["category" => $_POST['name']];

    $this->academy->insertNewCategory($data);
    redirect('admin/academyCategory');
  }

  public function updateCourse(){
    $data = [
      "id"          => $_POST['id'],
      "course"      => $_POST['name'],
      "updated_at"  => date(DATE_ATOM, time()),
      "updated_by"  => $_SESSION['admin']->id_user
    ];

    $this->academy->updateCourse($data);
    redirect('admin/wangsitAcademy');
  }

  public function disableCourse(){
    $data = [
      "id"          => $_POST['id'],
      "status"      => 0,
      "updated_at"  => date(DATE_ATOM, time()),
      "updated_by"  => $_SESSION['admin']->id_user
    ];

    $status = $this->academy->disableCourse($data);
    echo json_encode(["status" => $status]);
  }

  public function updateCategory(){
    $data = [
      "id"          => $_POST['id'],
      "category"    => $_POST['name'],
      "updated_at"  => date(DATE_ATOM, time()),
      "updated_by"  => $_SESSION['admin']->id_user
    ];

    $this->academy->updateCategory($data);
    redirect('admin/academyCategory');
  }

  public function disableCategory(){
    $data = [
      "id"          => $_POST['id'],
      "status"      => 0,
      "updated_at"  => date(DATE_ATOM, time()),
      "updated_by"  => $_SESSION['admin']->id_user
    ];

    $status = $this->academy->disableCategory($data);
    echo json_encode(["status" => $status]);
  }
}