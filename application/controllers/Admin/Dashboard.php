<?php 

class Dashboard extends CI_Controller{
  
  public function index(){
    $data['title'] = "dashboard";

    $this->template->renderAdminView('admin/dashboard', $data);
  }
}