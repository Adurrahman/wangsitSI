<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EventPsdm2 extends CI_Controller
{
	var $api = "";

	function __construct()
	{
		parent::__construct();
		// $this->api="http://apiauth.kbmsi.or.id/index.php";
		if (!isset($_SESSION['userData']))
			redirect('/');
		$this->load->library('session');
		// $this->load->library('curl');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('M_EventPsdm2');
		$this->load->model('M_EventPsdm');
		$this->load->model('M_user', 'user');
		$this->load->model('M_Event', 'event');
	}

	public function isi_form()
	{
		$data['user'] = $this->user->getUserData($_SESSION['userData']['ID']);

		$cek = $this->M_EventPsdm2->cekDaftar($data['user']->NIM);
		$angkatan = substr($data['user']->NIM, 0, 5);
		if ($angkatan != "21515" && $angkatan != "20515") {
			$this->session->set_flashdata('message', 'Maaf Pendaftar bukan angkatan 2020 atau 2021');
			redirect('event/statusDaftarMrfest');
		} else if ($cek != 0) {
			$this->session->set_flashdata('message', 'Anda sudah mendaftar sebelumnya');
			redirect('event/statusDaftarMrfest');
		}
		$this->load->view('Event/Mrfest/form', $data);
	}

	public function terima()
	{
		$this->load->view('Event/Mrfest/pengumuman');
	}

	public function tolak()
	{
		$this->load->view('Event/Mrfest/pengumuman');
	}

	public function submit_formWow()
	{
		$data['user']           = $this->user->getUserData($_SESSION['userData']['ID']);
		$eventWithId = $this->event->getEventById('17');
		if ($eventWithId->start_date > date("Y-m-d")) {
			$this->template->render('Event/Wow/afterRegister');
		} else {
			$data["cekPengumuman"] = $this->M_EventPsdm->cekDiterima($data['user']->NIM);
			$this->load->view('Event/Wow/AnnouncementLolos', $data);
		}
	}

	public function submit_formMrfest()
	{
		$data['user']           = $this->user->getUserData($_SESSION['userData']['ID']);
		$eventWithId = $this->event->getEventById('16');
		if ($eventWithId->start_date > date("Y-m-d")) {
			$this->template->render('Event/Mrfest/afterRegister');
		} else {
			$data["cekPengumuman"] = $this->M_EventPsdm2->cekDiterima($data['user']->NIM);
			$this->load->view('Event/Mrfest/AnnouncementLolos', $data);
		}
	}

	public function status()
	{
		$this->load->view('Event/Oprec/status');
	}

	public function statusDaftar()
	{
		$this->load->view('Event/Oprec/statusDaftar');
	}
}
