<?php

class Academy extends CI_Controller{
  
  public function __construct() {
		parent::__construct();
		$this->load->model('M_Academy', "academy");
  }
  
  public function index(){

    if(!$this->session->userdata('login')){
      redirect('login');
    }
    
    $data['courses']      = $this->academy->getLessons();
    $data['categories']   = $this->academy->getCategories();
    $this->load->view('academy/v_index', $data);
  
  }

  public function redirect($id){
    $link = $this->academy->getLink($id);
    $this->academy->updateVisitValue($id);
    redirect($link);
  }
}