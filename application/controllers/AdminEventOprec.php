<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminEventOprec extends CI_Controller {
  function __construct(){
    parent::__construct();
      $this->load->library('session');
      $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('M_EventOprec');
  }

  public function login(){
    if ($this->session->userdata('user')!="") {
				redirect('event/dashboardOprec');
		}
    $akun= $this->input->post('akun');
    $pass= $this->input->post('password');
    if ($akun=="adminOprec9" && $pass=="OprecJaya") {
  			$this->session->set_userdata('user', $akun);
        redirect('event/dashboardOprec');
      }
      else{
        $this->load->view('Event/Oprec/loginAdmin');
      }
    }

  public function dashboard(){
    if ($this->session->userdata('user')!="") {
    $data['pendaftar']=$this->M_EventOprec->getAllPendaftar();
    $this->load->view('Event/Oprec/dashboard',$data);
  }
  else {
    redirect('event/loginAdminOprec');
  }
}
  public function logout(){
    $this->session->unset_userdata('user');
	redirect('event/loginAdminOprec');
  }

  public function ubah($id_event, $key)
 {
	 $this->M_EventOprec->ubahStatus($id_event, $key);
	 redirect('event/dashboardOprec');
 }
}
