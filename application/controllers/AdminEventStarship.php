<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminEventStarship extends CI_Controller {
  function __construct(){
    parent::__construct();
      $this->load->library('session');
      $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('M_EventStarship');
  }

  public function login(){
    if ($this->session->userdata('user')!="") {
				redirect('event/dashboardStarship');
		}
    $akun= $this->input->post('akun');
    $pass= $this->input->post('password');
    if ($akun=="adminStarship" && $pass=="starshipJaya") {
  			$this->session->set_userdata('user', $akun);
        redirect('event/dashboardStarship');
      }
      else{
        $this->load->view('Event/Starship/loginAdmin');
      }
    }

  public function dashboard(){
    if ($this->session->userdata('user')!="") {
    $data['pendaftar']=$this->M_EventStarship->getAllPendaftar();
    $this->load->view('Event/Starship/dashboard',$data);
  }
  else {
    redirect('event/loginAdminStarship');
  }
}
  public function logout(){
    $this->session->unset_userdata('user');
	redirect('event/loginAdminStarship');
  }

  public function ubah($id_event, $key)
 {
	 $this->M_EventStarship->ubahStatus($id_event, $key);
	 redirect('event/dashboardStarship');
 }
}
